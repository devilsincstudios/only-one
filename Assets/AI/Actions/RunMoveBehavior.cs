//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using RAIN.Core;
//using RAIN.Action;
//using RAIN.Representation;
//
//[RAINAction]
//public class RunMoveBehavior : RAINAction
//{
//	//You can use Expressions to define all the BTAssets you want executed
//	//This will be displayed as a field in your custom action node
//	public Expression WalkBehavior;
//
//	//Turn this on to see debug output
//	private bool DisplayDebug = false;
//
//    public RunMoveBehavior()
//    {
//		actionName = "RunMoveBehavior";
//	}
//	
//    public override void Start(AI ai)
//    {
//        base.Start(ai);
//    }
//
//    public override ActionResult Execute(AI ai)
//    {
//		//Get sensor and match on aspect name
//		ai.Senses.GetSensor("eyes").MatchAspectName("aObject");
//		var match = ai.Senses.GetSensor("eyes").Matches;
//
//		//Output debug info
//		if(DisplayDebug)
//		{
//			Debug.Log("Root Node Action State = " + 
//			          ((CustomMind)ai.Mind).BindingNodes[0].ActionState
//			          + " Action Name = " + 
//			          ((CustomMind)ai.Mind).BindingNodes[0].ActionName);
//
//			for(int i=0; i < ((CustomMind)ai.Mind).BindingNodes[0].GetChildCount(); i++)
//			{
//				Debug.Log("Action State = " + 
//				          ((CustomMind)ai.Mind).BindingNodes[0].GetChild(i).ActionState 
//				          + " & Action Name = " + 
//				          ((CustomMind)ai.Mind).BindingNodes[0].GetChild(i).ActionName);
//			}
//		}
//
//		//Check if particular entity was detected
//		//Another option, loop over your BindingNodes collection 
//		//to find the ActionName matching the Expression WalkBehavior
//		if(match != null && match.Count > 0)
//		{
//			//Here you could choose which BTAsset to run by
//			//evaluating the expression variable and looping 
//			//over the BindingNodes collection to find 
//			//the ActionName that matches your Expression WalkBehavior
//			var btCommand = WalkBehavior.Evaluate(ai.DeltaTime, ai.WorkingMemory).GetValue<string>();
//			Debug.Log(btCommand);
//
//			//Execute the only BTAsset we have Walk index 0
//			//As noted above, you could enter the index of the BTAsset 
//			//you wanted based on the Expression WalkBehavior match in the BindingNodes collection
//			return ((CustomMind)ai.Mind).BindingNodes[0].Run(ai);
//		}
//
//		return ActionResult.SUCCESS;
//    }
//
//    public override void Stop(AI ai)
//    {
//		//Optional to reset the BTAssets children as if they have never been run
//		((CustomMind)ai.Mind).BindingNodes[0].ResetChildren();
//        base.Stop(ai);
//    }
//}