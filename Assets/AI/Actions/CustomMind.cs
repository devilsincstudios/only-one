﻿//using UnityEngine;
//using RAIN.Serialization;
//using RAIN.Core;
//using RAIN.Minds;
//using RAIN.BehaviorTrees;
//using System.Collections.Generic;
//
//[RAIN.Serialization.RAINSerializableClass]
//public class CustomMind : BasicMind  {
//
//	//Expose a property list for adding behavior tree assets in the AIRig mind element panel
//	[RAIN.Serialization.RAINSerializableField(Visibility=RAIN.Serialization.FieldVisibility.ShowAdvanced)]
//	public List<RAIN.BehaviorTrees.BTAssetBinding> assetBindings = new List<RAIN.BehaviorTrees.BTAssetBinding>();
//
//	//Create a variable to store the root node reference to each behavior tree asset we load
//	public List<RAIN.BehaviorTrees.BTNode> BindingNodes = new List<RAIN.BehaviorTrees.BTNode>();
//
//	//Load up all behavior tree assets on AI Initialize
//	public override void AIInit()
//	{
//		foreach(var asset in assetBindings)
//		{
//			//Load the Behavior Tree Asset and just new up an empty bindings list since we won't have any
//			//Add these to our Behavior Tree Nodes collection
//			//BindingNodes.Add(BTLoader.Load(asset.behaviorTree, new List<RAIN.BehaviorTrees.BTAssetBinding>()));
//		}
//
//		base.AIInit();
//	}
//}
