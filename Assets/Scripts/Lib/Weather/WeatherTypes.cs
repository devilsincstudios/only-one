/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for types of weather conditions
*
*/

namespace DI_GameState.Weather {
	public enum WeatherTypes {
		CLEAR,
		RAIN,
		OVERCAST,
		STORM,
		FOG,
		HAZE
	}
}
