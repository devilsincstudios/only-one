// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

namespace DI_Game.Entity
{
	public enum EntityType
	{
		FRIENDLY,
		HELICOPTER,
		HOSTILE,
		ITEM,
		OUTPOST,
		PLACEMENT,
		PLAYER,
		SNIPER,
		SURVIVOR
	}
}