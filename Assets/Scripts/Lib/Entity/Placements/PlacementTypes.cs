// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

namespace DI_Game.Entity
{
	public enum PlacementType
	{
		AMMO_CACHE,
		BARRIER_BARBED_WIRE,
		BARRIER_SAND_BAGS,
		WEAPON_MOUNTED_MACHINE_GUN,
	}
}