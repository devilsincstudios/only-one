//// /*
//// *
//// * 	Devils Inc Studios
//// * 	How Long
//// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//// *	
//// *	TODO: Include a description of the file here.
//// *
//// */
//
//	This isn't going to be easy to accomplish.
//	Mono 4.5 is required for these functions, however Unity uses 2.0.
// 	The work around is to spawn this off as a seperate process but its not (in my opinion) worth the time to develop.
//
//using System;
//using System.Speech;
//using System.Speech.Synthesis;
//using UnityEngine;
//using System.Collections;
//
//public class Speech : MonoBehaviour {
//	SpeechSynthesizer speaker;
//	public Speech() {
//		speaker = new SpeechSynthesizer();
//	}
//	public void speak(string message) {
//		speaker.SpeakAsync(message);
//	}
//	public void pause() {
//		if (speaker.State == SynthesizerState.Speaking) 
//		{ 
//			speaker.Pause(); 
//		}
//	}
//	public void resume() {
//		if (speaker.State == SynthesizerState.Paused) 
//		{ 
//			speaker.Pause(); 
//		}
//	}
//	public void  wait(float time) {
//		StartCoroutine(waitSeconds(time));
//	}
//	public IEnumerator waitSeconds(float time) {
//		yield return new WaitForSeconds(time);
//		speaker.Resume();
//	}
//}