/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
* 	Class for creating log entrys.
*
*/

using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Web;

namespace DI_Debug {

	// This is used in combination with logger.
	public class LogMessage {

		public string title;
		public string message;
		public StackTrace trace;
		public string screenshotFileName;
		public DateTime date;
		public string JSONMessage;
		public List<string> traceOutput;
		public string safeTime;
		public string component;
		public string logLevel;

		// NOTE
		// This used to have more options but if I keep adding to it its going to get insane.
		// Just use the methods to properly setup a logMessage object.
		public LogMessage() {
			setTitle("");
			setMessage("");
			setStackTrace(new StackTrace());
			setTime(DateTime.Now);
			setScreenShot("");
			setLogLevel("info");
			setComponent("system");
		}

		// Set the title of the entry.
		public void setTitle(string msg) {
			title = msg;
		}
		// Get the title of the entry.
		public string getTitle() {
			return title;
		}
		// Set the message of the entry.
		public void setMessage(string msg) {
			message = msg;
		}
		// Get the message of the entry.
		public string getMessage() {
			return message;
		}
		// Set the stack trace of the entry.
		public void setStackTrace(StackTrace st) {
			trace = st;
		}
		// Get the stack trace of the entry.
		public StackTrace getStackTrace() {
			return trace;
		}
		// Set the screenshot file name of the entry.
		public void setScreenShot(string ss) {
			screenshotFileName = ss;
		}
		// Get the screenshot file name of the entry.
		public string getScreenShot() {
			return screenshotFileName;
		}
		// Does this entry have a screenshot?
		public bool hasScreenShot() {
			return (screenshotFileName != "");
		}
		// Set the time of the entry.
		public void setTime(DateTime dt) {
			date = dt;
		}
		// Get the time of the entry.
		public DateTime getTime() {
			return date;
		}

		// Set the log level of the entry.
		// TODO Convert this to an ENUM
		// For now
		/*
		 * 1 = Critical
		 * 2 = High
		 * 3 = Medium
		 * 4 = Low
		 * 5 = Info
		 */
		public void setLogLevel(string level) {
			logLevel = level;
		}

		// Get the log level.
		public string getLogLevel() {
			return logLevel;
		}

		// Set the componet this entry is coming from.
		// TODO Convert this to an ENUM.
		// For now any string value works.
		// If you use an unknown string, the log viewer won't filter for it but it should still work.
		public void setComponent(string system) {
			component = system;
		}

		// Get the component for this entry.
		public string getComponent() {
			return component;
		}

		// Encode the time for HTML safety.
		public string getSafeTime() {
			return HttpUtility.HtmlEncode(date.ToString());
		}

		// Get the trace output for the entry.
		// This walks though each line of the entry and creates a json array of its contents.
		// NOTE the return is a json formatted string.
		public string getTraceOutput() {
			string output;
			int iteration = 0;
			output = "";

			// This should do a foreach and take each of the lines in the stack trace, compress them into a single line and return it as proper json.
			/*
				{"0":"parseCommand (System.Reflection.ParameterInfo[]) @ D:\\HowLong\\Assets\\Scripts\\Game Scripts\\GamePlay\\Debug\\DebugConsole.cs:257"},
				{"1":"DrawOutsideContents (System.Reflection.ParameterInfo[]) @ D:\\HowLong\\Assets\\Scripts\\Game Scripts\\GamePlay\\Debug\\DebugConsole.cs:99"},
				{"2":"OnGUI (System.Reflection.ParameterInfo[]) @ D:\\HowLong\\Assets\\Scripts\\Game Scripts\\GamePlay\\Debug\\DebugConsole.cs:86"}
			 * */
			traceOutput.ForEach(delegate(String stOutput) {
				if (traceOutput.Count == iteration + 1) {
					output += "{\"" + iteration + "\":\"" + stOutput + "\"}" + Environment.NewLine;
				}
				else {
					output += "{\"" + iteration + "\":\"" + stOutput + "\"}," + Environment.NewLine;
				}
				++iteration;
			});
			return output;
		}

		// Commits the message, converting all lines to their html friendly counter-parts.
		public void commitMessage() {
			setTitle(HttpUtility.HtmlEncode(title));
			setMessage (HttpUtility.HtmlEncode(message));
			traceOutput = new List<string>();
			foreach (StackFrame frame in trace.GetFrames()) {
				// Add each line of the stack trace to the buffer.
				// For each line of the stack trace make it html safe and save it in json format.
				traceOutput.Add(parseStackFrame(frame));
			}
			// Convert the timestamp to html format.
			safeTime = getSafeTime();
			// Convert screenshot file name to one we can use for linking.
			if (hasScreenShot()) {
				setScreenShot(HttpUtility.HtmlEncode(screenshotFileName.Replace("Logs\\", "")));
			}
		}

		// Returns the message as a JSON object.
		public string toJSON() {
			/*
			{
				"0_33_17_952_4085" : [
				{"title":""},
				{"message":"Test Message"}, 
				{"stack trace":[
				{"0":"parseCommand (System.Reflection.ParameterInfo[]) @ D:\\HowLong\\Assets\\Scripts\\Game Scripts\\GamePlay\\Debug\\DebugConsole.cs:257"},
				{"1":"DrawOutsideContents (System.Reflection.ParameterInfo[]) @ D:\\HowLong\\Assets\\Scripts\\Game Scripts\\GamePlay\\Debug\\DebugConsole.cs:99"},
				{"2":"OnGUI (System.Reflection.ParameterInfo[]) @ D:\\HowLong\\Assets\\Scripts\\Game Scripts\\GamePlay\\Debug\\DebugConsole.cs:86"}
				]},
				{"screenshot":"ScreenShots\\11_27_2013\\0_33_953.png"}, 
				{"time":"11/27/2013 12:33:17 AM"}, 
				{"log level":"high"}, 
				{"component":"system"}
				]
			}
			 */
			commitMessage();
			Random numberGen = new Random();
			int randomNumber = numberGen.Next(0, 10000);
			string uniqueName = date.Hour + "_" + date.Minute + "_" + date.Second + "_" + date.Millisecond + "_" + randomNumber;
			JSONMessage = "\"" + uniqueName + "\" : [" + Environment.NewLine;
			JSONMessage += "{\"title\" : \"" + getTitle() + "\"}," + Environment.NewLine;
			JSONMessage += "{\"message\" : \"" + getMessage() + "\"}, " + Environment.NewLine;
			JSONMessage += "{\"stack trace\" : [" + getTraceOutput().Replace("\\", "\\\\") + "]}," + Environment.NewLine;
			JSONMessage += "{\"screenshot\" : \"" + getScreenShot().Replace("\\", "\\\\") + "\"}, " + Environment.NewLine;
			JSONMessage += "{\"time\" : \"" + getSafeTime() + "\"}, " + Environment.NewLine;
			JSONMessage += "{\"log level\" : \"" + getLogLevel() + "\"}, " + Environment.NewLine;
			JSONMessage += "{\"component\" : \"" + getComponent() + "\"} " + Environment.NewLine;
			JSONMessage += "],";
			return JSONMessage;
		}

		// Parses the trace message and formats it into <method>@<file>:<line> format.
		private string parseStackFrame(StackFrame st) {
			string returnValue;
			returnValue = st.GetMethod().ToString() + " @ " + st.GetFileName() + ":" + st.GetFileLineNumber();
			return returnValue;
		}
	}
}