// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_Debug
{
	public class Exit : DebugCommand
	{
		public Exit()
		{
			name = "Exit";
			command = "EXIT";
			description = "Exits the game.";
			functionCall = ((method)delegate() {
				Application.Quit();
			});
		}
	}
}