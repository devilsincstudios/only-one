// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_Debug
{
	public class ClearWeather : DebugCommand
	{
		public ClearWeather()
		{
			name = "Clear Weather";
			command = "SET WEATHER CLEAR";
			description = "Sets the weather to clear.";
			functionCall = ((method)delegate(){
				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.CLEAR);
			});
		}
	}
}