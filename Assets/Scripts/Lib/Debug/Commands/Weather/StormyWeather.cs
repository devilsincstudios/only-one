// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_Debug
{
	public class StormyWeather : DebugCommand
	{
		public StormyWeather()
		{
			name = "Stormy Weather";
			command = "SET WEATHER STORM";
			description = "Sets the weather to stormy.";
			functionCall = ((method)delegate(){
				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.STORM);
			});
		}
	}
}