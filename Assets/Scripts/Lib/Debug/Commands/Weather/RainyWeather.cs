// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_Debug
{
	public class RainyWeather : DebugCommand
	{
		public RainyWeather()
		{
			name = "Rainy Weather";
			command = "SET WEATHER RAIN";
			description = "Sets the weather to rain.";
			functionCall = ((method)delegate(){
				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.RAIN);
			});
		}
	}
}