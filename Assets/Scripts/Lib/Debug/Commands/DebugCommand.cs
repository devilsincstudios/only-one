/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	This class defines a debug command that will be used by the console.
*	See DebugCommands.cs for more info.
*
*/

using System;
using System.Collections.Generic;
using System.Collections;

namespace DI_Debug {
	public class DebugCommand {
		public string name;
		public string command;
		public string description;
		public Delegate functionCall;
		public delegate void method();

		public void runFunction() {
			if (functionCall != null) {
				functionCall.DynamicInvoke();
			}
		}
	}
}
