// /*
// *
// * 	Devils Inc Studios
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	The idea here is to have a tree hierarchy of commands so that they can be displayed by the console in a tree view.
// *	
// *	Clear
// *	Set
// *	Set\Players
// *	Set\Players\1
// *	Set\Players\2
// *	Set\Time
// *	Set\Time\+1 hour
// *	Set\Time\-1 hour
// *	Set\Time\Dawn
// *	Set\Time\Noon
// *	Set\Time\Dusk
// *	Set\Time\Midnight
// *	Set\Weather
// *	Set\Weather\Clear
// *	Set\Weather\Fog
// *	Set\Weather\Overcast
// *	Set\Weather\Rain
// *	Set\Weather\Storm
// *	Show Metrics
// *	Screenshot
// *	Test
// *	Test\Events
// *	Test\Logs
// *	Toggle\Freeroam
// */
//
//using UnityEngine;
//using System.Collections;
//using System;
//using System.Collections.Generic;
//
//namespace DI_Debug {
//	public class DebugCommands {
//		private DebugConsole debugConsole;
//		private GameObject masterMind;
//		private DI_GameState.GameStateController GSC;
//		private List<DebugCommand> rootNodes;
//
//		delegate void method();
//
//		private void onTestEvent2(string message) {
//			debugConsole.log ("Received Event: " + message);
//		}
//		
//		private void onTestEvent(string message) {
//			debugConsole.log ("Received Event: " + message);
//		}
//
//		// This looks insane, but its because of the nesting.
//		// Headers of each root node provided to help with figuring out what is going on.
//		//== Means end of child node.
//		//========== Means end of parent node.
//		// Functions are inlined and should all return void and be called with no arguments.
//		// The console will need to call the DebugCommand.runFunction() method to actually use the function.
//
//
//		public DebugCommands() {
//			// Important game references.
//			debugConsole = GameObject.FindGameObjectWithTag("Console").GetComponent<DebugConsole>();
//			masterMind = GameObject.FindGameObjectWithTag("Master Mind");
//			GSC = masterMind.GetComponent<DI_GameState.GameStateController>();
//
//			// List of all the nodes starting with the root.
//			rootNodes = new List<DebugCommand>();
//
//			// Clear =========================================================================================================
//			DebugCommand clear = new DebugCommand();
//			clear.setName("Clear Log");
//			clear.setDescription("Clear the console history.");
//			clear.setFunction((method)delegate() {
//				debugConsole.clearContents();
//			});
//
//			// Update Root Node
//			// ---------------------------------------------------------
//			rootNodes.Add(clear);
//			// ---------------------------------------------------------
//
//			// Set =========================================================================================================
//			/*
//			*	Set
//			*	Set\Players
//			*	Set\Players\1
//			*	Set\Players\2
//			*	Set\Time
//			*	Set\Time\+1 hour
//			*	Set\Time\-1 hour
//			*	Set\Time\Dawn
//			*	Set\Time\Noon
//			*	Set\Time\Dusk
//			*	Set\Time\Midnight
//			*	Set\Weather
//			*	Set\Weather\Clear
//			*	Set\Weather\Fog
//			*	Set\Weather\Overcast
//			*	Set\Weather\Rain
//			*	Set\Weather\Storm
//			*/
//			// Set =========================================================================================================
//			DebugCommand set = new DebugCommand();
//			set.setName("Set");
//
//			//========== PLAYERS
//			/*
//			*	Set\Players
//			*	Set\Players\1
//			*	Set\Players\2
//			*/
//
//			DebugCommand setPlayers = new DebugCommand();
//			setPlayers.setName("Players");
//			//==
//			DebugCommand setPlayers1 = new DebugCommand();
//			setPlayers1.setName("1");
//			setPlayers1.setDescription("Set the amount of players to 1.");
//			setPlayers1.setFunction((method)delegate() {
//				GSC.setPlayers(1);
//				debugConsole.log ("Set Players to 1.");
//			});
//			//==
//
//			DebugCommand setPlayers2 = new DebugCommand();
//			setPlayers2.setName("2");
//			setPlayers2.setDescription("Set the amount of players to 2.");
//			setPlayers2.setFunction((method)delegate() {
//				GSC.setPlayers(2);
//				debugConsole.log ("Set Players to 2.");
//			});
//			//==
//
//			// Update parent Node [Players]
//			//==========
//			setPlayers.addChild(setPlayers1);
//			setPlayers.addChild(setPlayers2);
//			set.addChild(setPlayers);
//			//==========
//
//			//========== TIME
//			/*
//			*	Set\Time
//			*	Set\Time\+1 hour
//			*	Set\Time\-1 hour
//			*	Set\Time\Dawn
//			*	Set\Time\Noon
//			*	Set\Time\Dusk
//			*	Set\Time\Midnight
//			*/
//
//			DebugCommand setTime = new DebugCommand();
//			setTime.setName("Time");
//			//==
//			// +1 Hour
//			DebugCommand setTimeHourForward = new DebugCommand();
//			setTimeHourForward.setName("+1 Hour");
//			setTimeHourForward.setDescription("Moves the game time ahead by one hour.");
//			setTimeHourForward.setFunction((method)delegate() {
//				debugConsole.log("Time: +1 Hour");
//			});
//			//==
//			// -1 Hour
//			DebugCommand setTimeHourBackward = new DebugCommand();
//			setTimeHourBackward.setName("-1 Hour");
//			setTimeHourBackward.setDescription("Moves the game time back by one hour.");
//			setTimeHourBackward.setFunction((method)delegate() {
//				debugConsole.log("Time: -1 Hour");
//			});
//			//==
//			// Dawn
//			DebugCommand setTimeDawn = new DebugCommand();
//			setTimeDawn.setName("Dawn");
//			setTimeDawn.setDescription("Sets the game time to dawn.");
//			setTimeDawn.setFunction((method)delegate() {
//				debugConsole.log("Time: Dawn");
//			});
//			//==
//			// Noon
//			DebugCommand setTimeNoon = new DebugCommand();
//			setTimeNoon.setName("Noon");
//			setTimeNoon.setDescription("Sets the game time to noon.");
//			setTimeNoon.setFunction((method)delegate() {
//				debugConsole.log("Time: Noon");
//			});
//			//==
//			// Dusk
//			DebugCommand setTimeDusk = new DebugCommand();
//			setTimeDusk.setName("Dusk");
//			setTimeDusk.setDescription("Sets the game time to dusk.");
//			setTimeDusk.setFunction((method)delegate() {
//				debugConsole.log("Time: Dusk");
//			});
//			//==
//			// Midnight
//			DebugCommand setTimeMidnight = new DebugCommand();
//			setTimeMidnight.setName("Midnight");
//			setTimeMidnight.setDescription("Sets the game time to midnight.");
//			setTimeMidnight.setFunction((method)delegate() {
//				debugConsole.log("Time: Midnight");
//			});
//			//==========
//
//			// Update parent Node [Time]
//			//==========
//			setTime.addChild(setTimeHourForward);
//			setTime.addChild(setTimeHourBackward);
//			setTime.addChild(setTimeDawn);
//			setTime.addChild(setTimeDusk);
//			setTime.addChild(setTimeMidnight);
//			setTime.addChild(setTimeNoon);
//			set.addChild(setTime);
//			//==========
//
//			//========== WEATHER
//			/*
//			*	Set\Weather
//			*	Set\Weather\Clear
//			*	Set\Weather\Fog
//			*	Set\Weather\Overcast
//			*	Set\Weather\Rain
//			*	Set\Weather\Storm
//			*/
//
//			DebugCommand setWeather = new DebugCommand();
//			setWeather.setName("Weather");
//			//==
//			// Clear
//			DebugCommand setWeatherClear = new DebugCommand();
//			setWeatherClear.setName("Clear");
//			setWeatherClear.setDescription("Sets the weather to clear.");
//			setWeatherClear.setFunction((method)delegate() {
//				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.CLEAR);
//				debugConsole.log("Weather: Clear");
//			});
//			//==
//
//			// Fog
//			DebugCommand setWeatherFog = new DebugCommand();
//			setWeatherFog.setName("Fog");
//			setWeatherFog.setDescription("Sets the weather to fog.");
//			setWeatherFog.setFunction((method)delegate() {
//				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.FOG);
//				debugConsole.log("Weather: Fog");
//			});
//			//==
//
//			// Overcast
//			DebugCommand setWeatherOvercast = new DebugCommand();
//			setWeatherOvercast.setName("Overcast");
//			setWeatherOvercast.setDescription("Sets the weather to overcast.");
//			setWeatherOvercast.setFunction((method)delegate() {
//				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.OVERCAST);
//				debugConsole.log("Weather: Overcast");
//			});
//			//==
//
//			// Rain
//			DebugCommand setWeatherRain = new DebugCommand();
//			setWeatherRain.setName("Rain");
//			setWeatherRain.setDescription("Sets the weather to rain.");
//			setWeatherRain.setFunction((method)delegate() {
//				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.RAIN);
//				debugConsole.log("Weather: Rain");
//			});
//			//==
//
//			// Storm
//			DebugCommand setWeatherStorm = new DebugCommand();
//			setWeatherStorm.setName("Storm");
//			setWeatherStorm.setDescription("Sets the weather to storm.");
//			setWeatherStorm.setFunction((method)delegate() {
//				DI_Events.EventCenter<DI_GameState.Weather.WeatherTypes>.invoke("onWeatherChanged", DI_GameState.Weather.WeatherTypes.STORM);
//				debugConsole.log("Weather: Storm");
//			});
//			//==========
//
//			// Update parent Node [Weather]
//			//==========
//			setWeather.addChild(setWeatherClear);
//			setWeather.addChild(setWeatherFog);
//			setWeather.addChild(setWeatherOvercast);
//			setWeather.addChild(setWeatherRain);
//			setWeather.addChild(setWeatherStorm);
//			set.addChild(setWeather);
//			//==========
//
//			// Update Root Node
//			// ---------------------------------------------------------
//			rootNodes.Add(set);
//			// ---------------------------------------------------------
//
//			// Show Metrics =========================================================================================================
//			DebugCommand showMetrics = new DebugCommand();
//			showMetrics.setName("Show Metrics");
//			showMetrics.setDescription("Shows the current cpu / ram usage and frame rate.");
//			showMetrics.setFunction((method)delegate() {
//				Metrics metrics = new Metrics();
//				debugConsole.StartCoroutine(metrics.getCurrentCpuUsage());
//				debugConsole.StartCoroutine(metrics.getAvailableRAM());
//			});
//
//			// Update Root Node
//			// ---------------------------------------------------------
//			rootNodes.Add(showMetrics);
//			// ---------------------------------------------------------
//
//			// Screenshot =========================================================================================================
//			DebugCommand ss = new DebugCommand();
//			ss.setName("Screenshot");
//			ss.setDescription("Take a Screenshot.");
//			ss.setFunction((method)delegate() {
//				using (ScreenShot screenshot = new ScreenShot()) {
//					debugConsole.log ("Screenshot saved as: " + screenshot.takeScreenShot(true));
//				}
//			});
//
//			// Update Root Node
//			// ---------------------------------------------------------
//			rootNodes.Add(ss);
//			// ---------------------------------------------------------
//
//			//========== TEST
//			/*
//			 *	Test
//			 *	Test\Events
//			 *	Test\Logs
//			*/
//			DebugCommand test = new DebugCommand();
//			test.setName("Test");
//
//			//Test/Events
//			DebugCommand testEvents = new DebugCommand();
//			testEvents.setName("Events");
//			testEvents.setDescription("Runs a test of the events system.");
//			testEvents.setFunction((method)delegate() {
//				debugConsole.log ("Registering Event.");
//				DI_Events.EventCenter<string>.addListener("TestEvent", onTestEvent);
//				debugConsole.log ("Registering Event Again.");
//				DI_Events.EventCenter<string>.addListener("TestEvent", onTestEvent2);
//				debugConsole.log ("Sending Event.");
//				DI_Events.EventCenter<string>.invoke("TestEvent", "Test");
//				debugConsole.log ("Unregistering Event.");
//				DI_Events.EventCenter<string>.removeListener("TestEvent", onTestEvent);
//				DI_Events.EventCenter<string>.removeListener("TestEvent", onTestEvent2);
//			});
//			//==
//
//			//Test/Logs
//			DebugCommand testLogs = new DebugCommand();
//			testLogs.setName("Logs");
//			testLogs.setDescription("Runs a test of the logging system.");
//			testLogs.setFunction((method)delegate() {
//				Logger.Instance.testLogger();
//				debugConsole.log ("wrote to log file.");
//			});
//			//==
//
//			//Test/Save Binds
//			DebugCommand testSaveBinds = new DebugCommand();
//			testSaveBinds.setName("Save Binds");
//			testSaveBinds.setDescription("Saves the current keybindings to file - This should be removed once we have a GUI.");
//			testSaveBinds.setFunction((method)delegate() {
//				DI_Input.InputManager.saveKeyBindings();
//				debugConsole.log ("Saved Keybindings.");
//			});
//			//==========
//
//			// Test Waypoint Camera
//			DebugCommand testWaypointCamera = new DebugCommand();
//			testWaypointCamera.setName("Waypoint Camera");
//			testWaypointCamera.setDescription("Tests the waypoint camera.");
//			testWaypointCamera.setFunction((method)delegate() {
//				debugConsole.log ("Testing waypoint camera.");
//			});
//
//			// Update parent Node [Test]
//			//==========
//			test.addChild(testEvents);
//			test.addChild(testLogs);
//			test.addChild(testSaveBinds);
//			test.addChild(testWaypointCamera);
//			//==========
//
//			// Update Root Node
//			// ---------------------------------------------------------
//			rootNodes.Add(test);
//			// ---------------------------------------------------------
//
//			// Toggle =========================================================================================================
//			DebugCommand toggle = new DebugCommand();
//			toggle.setName("Toggle");
//			//==========
//
//			// Freeroam
//			DebugCommand toggleFreeRoam = new DebugCommand();
//			toggleFreeRoam.setName("Freeroam");
//			toggleFreeRoam.setDescription("Toggles the freeroaming camera view.");
//			toggleFreeRoam.setFunction((method)delegate() {
//				GameObject freeroam = GameObject.FindGameObjectWithTag("Freeroam Camera");
//				DI_Camera.FreeroamCamera freeroamScript = freeroam.GetComponent<DI_Camera.FreeroamCamera>();
//				GameObject P1Camera = GameObject.FindGameObjectWithTag("MainCamera");
//				GameObject P2Camera = GameObject.FindGameObjectWithTag("P2Camera");
//				if (freeroamScript.isEnabled()) {
//					freeroamScript.disableCamera();
//					P1Camera.camera.enabled = true;
//					P1Camera.GetComponent<DI_Camera.ThirdPersonCamera>().enabled = true;
//					if (GSC.getPlayers() == 2) {
//						P2Camera.camera.enabled = true;
//					}
//				}
//				else {
//					freeroamScript.enableCamera();
//					P1Camera.camera.enabled = false;
//					P1Camera.GetComponent<DI_Camera.ThirdPersonCamera>().enabled = false;
//					P2Camera.camera.enabled = false;
//				}
//				debugConsole.log("Toggled: Freeroam.");
//			});
//			//==
//
//			// Update parent Node [Toggle]
//			//==========
//			toggle.addChild(toggleFreeRoam);
//			//==========
//
//			// Update Root Node
//			// ---------------------------------------------------------
//			rootNodes.Add(toggle);
//			// ---------------------------------------------------------
//
//			// Quit =========================================================================================================
//			DebugCommand quit = new DebugCommand();
//			quit.setName("Quit Game");
//			quit.setDescription("Quit the game.");
//			quit.setFunction((method)delegate() {
//				Application.Quit();
//			});
//			
//			// Update Root Node
//			// ---------------------------------------------------------
//			rootNodes.Add(quit);
//			// ---------------------------------------------------------
//
//		}
//		public List<DebugCommand> getCommands() {
//			return rootNodes;
//		}
//	}
//}
