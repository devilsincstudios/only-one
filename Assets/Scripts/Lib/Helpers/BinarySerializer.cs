// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class BinarySerializer<T>
{
	public static string serialize(T objectToSerialize)
	{
		using (MemoryStream stream = new MemoryStream()) {
			BinaryFormatter binFormater = new BinaryFormatter();
			binFormater.Serialize(stream, objectToSerialize);
			stream.Flush();
			stream.Position = 0;
			return Convert.ToBase64String(stream.ToArray());
		}
	}

	public static T deserialize(string serializedObject)
	{
		byte[] bytes = Convert.FromBase64String(serializedObject);
		using (MemoryStream stream = new MemoryStream(bytes)) {
			BinaryFormatter binFormater = new BinaryFormatter();
			stream.Seek(0, SeekOrigin.Begin);
			return (T)binFormater.Deserialize(stream);
		}
	}
}