/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for types of movements of doors.
*
*/

namespace DI_Game {
	public enum DoorMovementType {
		OPEN,
		CLOSE
	}
}