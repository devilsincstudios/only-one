// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

// NOTE This is not finished in the least bit
// This was an early attempt to standardize GUI calls.
// This is on hold until we start on guis.

namespace DI_GUI
{
	public class GUIBase : MonoBehaviour
	{
		public string guiName;
		public bool isVisable;
		public GUISkin guiSkin;
		public string skinName;

		protected GUIStyle rightTextGood;
		protected GUIStyle rightTextOk;
		protected GUIStyle rightTextPoor;
		protected GUIStyle rightTextCritical;
		protected GUIStyle rightAligned;
		
		protected GUIStyle leftTextGood;
		protected GUIStyle leftTextOk;
		protected GUIStyle leftTextPoor;
		protected GUIStyle leftTextCritical;
		protected GUIStyle leftAligned;

		protected GUIStyle centerTextGood;
		protected GUIStyle centerTextOk;
		protected GUIStyle centerTextPoor;
		protected GUIStyle centerTextCritical;
		protected GUIStyle centerAligned;

		protected GUIStyle guiHeader;

		public void Awake()
		{
			if (guiSkin == null) {
				guiSkin = (GUISkin) Resources.Load("Skins/" + skinName);
			}
		}

		public void OnGUI()
		{
			GUI.skin = guiSkin;
			rightTextGood = new GUIStyle("RightTextGood");
			rightTextOk = new GUIStyle("RightTextOk");
			rightTextPoor = new GUIStyle("RightTextPoor");
			rightTextCritical = new GUIStyle("RightTextCritical");
			rightAligned = new GUIStyle("RightAligned");
			
			leftTextGood = new GUIStyle("LeftTextGood");
			leftTextOk = new GUIStyle("LeftTextOk");
			leftTextPoor = new GUIStyle("LeftTextPoor");
			leftTextCritical = new GUIStyle("LeftTextCritical");
			leftAligned = new GUIStyle("LeftAligned");
			
			centerTextGood = new GUIStyle("CenterTextGood");
			centerTextOk = new GUIStyle("CenterTextOk");
			centerTextPoor = new GUIStyle("CenterTextPoor");
			centerTextCritical = new GUIStyle("CenterTextCritical");
			centerAligned = new GUIStyle("CenterAligned");
			
			guiHeader = new GUIStyle("GuiHeader");
		}

		public void toggleGUI()
		{
			isVisable = !isVisable;
			if (isVisable) {
				showGUI();
			}
			else {
				hideGUI();
			}
		}

		public void setName(string name)
		{
			guiName = name;
		}
		
		public string getName()
		{
			return guiName;
		}
		
		public bool isActive()
		{
			return isVisable;
		}
		
		// If you want to do animation on a GUI such as a fly in.
		// Do it in this function
		public void showGUI()
		{
			isVisable = true;
			DI_Events.EventCenter<DI_Game.GameState>.invoke("onGameStateChanged", DI_Game.GameState.IN_MENU);
		}
		
		// If you want to do animation on a GUI such as a fly out.
		// Do it in this function
		public void hideGUI()
		{
			isVisable = false;
			DI_Events.EventCenter<DI_Game.GameState>.invoke("onGameStateChanged", DI_Game.GameState.PLAYING);
		}

		// Function to handle drawing the GUI.
		public virtual void drawGUI(int guiId) {
		}
	}
}