// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections;

namespace DI_Game {
	public static class Footsteps {
		public static void playFootstep(Materials material, AudioSource noiseMaker) {
			MaterialSound materialSound = MaterialSounds.getSounds(material);
			if (materialSound.sounds.Count > 0) {
				float pitchRandomness = 0.05f;
				float volumeRandomness = 0.1f;

				float volumne = UnityEngine.Random.Range(0.4f - volumeRandomness, 0.4f + volumeRandomness);
				float pitch = UnityEngine.Random.Range(1.0f - pitchRandomness, 1.0f + pitchRandomness);



				AudioClip clip = materialSound.sounds[UnityEngine.Random.Range(0, materialSound.sounds.Count - 1)];

				noiseMaker.volume = volumne;
				noiseMaker.pitch = pitch;
				noiseMaker.clip = clip;
				noiseMaker.Play();
			}
		}
	}
}
