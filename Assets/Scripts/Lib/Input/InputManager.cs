/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	This class handles polling key states, saving and loading key bindings and setting controller types.
*	This class is static.
*
*/

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace DI_Input {
	static public class InputManager {
		private static Dictionary<int, ControllerType> controllers = new Dictionary<int, ControllerType>();

		// Returns the value of the axis bound to the key.
		static public float getAxisValue(RegisteredKeys key, int player) {
			if (BindManager.getType(key, player) == BindType.BIND_AXIS) {
				return Input.GetAxis(BindManager.getKey(key, player));
			}
			else {
				throw new Exception("Unable to get axis value from a non axis bind.");
			}
		}

		// Returns all known keystates.
		static public Dictionary<RegisteredKeys, KeyStates> getKeyStates(int player) {
			Dictionary<RegisteredKeys, KeyStates> keyStates = new Dictionary<RegisteredKeys, KeyStates>();
			foreach (RegisteredKeys key in Enum.GetValues(typeof(RegisteredKeys))) {
				keyStates.Add(key, getKeyDown(key, player));
			}
			return keyStates;
		}

		// Returns KeyState for the key.
		// If this is a key, it can return KEY_PRESSED or KEY_NOT_PRESSED.
		// If this is an axis, it can return KEY_POSITIVE, KEY_NEGATIVE
		static public KeyStates getKeyDown(RegisteredKeys key, int player) {
			// This can throw an exception.
			// Instead of handling it here, let it be handled upstream so you know its not bound to anything.
			// Same thing here, but if it makes it here, its safe.
			try {
				if (BindManager.getType(key, player) == BindType.BIND_KEY) {
					if (Input.GetKey((KeyCode)Enum.Parse(typeof(KeyCode), BindManager.getKey(key, player)))) {
						return KeyStates.KEY_PRESSED;
					}
					else {
						return KeyStates.KEY_NOT_PRESSED;
					}
				}
				if (BindManager.getType(key, player) == BindType.BIND_AXIS) {
					float axisValue = getAxisValue(key, player);
					if (axisValue > 0) {
						return KeyStates.KEY_POSITIVE;
					}
					else if (axisValue < 0) {
						return KeyStates.KEY_NEGATIVE;
					}
				}
				return KeyStates.KEY_NOT_PRESSED;
			}
			// General catch all, if we for some reason hit an exception log it and return NOT_PRESSED.
			catch (ArgumentException error) {
				UnityEngine.Debug.LogException(error);
				return KeyStates.KEY_NOT_PRESSED;
			}
		}

		// Saves the currently bound keys to the keybinds file (xml).
		static public void saveKeyBindings() {
			Dictionary<RegisteredKeys, string> p1Bindings = BindManager.getBinds(1);
			Dictionary<RegisteredKeys, string> p2Bindings = BindManager.getBinds(2);

			try {
				if (!Directory.Exists("Config")) {
					Directory.CreateDirectory("Config");
				}

				XElement root = new XElement("root");
				XElement player1 = new XElement("Player1");
				XElement player2 = new XElement("Player2");

				// Loop through player 1's binds saving each one as we go.
				foreach (KeyValuePair<RegisteredKeys, string> entry in p1Bindings) {
					XElement keyBind = new XElement(Enum.GetName(typeof(RegisteredKeys), entry.Key), entry.Value);
					keyBind.SetAttributeValue("type", Enum.GetName(typeof(BindType), BindManager.getType(entry.Key, 1)));
					player1.Add(keyBind);
				}
				// Loop through player 2's binds saving each one as we go.
				foreach (KeyValuePair<RegisteredKeys, string> entry in p2Bindings) {
					XElement keyBind = new XElement(Enum.GetName(typeof(RegisteredKeys), entry.Key), entry.Value);
					keyBind.SetAttributeValue("type", Enum.GetName(typeof(BindType), BindManager.getType(entry.Key, 2)));
					player2.Add(keyBind);
				}
				root.Add(player1);
				root.Add(player2);
				root.Save("Config/KeyBindings.xml");
			}
			catch (Exception error) {
				UnityEngine.Debug.LogException(error);
			}
		}

		// Load the saved key binds (xml)
		static public void loadKeyBindings() {
			if (File.Exists("Config/KeyBindings.xml")) {
				try {
					XElement config = XElement.Load("Config/KeyBindings.xml");
					XElement player1 = config.Element("Player1");
					XElement player2 = config.Element("Player2");
					foreach (XElement node in player1.Elements()) {
						// If its a BIND_AXIS node, treat it as an axis bind.
						//Debug.Log(node.Attribute("type").ToString() + ": " + node.Name.ToString() + ": " + node.Value);
						if (node.Attribute("type").ToString() == "type=\"BIND_AXIS\"") {
							BindManager.bindKey((RegisteredKeys)Enum.Parse(typeof(RegisteredKeys), node.Name.ToString()), node.Value, 1);
						}
						// Otherwise it must be a key bind.
						else {
							BindManager.bindKey((RegisteredKeys)Enum.Parse(typeof(RegisteredKeys), node.Name.ToString()), (KeyCode)Enum.Parse(typeof(KeyCode), node.Value), 1);
						}
					}
					foreach (XElement node in player2.Elements()) {
						// If its a BIND_AXIS node, treat it as an axis bind.
						//Debug.Log(node.Attribute("type").ToString() + ": " + node.Name.ToString() + ": " + node.Value);
						if (node.Attribute("type").ToString() == "type=\"BIND_AXIS\"") {
							BindManager.bindKey((RegisteredKeys)Enum.Parse(typeof(RegisteredKeys), node.Name.ToString()), node.Value, 2);
						}
						// Otherwise it must be a key bind.
						else {
							BindManager.bindKey((RegisteredKeys)Enum.Parse(typeof(RegisteredKeys), node.Name.ToString()), (KeyCode)Enum.Parse(typeof(KeyCode), node.Value), 2);
						}
					}
				}
				// If something went wrong, for example the file wasn't readable or was corrupt.
				// We still want keybinds, use the defaults.
				catch (Exception error) {
					UnityEngine.Debug.Log("Falling back to defaults.");
					setPlayerControllerType(1, ControllerType.PLAYER_ONE_KEYBOARD);
					setPlayerControllerType(2, ControllerType.PLAYER_TWO_CONTROLLER_ONE);
					loadDefaults(1);
					loadDefaults(2);
					saveKeyBindings();
					UnityEngine.Debug.LogException(error);
				}
			}
			// If the file doesn't exist
			// We still want keybinds, use the defaults.
			else {
				UnityEngine.Debug.Log("Falling back to defaults.");
				setPlayerControllerType(1, ControllerType.PLAYER_ONE_KEYBOARD);
				setPlayerControllerType(2, ControllerType.PLAYER_TWO_CONTROLLER_ONE);
				loadDefaults(1);
				loadDefaults(2);
				saveKeyBindings();
			}
		}

		// Sets the controller type the player is using.
		static public void setPlayerControllerType(int player, ControllerType type) {
			ControllerType oldType;
			if (controllers.TryGetValue(player, out oldType)) {
				controllers[player] = type;
			}
			else {
				controllers.Add(player, type);
			}
		}

		// Gets the controller type the player is using.
		static public ControllerType getPlayerControllerType(int player) {
			ControllerType type;
			if (controllers.TryGetValue(player, out type)) {
				return type;
			}
			else {
				return ControllerType.CONTROLLER_NONE;
			}
		}

		// This sets the default keys in the event we don't have a bindings file already or if there was an error.
		static public void loadDefaults(int player) {
			ControllerType controller = getPlayerControllerType(player);
			//Debug.Log("Player: " + player + " Controller:" + Enum.GetName(typeof(ControllerType), controller));

			if (controller == ControllerType.PLAYER_ONE_KEYBOARD || controller == ControllerType.PLAYER_TWO_KEYBOARD) {
				BindManager.bindKey(RegisteredKeys.AIM, KeyCode.Mouse1, player);
				BindManager.bindKey(RegisteredKeys.ATTACK, KeyCode.Mouse0, player);
				BindManager.bindKey(RegisteredKeys.BACKWARDS, KeyCode.S, player);
				BindManager.bindKey(RegisteredKeys.CAMERA_X, "Mouse X", player);
				BindManager.bindKey(RegisteredKeys.CAMERA_Y, "Mouse Y", player);
				BindManager.bindKey(RegisteredKeys.DEV_CONSOLE, KeyCode.BackQuote, player);
				BindManager.bindKey(RegisteredKeys.CROUCH, KeyCode.LeftControl, player);
				// ===============================================================
				// Developement Keys, Remove for final product.
				// ===============================================================
				BindManager.bindKey(RegisteredKeys.DEV_MENU, KeyCode.F12, player);
				BindManager.bindKey(RegisteredKeys.DEV_EXIT, KeyCode.F1, player);
				// ===============================================================
				BindManager.bindKey(RegisteredKeys.FLASHLIGHT, KeyCode.F, player);
				BindManager.bindKey(RegisteredKeys.FORWARDS, KeyCode.W, player);
				BindManager.bindKey(RegisteredKeys.INTERACT, KeyCode.E, player);
				BindManager.bindKey(RegisteredKeys.INVENTORY, KeyCode.I, player);
				BindManager.bindKey(RegisteredKeys.JUMP, KeyCode.Space, player);
				BindManager.bindKey(RegisteredKeys.NEXT_WEAPON, KeyCode.C, player);
				BindManager.bindKey(RegisteredKeys.PREVIOUS_WEAPON, KeyCode.Z, player);
				BindManager.bindKey(RegisteredKeys.RELOAD, KeyCode.R, player);
				BindManager.bindKey(RegisteredKeys.SPRINT, KeyCode.LeftShift, player);
				BindManager.bindKey(RegisteredKeys.TURN_LEFT, KeyCode.A, player);
				BindManager.bindKey(RegisteredKeys.TURN_RIGHT, KeyCode.D, player);
				BindManager.bindKey(RegisteredKeys.WEAPON_SWAP, "Mouse Scroll", player);
			}
			else {
				/*
				XBOX 360 Controller Bindings
				Flashlight                X
				Forward
				Backward        
				Turn Left               All from Left Stick
				Turn Right        
				Journal                Start
				Map                   Select
				Interact             B
				Jump               A  
				Attack            L1
				Aim               R1
				Switch Weapon        L2/R2
				Open Inventory         Y
				View PDA              Start
				Pause                  Depends if the option is set Bringin up the PDA will Pause
				Camera               Right Stick
				That's how I would map if it were me setting up joypad input, Quick heal on say right of the directional, up directional quick access to the journal page, 

				Buttons (Key or Mouse Button)	 Axis (Joystick Axis)
				joystick button 0 = A	 X axis = Left analog X
				joystick button 1 = B	 Y axis = Left analog Y
				joystick button 2 = X	 3rd axis = LT/RT
				joystick button 3 = Y	 4th axis = Right analog X
				joystick button 4 = L	5th axis = Right analog Y
				joystick button 5 = R	6th axis = Dpad X
				joystick button 6 = Back	7th axis = Dpad Y
				joystick button 7 = Home
				joystick button 8 = Left analog press
				joystick button 9 = Right analog press
				*/

				if (controller == ControllerType.PLAYER_ONE_CONTROLLER_ONE || controller == ControllerType.PLAYER_TWO_CONTROLLER_ONE) {
					BindManager.bindKey(RegisteredKeys.AIM, KeyCode.Joystick1Button5, player);
					BindManager.bindKey(RegisteredKeys.ATTACK, KeyCode.Joystick1Button4, player);
					BindManager.bindKey(RegisteredKeys.CAMERA_X, "R_XAxis_1", player);
					BindManager.bindKey(RegisteredKeys.CAMERA_Y, "R_YAxis_1", player);
					BindManager.bindKey(RegisteredKeys.FLASHLIGHT, KeyCode.Joystick1Button2, player);
					BindManager.bindKey(RegisteredKeys.HORIZONTAL, "L_XAxis_1", player);
					BindManager.bindKey(RegisteredKeys.INTERACT, KeyCode.Joystick1Button1, player);
					BindManager.bindKey(RegisteredKeys.INVENTORY, KeyCode.Joystick1Button3, player);
					BindManager.bindKey(RegisteredKeys.JUMP, KeyCode.Joystick1Button0, player);
					BindManager.bindKey(RegisteredKeys.NEXT_WEAPON, "TriggersR_1", player);
					BindManager.bindKey(RegisteredKeys.PREVIOUS_WEAPON, "TriggersL_1", player);
					BindManager.bindKey(RegisteredKeys.VERTICAL, "L_YAxis_1", player);
				}
				if (controller == ControllerType.PLAYER_TWO_CONTROLLER_TWO) {
					BindManager.bindKey(RegisteredKeys.AIM, KeyCode.Joystick2Button5, player);
					BindManager.bindKey(RegisteredKeys.ATTACK, KeyCode.Joystick2Button4, player);
					BindManager.bindKey(RegisteredKeys.CAMERA_X, "R_XAxis_2", player);
					BindManager.bindKey(RegisteredKeys.CAMERA_Y, "R_YAxis_2", player);
					BindManager.bindKey(RegisteredKeys.FLASHLIGHT, KeyCode.Joystick2Button2, player);
					BindManager.bindKey(RegisteredKeys.HORIZONTAL, "L_XAxis_2", player);
					BindManager.bindKey(RegisteredKeys.INTERACT, KeyCode.Joystick2Button1, player);
					BindManager.bindKey(RegisteredKeys.INVENTORY, KeyCode.Joystick2Button3, player);
					BindManager.bindKey(RegisteredKeys.JUMP, KeyCode.Joystick2Button0, player);
					BindManager.bindKey(RegisteredKeys.NEXT_WEAPON, "TriggersR_2", player);
					BindManager.bindKey(RegisteredKeys.PREVIOUS_WEAPON, "TriggersL_2", player);
					BindManager.bindKey(RegisteredKeys.VERTICAL, "L_YAxis_2", player);
				}
			}
		}
	}
}
