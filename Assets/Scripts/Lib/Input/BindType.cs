// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	ENUM values used for key bindings.
// *
// */

namespace DI_Input {
	public enum BindType {
		BIND_KEY,
		BIND_AXIS,
		UNDEFINED
	}
}