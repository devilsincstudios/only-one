/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for bindable keys aka game functions.
*
*/

namespace DI_Input {
	public enum RegisteredKeys {
		AIM,
		ATTACK,
		BACKWARDS,
		CAMERA_X,
		CAMERA_Y,
		CROUCH,
		DEV_CONSOLE,
		DEV_MENU,
		DEV_EXIT,
		FLASHLIGHT,
		FORWARDS,
		HORIZONTAL,
		INTERACT,
		INVENTORY,
		JUMP,
		NEXT_WEAPON,
		PREVIOUS_WEAPON,
		RELOAD,
		SPRINT,
		TURN_LEFT,
		TURN_RIGHT,
		UNBOUND,
		WEAPON_SWAP,
		VERTICAL
	}
}