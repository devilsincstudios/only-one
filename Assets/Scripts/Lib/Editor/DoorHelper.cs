// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System.Collections;

namespace DI_Editor {
	[CustomEditor(typeof(DI_Game.DoorController))]
	public class DoorHelper : Editor {
		private DI_Game.DoorController controller;
		public void Awake() {
			controller = (DI_Game.DoorController)target;
			if (controller.doorHandle == null) {
				GameObject handle = new GameObject();
				handle.transform.parent = controller.gameObject.transform;
				handle.transform.localPosition = new Vector3(controller.openX, controller.openY, controller.openZ);
				handle.gameObject.name = "Handle";
				controller.doorHandle = handle;
			}
			if (controller.openingSound == null) {
				// Add objects for opening sound.
				GameObject openingSound = new GameObject("Opening Sound");
				openingSound.gameObject.transform.parent = controller.doorHandle.transform;
				openingSound.gameObject.AddComponent<AudioSource>();
				openingSound.gameObject.transform.localPosition = new Vector3(0,0,0);
				openingSound.GetComponent<AudioSource>().volume = 0.5f;
				openingSound.GetComponent<AudioSource>().playOnAwake = false;

				// Add objects for closing sound.
				GameObject closingSound = new GameObject("Closing Sound");
				closingSound.gameObject.AddComponent<AudioSource>();
				closingSound.gameObject.transform.parent = controller.doorHandle.transform;
				closingSound.gameObject.transform.localPosition = new Vector3(0,0,0);
				closingSound.GetComponent<AudioSource>().volume = 0.5f;
				closingSound.GetComponent<AudioSource>().playOnAwake = false;
				
				// Assign the sounds to the controller.
				controller.openingSound = openingSound.GetComponent<AudioSource>();
				controller.closingSound = closingSound.GetComponent<AudioSource>();
			}
		}
		public override void OnInspectorGUI() {
			EditorGUIUtility.LookLikeControls();
			EditorGUILayout.BeginVertical();
			//====================================================================================================
			// Open Door Settings
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Open Door")) {
				if (controller.isSliding) {
					controller.gameObject.transform.localPosition = new Vector3(controller.openX, controller.openY, controller.openZ);
					controller.isOpen = true;
				}
				else {
					controller.gameObject.transform.localEulerAngles = new Vector3(controller.openX, controller.openY, controller.openZ);
					controller.isOpen = true;
				}
			}
			if (GUILayout.Button("Set Open Position")) {
				if (controller.isSliding) {
					controller.openX = controller.gameObject.transform.localPosition.x;
					controller.openY = controller.gameObject.transform.localPosition.y;
					controller.openZ = controller.gameObject.transform.localPosition.z;
				}
				else {
					controller.openX = controller.gameObject.transform.localEulerAngles.x;
					controller.openY = controller.gameObject.transform.localEulerAngles.y;
					controller.openZ = controller.gameObject.transform.localEulerAngles.z;
				}
			}
			EditorGUILayout.EndHorizontal();
			//====================================================================================================

			//====================================================================================================
			// Close Door Settings
			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("Close Door")) {
				if (controller.isSliding) {
					controller.gameObject.transform.localPosition = new Vector3(controller.closedX, controller.closedY, controller.closedZ);
					controller.isOpen = false;
				}
				else {
					controller.gameObject.transform.localEulerAngles = new Vector3(controller.closedX, controller.closedY, controller.closedZ);
					controller.isOpen = false;
				}
			}
			if (GUILayout.Button("Set Closed Position")) {
				if (controller.isSliding) {
					controller.closedX = controller.gameObject.transform.localPosition.x;
					controller.closedY = controller.gameObject.transform.localPosition.y;
					controller.closedZ = controller.gameObject.transform.localPosition.z;
				}
				else {
					controller.closedX = MathLib.wrapAngle(controller.gameObject.transform.localEulerAngles.x);
					controller.closedY = MathLib.wrapAngle(controller.gameObject.transform.localEulerAngles.y);
					controller.closedZ = MathLib.wrapAngle(controller.gameObject.transform.localEulerAngles.z);
				}
			}
			EditorGUILayout.EndHorizontal();
			//====================================================================================================

			//====================================================================================================
			// Door Params
			EditorGUILayout.BeginHorizontal();
			controller.isOpen = GUILayout.Toggle(controller.isOpen, "Is this door open?");
			controller.isSliding = GUILayout.Toggle(controller.isSliding, "Is this a sliding door?");
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			controller.movementSpeed = (float) EditorGUILayout.FloatField("Movement Speed", controller.movementSpeed);
			controller.movementSpeed = Mathf.Clamp(controller.movementSpeed, 0.0f, 100.0f);
			EditorGUILayout.EndHorizontal();

			// Opening Sound
			EditorGUILayout.BeginHorizontal();
			controller.openingSound.volume = (float) EditorGUILayout.FloatField("Opening Volume", controller.openingSound.volume);
			controller.openingSound.volume = Mathf.Clamp(controller.openingSound.volume, 0.0f, 1.0f);
			controller.openingSound.clip = (AudioClip)EditorGUILayout.ObjectField(controller.openingSound.clip, typeof(AudioClip), true);
			EditorGUILayout.EndHorizontal();

			// Closing Sound
			EditorGUILayout.BeginHorizontal();
			controller.closingSound.volume = (float) EditorGUILayout.FloatField("Closing Volume", controller.closingSound.volume);
			controller.closingSound.volume = Mathf.Clamp(controller.closingSound.volume, 0.0f, 1.0f);
			controller.closingSound.clip = (AudioClip)EditorGUILayout.ObjectField(controller.closingSound.clip, typeof(AudioClip), true);
			EditorGUILayout.EndHorizontal();
			//====================================================================================================
			EditorGUILayout.EndVertical();
		}
	}
}
