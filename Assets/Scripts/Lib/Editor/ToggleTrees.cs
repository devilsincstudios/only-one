// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	TODO: Include a description of the file here.
// *
// */

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

[InitializeOnLoad]
public class ToggleTrees : Editor  {
	static private Terrain terrain;
	static private float treeDistance;
	static private float treeBillboardDistance;
	static private GameObject structures;
	static private float drawDistance;
	static private int treeMeshes;
	static private bool sceneLoaded;

	static ToggleTrees() {
		sceneLoaded = false;
		SceneView.onSceneGUIDelegate += TreeUpdate;
	}

	static void TreeUpdate(SceneView sceneview)
	{
		if (sceneLoaded == false) {
			try {
				terrain = GameObject.FindGameObjectWithTag("Terrain").GetComponent<Terrain>();
				structures = GameObject.Find("Structures");
				drawDistance = terrain.detailObjectDistance;
				treeDistance = terrain.treeDistance;
				treeBillboardDistance = terrain.treeBillboardDistance;
				treeMeshes = terrain.treeMaximumFullLODCount;
				sceneLoaded = true;
			}
			catch (Exception) {
				//Debug.LogWarning(exception.Message);
			}
		}

		Event e = Event.current;
		if (e.isKey && e.character == 'y') {
			foreach (Renderer childRenderer in structures.GetComponentsInChildren<Renderer>(true)) {
				childRenderer.enabled = !childRenderer.enabled;
			}
		}

		if (e.isKey && e.character == 't')
		{

			if (terrain.treeDistance == 0) {
				terrain.treeDistance = treeDistance;
				terrain.treeBillboardDistance = treeBillboardDistance;
				terrain.treeMaximumFullLODCount = treeMeshes;
				terrain.detailObjectDistance = drawDistance;
			}
			else {
				terrain.treeDistance = 0;
				terrain.treeBillboardDistance = 0;
				terrain.treeMaximumFullLODCount = 0;
				terrain.detailObjectDistance = 0;
			}
		}
	}
}
