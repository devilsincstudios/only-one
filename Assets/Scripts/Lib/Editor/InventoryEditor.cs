// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	This is the Item Manager Tool
// 	it handle the window tool called "item manager editor"
//	and allow the users to create and manage all the items needed on the game. 
//	Every item will be stored into an external file
//	and can be loaded whenever the user want.
//	TODO: 	- find item by name
//			- "create prefab" button
//

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

namespace DI_Editor {
	public class InventoryEditor : EditorWindow {

		public List<int> inventoryIds;
		public List<string> inventoryNames;
		public List<string> inventoryDescriptions;
		private int currentInventory = 1;
		private bool inventoryLoaded = false;
		private Vector2 inventoryScrollPosition;
		public DI_Item.Inventory inventory;
		public Vector2 descriptionScroll;

		private int currentItem;
		private bool displayItem = false;
		private int selectedSlot = 0;

		private List<string> itemNames;
		private List<int> itemIds;
		private List<string> itemDescriptions;
		private List<int> itemIndexes;
		private int selectedItemIndex = 0;
		private bool loadSlotData = false;
		private DI_Item.Item _item;
		List<int> inventoryCount;

		[MenuItem("How Long/Items/Inventory Editor")]
		public static void Init() 
		{
			EditorWindow.GetWindow (typeof(InventoryEditor));	
		}

		public void loadInventoryData()
		{
			inventoryIds = DI_Item.InventoryManager.getInventoryIds();
			if (inventoryIds.Count != 0) {
				inventoryNames = DI_Item.InventoryManager.getInventoryNames();
				inventoryDescriptions = DI_Item.InventoryManager.getInventoryDescriptions();
				if (currentInventory != 0) {
					inventory = DI_Item.InventoryManager.loadInventory(inventoryIds[currentInventory - 1]);
				}

				List<string> temp = new List<string>();
				for (int iteration = 0; iteration < inventoryIds.Count; ++iteration) {
					temp.Add(inventoryIds[iteration] + ": " + inventoryNames[iteration]);
				}
				inventoryNames = temp;
				inventoryCount.Clear();
				for (int iteration = 0; iteration < inventoryIds.Count; ++iteration) {
					inventoryCount.Add(iteration + 1);
				}
			}
			else {
				inventory = new DI_Item.Inventory();
			}
			inventoryLoaded = true;
		}

		public void Awake()
		{
			inventoryCount = new List<int>();
			loadInventoryData();
		}

		public void OnEnable()
		{
			this.title = "Inventory Editor";
			inventoryScrollPosition = new Vector2();
			descriptionScroll = new Vector2();
			itemIds = DI_Item.ItemDatabase.getItemIds();
			itemNames = DI_Item.ItemDatabase.getItemNames();
			//itemDescriptions = DI_Item.ItemDatabase.getItemDescriptions();
			itemIndexes = DI_Item.ItemDatabase.getItemIndexes();
		}

		public Texture2D getAddItemIcon()
		{
			return Resources.Load<Texture2D>("Icons/Items/addItem");
			//return (Texture2D) AssetDatabase.LoadAssetAtPath("Assets/GUI/addItem.png", typeof(Texture2D));
		}

		public void OnGUI()
		{
			if (inventoryLoaded) {
				if (inventoryIds.Count == 0) {
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField("No inventories have been created.");
					EditorGUILayout.EndHorizontal();

					EditorGUILayout.BeginHorizontal();
					if (GUILayout.Button("Create new inventory")) {
						DI_Item.Inventory _inventory = new DI_Item.Inventory();
						DI_Item.InventoryManager.saveInventory(_inventory, _inventory.getId());
						loadInventoryData();
					}
					if (GUILayout.Button("Reset inventory ids")) {
						PlayerPrefs.DeleteKey("Game/Inventory/Last Id");
						loadInventoryData();
					}
					EditorGUILayout.EndHorizontal();
				}
				else {
					GUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField("Current Inventory:", GUILayout.Width(125));
					currentInventory = EditorGUILayout.IntPopup(currentInventory, inventoryNames.ToArray(), inventoryCount.ToArray());

					if (GUI.changed) {
						Debug.Log("Current inventory: " + currentInventory);
						loadInventoryData();
					}

					if (currentInventory > inventoryIds.Count) {
						currentInventory = inventoryIds.Count;
					}
					if (currentInventory < 1) {
						currentInventory = 1;
					}
					
					if (GUILayout.Button("Previous", GUILayout.ExpandWidth (false))) {
						if (currentInventory > 1)
							currentInventory--;
							loadInventoryData();
						GUI.FocusControl ("clearFocus");
					}
					
					if (currentInventory < inventoryIds.Count) {
						if (GUILayout.Button ("Next", GUILayout.ExpandWidth (false))) {
							if (currentInventory < inventoryIds.Count) {
								currentInventory++;
								loadInventoryData();
							}
							GUI.FocusControl("clearFocus");
						}
					}
					else {
						if (GUILayout.Button ("New", GUILayout.ExpandWidth (false))) {
							if (currentInventory == inventoryIds.Count) {
								DI_Item.Inventory _inventory = new DI_Item.Inventory();
								DI_Item.InventoryManager.saveInventory(_inventory, _inventory.getId());
								loadInventoryData();
								currentInventory++;
								loadInventoryData();
							}
							GUI.FocusControl("clearFocus");
						}
					}
					GUILayout.EndHorizontal ();

					GUILayout.BeginHorizontal ();
					GUILayout.Label("Inventory Id: " + inventory.inventoryId);
					GUILayout.EndHorizontal ();

					GUILayout.BeginHorizontal ();
					inventory.inventoryName = EditorGUILayout.TextField("Inventory Name: ", inventory.inventoryName);
					GUILayout.EndHorizontal ();

					GUILayout.BeginHorizontal ();
					inventory.maxSlots = EditorGUILayout.IntSlider("Inventory Slots:", inventory.maxSlots, 1, 20, GUILayout.ExpandWidth(false));
					GUILayout.EndHorizontal ();

					GUILayout.BeginHorizontal ();
					inventory.type = (DI_Item.InventoryType) EditorGUILayout.EnumPopup("Inventory Type:", inventory.type);
					GUILayout.EndHorizontal ();

					GUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Description", GUILayout.MaxWidth (70));
					descriptionScroll = EditorGUILayout.BeginScrollView (descriptionScroll);
					EditorStyles.textField.wordWrap = true;
					inventory.inventoryDescription = EditorGUILayout.TextArea (inventory.inventoryDescription, GUILayout.Height(50));
					EditorGUILayout.EndScrollView ();
					GUILayout.EndHorizontal();

					GUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField("Current Weight: " + inventory.currentWeight);
					EditorGUILayout.LabelField("Slots Used: " + inventory.slotsUsed + " of " + inventory.maxSlots);
					GUILayout.EndHorizontal ();

					GUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField("Selected Slot: " + selectedSlot);
					GUILayout.EndHorizontal ();

					GUILayout.BeginHorizontal ();
					EditorGUILayout.Space();
					GUILayout.EndHorizontal ();

					inventoryScrollPosition = GUILayout.BeginScrollView(inventoryScrollPosition);

					int row = 0;
					int displayedSlots = 0;
					int iteration = 0;
					for (row = 0; row < 4; ++row) {
						if (displayedSlots < inventory.maxSlots) {
							int currentItem = 0;
							GUILayout.BeginHorizontal();
							for (currentItem = 0; currentItem < 5; ++currentItem) {
								// Show 5 Items across
								if (displayedSlots < inventory.maxSlots) {
									GUIStyle style = new GUIStyle("button");
									if (selectedSlot == iteration) {
										style.border.left = 1;
										style.border.bottom = 1;
										style.border.top = 1;
										style.border.right = 1;
									}
									else {
										style.border.left = 0;
										style.border.bottom = 0;
										style.border.top = 0;
										style.border.right = 0;
									}
									try {
										if (GUILayout.Button(new GUIContent(DI_Item.ItemDatabase.getItemIcon(inventory.inventoryItems[iteration]), inventory.inventoryItems[iteration].description), style, GUILayout.Width(64), GUILayout.Height(64))) {
											selectedSlot = iteration;
											selectedItemIndex = 0;
											loadSlotData = true;
										}
										displayedSlots += inventory.inventoryItems[iteration].slots;
									}
									catch (Exception) {
										if (GUILayout.Button(getAddItemIcon(), style, GUILayout.Width(64), GUILayout.Height(64))) {
											selectedSlot = iteration;
											selectedItemIndex = 0;
											loadSlotData = true;
										}
										++displayedSlots;
									}
									++iteration;
								}
							}
							GUILayout.EndHorizontal ();
						}
					}
					GUILayout.EndScrollView();

					displayItem = EditorGUILayout.Foldout(displayItem, "Item:");
					if (displayItem) {
						EditorGUILayout.BeginHorizontal();
						if (loadSlotData) {
							try {
								if (inventory.inventoryItems[selectedSlot].itemName != "") {
									selectedItemIndex = DI_Item.ItemDatabase.getItemIndexFromName(inventory.inventoryItems[selectedSlot].itemName);
								}
							}
							catch (Exception) {
									selectedItemIndex = 0;
							}
							loadSlotData = false;
						}
						selectedItemIndex = EditorGUILayout.IntPopup(selectedItemIndex, itemNames.ToArray(), itemIndexes.ToArray(), GUILayout.ExpandWidth(false));

						if (GUI.changed) {
							if (selectedItemIndex != 0) {
								try {
									_item = inventory.inventoryItems[selectedSlot];
									Debug.Log("Loaded item from internal inventory");
								}
								catch (Exception) {
									_item = DI_Item.ItemDatabase.loadItemById(itemIds[selectedItemIndex - 1]);
									Debug.Log(_item.itemName);
									Debug.Log("Loaded item from item database");
								}
							}
						}

						EditorGUILayout.EndHorizontal();

						if (selectedItemIndex != 0) {
							EditorGUILayout.BeginHorizontal();
							if (_item.type == DI_Item.ItemTypes._ARMOR || _item.type == DI_Item.ItemTypes._WEAPON) {
								_item.currentDurability = EditorGUILayout.IntSlider("Durability", _item.currentDurability, 0, _item.maxDurability, GUILayout.ExpandWidth(false));
							}
							EditorGUILayout.EndHorizontal();

							EditorGUILayout.BeginHorizontal();
							if (_item.type == DI_Item.ItemTypes._ARMOR || _item.type == DI_Item.ItemTypes._WEAPON) {
								if (_item.ammoType != DI_Item.AmmoType._UNASSIGNED) {
									_item.currentAmmo = EditorGUILayout.IntSlider("Current Ammo", _item.currentAmmo, 0, _item.maxAmmo, GUILayout.ExpandWidth(false));
								}
							}
							EditorGUILayout.EndHorizontal();

							EditorGUILayout.BeginHorizontal();
							EditorGUILayout.EndHorizontal();

							EditorGUILayout.BeginHorizontal();
							EditorGUILayout.EndHorizontal();

							EditorGUILayout.BeginHorizontal();
							if (_item.stackable) {
								_item.stackSize = EditorGUILayout.IntSlider("Stack Size", _item.stackSize, 1, _item.maxStackSize, GUILayout.ExpandWidth(false));
							}
							EditorGUILayout.EndHorizontal();
						}

						if (GUI.changed) {
						}

						EditorGUILayout.BeginHorizontal();
						if (GUILayout.Button("Remove Item", GUILayout.ExpandWidth(false))) {
							inventory.removeItem(selectedSlot);
							selectedItemIndex = 0;
						}
						if (GUILayout.Button("Add Item", GUILayout.ExpandWidth(false))) {
							if (selectedItemIndex != 0) {
								inventory.setItem(_item, selectedSlot);
								Debug.Log("Saving item: " + _item.itemName + " to slot: " + selectedSlot);
							}
						}
						EditorGUILayout.EndHorizontal();
					}

					GUILayout.Space(5);

					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.Space();
					if (GUILayout.Button ("Delete", GUILayout.ExpandWidth(true), GUILayout.Height(15))) {
						if (EditorUtility.DisplayDialog("Confirm Deletion", "Are you sure you want to delete " + inventory.inventoryId + "?", "Yes", "No")) {
							DI_Item.InventoryManager.deleteInventory(inventory.inventoryId);
							if (currentInventory > 1) {
								--currentInventory;
							}
							else if (inventoryCount.Count > 1) {
								currentInventory = 1;
							}
							else {
								currentInventory = 0;
							}

							loadInventoryData();
							GUI.FocusControl ("clearFocus");
						}
					}

					if (GUILayout.Button ("Save", GUILayout.ExpandWidth(true), GUILayout.Height(15))) {
						DI_Item.InventoryManager.saveInventory(inventory, inventory.inventoryId);
						loadInventoryData();
						Debug.Log("Saving inventory id: " + inventory.inventoryId);
					}

					EditorGUILayout.Space();
					EditorGUILayout.EndHorizontal();

					EditorGUILayout.BeginHorizontal();
					GUILayout.Label("", GUILayout.Height(10));
					EditorGUILayout.EndHorizontal();
				}
			}
		}
	}
}
