// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// an extension of the current editorutility.
//------------------------------------------------------------------------------

using UnityEditor;
using UnityEngine;


public class EditorUtilities : Editor
{
	/*	TEST
	 * 
	 * Shows a texture with a label and a button to select a new image
	 * from a list of images loaded from the path specified. This should 
	 * allow a selection of an image from a subset of images.
	 * 
	 * param label :		 	label to display
	 * param selectedImage :	selected image to show on the interface
	 * param yPosition : 		how far down in the interface to show this tool
	 * param textureFieldPath: 	file path containing the images to load
	 * param functionHandler: 	function to handle the selection of a texture
	 * */
	public static void TexturePreviewWithSelection(string label, Texture selectedImage, float yPosition,
	                                               string textureFilePath, ImageHandler functionHandler)
	{
		TexturePreviewWithSelection (label, selectedImage, yPosition, new string[] {textureFilePath}, functionHandler);
	}

	/*	TEST
	 * 
	 * Shows a texture with a label and a button to select a new image
	 * from a list of images loaded from the path specified. This should 
	 * allow a selection of an image from a subset of images.
	 * 
	 * param label :		 	label to display
	 * param selectedImage :	selected image to show on the interface
	 * param yPosition : 		how far down in the interface to show this tool
	 * param textureFieldPaths: file paths containing the images to load
	 * param functionHandler: 	function to handle the selection of a texture */
	public static void TexturePreviewWithSelection(string label, Texture selectedImage, float yPosition,
	                                               string[] textureFilePaths, ImageHandler functionHandler)
	{
		EditorGUILayout.BeginVertical (GUILayout.Height(125));
		{
			EditorGUILayout.LabelField (label);
			EditorGUI.DrawPreviewTexture (new Rect(50, yPosition, 100,100), selectedImage);

			//center the select texture button
			EditorGUILayout.BeginVertical ();
			EditorGUILayout.Space ();
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.Space ();
			if (GUILayout.Button ("Select img", GUILayout.MaxWidth (100)))
			{
				EditorUtilities.TexturePicker(textureFilePaths, functionHandler);
			}
			EditorGUILayout.Space ();
			EditorGUILayout.EndHorizontal ();
			EditorGUILayout.EndVertical ();
		}
		EditorGUILayout.EndVertical ();
				 
	}

	/* Creates a window with buttons to select a new image
	 * 
	 * param path: 				path to load the images from
	 * param functionHandler:	how to handle the new image selection 	*/
	public static void TexturePicker(string path, ImageHandler functionHandler)
	{
		EditorUtilities.TexturePicker (new string[] {path}, functionHandler);
	}

	public static void TexturePicker(string[] paths, ImageHandler functionHandler)
	{
		TexturePicker picker = (TexturePicker)EditorWindow.GetWindow(typeof(TexturePicker), true, "Texture Picker");
		picker.Setup (paths, functionHandler);
	}
}