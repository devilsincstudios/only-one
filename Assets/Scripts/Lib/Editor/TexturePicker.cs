// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	a texture picker for multiplie purpose
// It generate an editor windows picker of a given images path
//

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public delegate void ImageHandler(Texture2D imageName);

public class TexturePicker: EditorWindow
{
	public List<Texture2D> images = new List<Texture2D>();

	// a flag to indicate if the editor window has been setup or not
	private bool isSetup = false;

	private ImageHandler handler;

#region Setup

	//attempts to setup the editor by reading in textures from specified path
	public void Setup(string path, ImageHandler functionHandler)
	{
		string[] paths = new string[] {path};
		Setup(paths, functionHandler);
	}

	//same as before, but this function support the multiple paths of textures
	public void Setup(string[] paths, ImageHandler functionHandler)
	{
		isSetup = true;
		ReadInAllTextures(paths);
		handler = functionHandler;
	}

#endregion Setup

#region GUI

	Vector2 scrollPosition = Vector2.zero;
	void OnGUI()
	{
		if (!isSetup)
			return;

		scrollPosition = EditorGUILayout.BeginScrollView (scrollPosition);

		//create a button for each image loaded in, 4 buttons in width
		// calls the handler when an image is selected.
		int counter = 0;
		foreach(Texture2D img in images)
		{
			if (counter % 4 == 0 || counter == 0)
				EditorGUILayout.BeginHorizontal ();
			++counter;

			if (GUILayout.Button (img, GUILayout.Height (100), GUILayout.Width(100)))
			{
				handler(img);
				EditorWindow.focusedWindow.Close ();
			}

			if (counter % 4 == 0)
				EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.EndScrollView ();
	}

#endregion GUI

#region Utility

	//reads the in all textures from the paths
	void ReadInAllTextures(string[] paths)
	{
		foreach(string path in paths)
		{
			string[] allFilesInPath = Directory.GetFiles (path);
			foreach(string filePath in allFilesInPath)
			{
				Texture2D obj = (Texture2D)AssetDatabase.LoadAssetAtPath (filePath, typeof(Texture2D));
				if (obj is Texture2D)
				{
					images.Add (obj as Texture2D);
				}
			}
		}
	}

#endregion Utility


}

