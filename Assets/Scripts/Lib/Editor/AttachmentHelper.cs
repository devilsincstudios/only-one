// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace DI_Editor
{
	[CustomEditor(typeof(DI_Bones.AttachmentPoints))]
	public class AttachmentHelper : Editor
	{
		private DI_Bones.AttachmentPoints attachmentPoints;
		private string[] typeNames;
		private GameObject bone;
		private List<DI_Bones.AttachmentTypes> types;
		private Color defaultColor;
		private bool showRigEditor = false;
		private List<DI_Bones.AttachmentPoint> rigPoints;

		public void SaveBones()
		{
			if (attachmentPoints.bonesIndex == null) {
				attachmentPoints.bonesIndex = new List<DI_Bones.AttachmentTypes>();
				attachmentPoints.bonesValues = new List<GameObject>();
			}
			else {
				attachmentPoints.bonesIndex.Clear();
				attachmentPoints.bonesValues.Clear();
			}
			
			foreach (KeyValuePair<DI_Bones.AttachmentTypes, GameObject> data in attachmentPoints.bones) {
				attachmentPoints.bonesIndex.Add(data.Key);
				attachmentPoints.bonesValues.Add(data.Value);
			}
			EditorUtility.SetDirty(target);
		}

		public void SerializeBones()
		{
			if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying) {
				SaveBones();
			}
		}

		public void DeSerialzeBones()
		{
			if (attachmentPoints.bonesIndex != null) {
				if (attachmentPoints.bones == null) {
					attachmentPoints.bones = new Dictionary<DI_Bones.AttachmentTypes, GameObject>();
				}
				for (int index = 0; index < attachmentPoints.bonesIndex.Count; ++index) {
					setRigPoint(attachmentPoints.bonesIndex[index], attachmentPoints.bonesValues[index]);
				}
			}
		}

		public void OnEnable()
		{
			attachmentPoints = (DI_Bones.AttachmentPoints)target;
			DeSerialzeBones();
			typeNames = Enum.GetNames(typeof(DI_Bones.AttachmentTypes));
			types = new List<DI_Bones.AttachmentTypes>();
			foreach (string type in typeNames) {
				types.Add((DI_Bones.AttachmentTypes) Enum.Parse(typeof(DI_Bones.AttachmentTypes), type));
			}
			defaultColor = GUI.color;
			if (attachmentPoints.rigNode == null) {
				attachmentPoints.rigNode = attachmentPoints.gameObject;
			}

			rigPoints = new List<DI_Bones.AttachmentPoint>();
			loadBoneNames();

			EditorApplication.playmodeStateChanged += SerializeBones;
		}
		
		public void setRigPoint(DI_Bones.AttachmentTypes type, GameObject bone)
		{
			GameObject previousEntry;
			//UnityEngine.Debug.Log("setRig called with " + type.ToString() + " " + bone.name);
			//Debug.Log(attachmentPoints.bones.ToString());
			if (attachmentPoints.bones.TryGetValue(type, out previousEntry)) {
				attachmentPoints.bones[type] = bone;
			}
			else {
				attachmentPoints.bones.Add(type, bone);
			}
			attachmentPoints.setBone(type, bone);
		}

		public void loadBoneNames()
		{
			foreach (Type type in typeof(DI_Bones.AttachmentPoint).Assembly.GetTypes()) {
				if (type.IsSubclassOf(typeof(DI_Bones.AttachmentPoint))) {
					//UnityEngine.Debug.Log(type.ToString());
					DI_Bones.AttachmentPoint pointData = (DI_Bones.AttachmentPoint)Activator.CreateInstance(type);
					rigPoints.Add(pointData);
				}
			}
		}

		public override void OnInspectorGUI()
		{
			EditorGUIUtility.LookLikeControls();
			EditorGUILayout.BeginVertical();

			EditorGUILayout.BeginHorizontal();
			showRigEditor = EditorGUILayout.Foldout(showRigEditor, "Rig Binding Editor");
			EditorGUILayout.EndHorizontal();

			if (showRigEditor) {
				EditorGUILayout.BeginVertical();
				int iteration = 0;
				foreach (DI_Bones.AttachmentTypes attachmentPoint in types) {
					GameObject bone;
					if (!attachmentPoints.bones.TryGetValue(attachmentPoint, out bone)) {
						bone = attachmentPoints.gameObject;
					}
					EditorGUILayout.BeginHorizontal();
					if (bone.name == attachmentPoints.gameObject.name) {
						GUI.color = Color.red;
					}
					else {
						GUI.color = defaultColor;
					}
					bone = (GameObject) EditorGUILayout.ObjectField(typeNames[iteration], bone, typeof(GameObject), true);
					attachmentPoints.setBone(attachmentPoint, bone);
					EditorGUILayout.EndHorizontal();
					++iteration;
				}
				EditorGUILayout.EndVertical();
			}

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Separator();
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			if (attachmentPoints.rigNode.name == attachmentPoints.gameObject.name) {
				GUI.color = Color.red;
			}
			else {
				GUI.color = defaultColor;
			}
			attachmentPoints.rigNode = (GameObject) EditorGUILayout.ObjectField("Character Rig Node", attachmentPoints.rigNode, typeof(GameObject), true);
			EditorGUILayout.EndHorizontal();

			GUI.color = defaultColor;

			EditorGUILayout.Separator();
			if (attachmentPoints.rigNode.name == attachmentPoints.gameObject.name) {
				EditorGUILayout.BeginHorizontal();
				GUI.color = Color.red;
				EditorGUILayout.LabelField("Notice: Not setting the rig node is likely to cause a bad auto map.");
				EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Space();
			if (GUILayout.Button("Attempt Automapping")) {


				Transform[] children = attachmentPoints.rigNode.GetComponentsInChildren<Transform>();
				foreach (Transform node in children) {
					GameObject nodeObject = node.gameObject;
					string nodeName = node.name.ToLower();

					bool matchFound = false;
					foreach (DI_Bones.AttachmentPoint rigPoint in rigPoints.ToArray()) {
						foreach (string boneName in rigPoint.aliases.ToArray()) {
							string cleanBoneName = nodeName;
							if (cleanBoneName.Contains(":")) {
								cleanBoneName = cleanBoneName.Split(':')[1];
							}
							cleanBoneName = cleanBoneName.ToLower();
							if (boneName.ToLower() == cleanBoneName) {
								setRigPoint(rigPoint.attachmentPoint, nodeObject);
								if (rigPoint.collider != null) {
									rigPoint.collider.addCollider(nodeObject);
								}
								matchFound = true;
								break;
							}
							if (matchFound) {
								break;
							}
						}
					}
				}
				SaveBones();
			}
			GUI.color = defaultColor;
			EditorGUILayout.Space();
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();
		}
	}
}