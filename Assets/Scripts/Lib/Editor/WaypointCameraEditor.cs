using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	WAYPOINT CAMERA EDITOR
//
//	This class makes it easier to make and edit a waypoint list for the WP camera


namespace DI_Editor
{
	[CustomEditor(typeof(DI_Camera.WaypointCamera))]
	public class WaypointCameraEditor : Editor 
	{
		/*******************************************************************************************************************************
		MEMBERS
		*******************************************************************************************************************************/
		private const float HANDLE_SIZE = 0.7f;	
		private float WPbuttonWidth = 50f; //width of the WP up,down,delete buttons
		private static int selectedIndex = 0; //Index of the selected waypoint in the editor
	
		public WaypointCameraEditor()
		{
		}
		/*******************************************************************************************************************************
		ON INSPECTOR GUI
		*******************************************************************************************************************************/
		public override void OnInspectorGUI()
		{		
			//get accompanying script
			DI_Camera.WaypointCamera camera = (DI_Camera.WaypointCamera) target;
			
			EditorGUILayout.BeginVertical();
			
			EditorGUILayout.HelpBox ("To view a preview of the selected waypoint please open the camera component of the gameobject",MessageType.Info);
			
			//a text field for collection of waypoints title
			camera.waypointsID = EditorGUILayout.TextField ("Waypoints ID", camera.waypointsID);
			if(camera.waypointsID.Length >= 20)
			{	
				camera.waypointsID.Remove(20);
			}
			
			EditorGUILayout.Space ();
			
			//colour of the scenve view handles
			camera.editorColour = EditorGUILayout.ColorField("Waypoints Color",camera.editorColour );	
				
			EditorGUILayout.Space ();
			
			//toggle whether camera should loop through waypoints again when finished
			camera.loop = EditorGUILayout.Toggle ("Loop", camera.loop);
			
			EditorGUILayout.Space ();

			//CAN ONLY EDIT WHEN CAMERA ISN'T RUNNING
			if (Application.isPlaying == false)
			{
				//button to add a new waypoint to the list
				if (GUILayout.Button ("Add Waypoint")) 
				{
					camera.waypoints.Add (new DI_Camera.CameraWaypoint ());
					camera.waypoints [camera.waypoints.Count - 1].ID = string.Format ("Waypoint {0}", camera.waypoints.Count);
					camera.waypoints [camera.waypoints.Count - 1].activeInInspector = true;
					selectedIndex = camera.waypoints.Count - 1;
				}
	
				for (int i = 0; i < camera.waypoints.Count; i++) 
				{
					DrawWaypointInspector (i);
				}
	
				EditorGUILayout.Space ();

				//button to serialize the collection of WP		
				if (GUILayout.Button ("Save")) 
				{
					camera.Save ();			
				}
				
				if(camera.waypointsID == "")
				{
					EditorGUILayout.HelpBox ("Please ID the camera waypoints",MessageType.Warning);
				}
				
				else
				{
					EditorGUILayout.HelpBox (string.Format ("Click 'Save' to save waypoints to \"Assets/Camera Waypoint Sets/{0}.xml\"", camera.waypointsID), MessageType.Info);
				}
				
				EditorGUILayout.Space ();
				EditorGUILayout.Space ();

				//button to load waypoints into the list from file	
				if (GUILayout.Button ("Load")) 
				{
					camera.Load();
										
					camera.transform.position = camera.waypoints [selectedIndex].position;
					camera.transform.rotation = camera.waypoints [selectedIndex].rotation;
				}
								
				camera.file = (TextAsset)EditorGUILayout.ObjectField ("Waypoints File", camera.file, typeof(TextAsset), false);
				
				if(camera.file == null)
				{
					EditorGUILayout.HelpBox ("If waypoints need to be loaded from a valid waypoint set .XML file, select it above",MessageType.Warning);
				}
				else
				{
					EditorGUILayout.HelpBox ("Click 'Load' to load saved waypoints from the xml file", MessageType.Info);
				}
			}
			EditorGUILayout.EndVertical ();
		}
		
		/*******************************************************************************************************************************
		DRAW WAYPOINT INSPECTOR
		Draws an editable inspector for an individual WP node so user can edit values easily
		*******************************************************************************************************************************/
		public void DrawWaypointInspector(int i)
		{
			DI_Camera.WaypointCamera camera = (DI_Camera.WaypointCamera) target;
			EditorGUILayout.BeginHorizontal();
			//bool enabling foldout functionality		
			camera.waypoints[i].activeInInspector = EditorGUILayout.Foldout(camera.waypoints[i].activeInInspector,camera.waypoints[i].ID);		
			GUIClickTest(i);
			
			//button to move WP up in list
			if(GUILayout.Button ("Up", GUILayout.Width(WPbuttonWidth)))
			{
				//check so we dont move first WP up
				if(i > 0)
				{
					DI_Camera.CameraWaypoint temp = camera.waypoints[i];
					camera.waypoints[i] = camera.waypoints[i - 1];
					camera.waypoints[i - 1] = temp;
					camera.transform.position = camera.waypoints [selectedIndex].position;
					camera.transform.rotation = camera.waypoints [selectedIndex].rotation;
				}
			}
			//button to move WP down in list
			if(GUILayout.Button ("Down", GUILayout.Width(WPbuttonWidth)))
			{
				//check see if we dont move last WP down
				if( (i + 1) < camera.waypoints.Count)
				{
					DI_Camera.CameraWaypoint temp = camera.waypoints[i];
					camera.waypoints[i] = camera.waypoints[i + 1];
					camera.waypoints[i + 1] = temp;
					camera.transform.position = camera.waypoints [selectedIndex].position;
					camera.transform.rotation = camera.waypoints [selectedIndex].rotation;
				}
			}
			//button to delete WP from list
			if(GUILayout.Button ("Delete", GUILayout.Width(WPbuttonWidth)))
			{
				camera.waypoints.RemoveAt(i);

				if(camera.waypoints.Count <= 0)
				{
					selectedIndex = 0;
					//Reset transform component to default as it will not have a waypoint to set its position
					camera.transform.position = Vector3.zero;
					camera.transform.rotation = Quaternion.identity;
					return;
				}

				//If the WP being removed is the selected WP and it was bottom of the list
				if(selectedIndex >= camera.waypoints.Count )
				{	
					//select the last WP
					selectedIndex = camera.waypoints.Count - 1;
				}
				camera.transform.position = camera.waypoints [selectedIndex].position;
				camera.transform.rotation = camera.waypoints [selectedIndex].rotation;

				return;
			} 
			EditorGUILayout.EndHorizontal ();

			//if the foldout is open
			if(camera.waypoints[i].activeInInspector)
			{
				if(selectedIndex != i)
				{
					//GUI to edit properties of the WP
					EditorGUILayout.BeginVertical ();
					camera.waypoints[i].ID = EditorGUILayout.TextField("ID",camera.waypoints[i].ID);
					GUIClickTest (i);
					camera.waypoints[i].position = EditorGUILayout.Vector3Field("Position",camera.waypoints[i].position);
					GUIClickTest (i);
					camera.waypoints[i].rotation.eulerAngles = EditorGUILayout.Vector3Field ("Rotation", camera.waypoints[i].rotation.eulerAngles);
					GUIClickTest (i);
					camera.waypoints[i].speed = EditorGUILayout.Slider ("Speed",camera.waypoints[i].speed, 0f, DI_Camera.CameraWaypoint.INSTANT_TRANSITION);
					GUIClickTest (i);
					camera.waypoints[i].rotationSpeed = EditorGUILayout.Slider ("Rotation Speed",camera.waypoints[i].rotationSpeed, 0f, DI_Camera.CameraWaypoint.INSTANT_TRANSITION);
					GUIClickTest (i);
					camera.waypoints[i].waitForEvent = EditorGUILayout.Toggle("Leave On Event", camera.waypoints[i].waitForEvent);
					GUIClickTest (i);
					if(camera.waypoints[i].waitForEvent == false)
					{
						//will only need delay value if this WP isn't triggered by an event
						camera.waypoints[i].delay = EditorGUILayout.FloatField("Delay",camera.waypoints[i].delay);
						GUIClickTest (i);
					}
				}

				else
				{
					//GUI to edit properties of the WP
					EditorGUILayout.BeginVertical ();
					camera.waypoints[i].ID = EditorGUILayout.TextField("ID",camera.waypoints[i].ID);
					
					camera.waypoints[i].position = EditorGUILayout.Vector3Field("Position",camera.waypoints[i].position);
					camera.waypoints[i].rotation.eulerAngles = EditorGUILayout.Vector3Field ("Rotation", camera.waypoints[i].rotation.eulerAngles);

					//Update transform component if the selected WP transform is changed as they should be the same
					camera.transform.position = camera.waypoints[i].position;
					camera.transform.rotation = camera.waypoints[i].rotation;
					
					camera.waypoints[i].speed = EditorGUILayout.Slider ("Speed",camera.waypoints[i].speed, 0f, DI_Camera.CameraWaypoint.INSTANT_TRANSITION);
					camera.waypoints[i].rotationSpeed = EditorGUILayout.Slider ("Rotation Speed",camera.waypoints[i].rotationSpeed, 0f, DI_Camera.CameraWaypoint.INSTANT_TRANSITION);
					camera.waypoints[i].waitForEvent = EditorGUILayout.Toggle("Leave On Event", camera.waypoints[i].waitForEvent);
					if(camera.waypoints[i].waitForEvent == false)
					{
						//will only need delay value if this WP isn't triggered by an event
						camera.waypoints[i].delay = EditorGUILayout.FloatField("Delay",camera.waypoints[i].delay);
					}
				}
				EditorGUILayout.EndVertical ();
			}
		}
		
		/*******************************************************************************************************************************
		ON SCENE GUI
		Enables waypoints to be enabled in the scene view using handles
		*******************************************************************************************************************************/
		public void OnSceneGUI()
		{			
			DI_Camera.WaypointCamera camera = (DI_Camera.WaypointCamera) target;
			Handles.color = camera.editorColour;
			
			for(int i = 0; i < camera.waypoints.Count; i++)
			{						
				//draws a cone to represent WP 
				Handles.ConeCap (i,camera.waypoints[i].position,camera.waypoints[i].rotation,HANDLE_SIZE);
				//an ID label for the waypoint
				Handles.Label(camera.waypoints[i].position + Vector3.up * 0.5f,camera.waypoints[i].ID);	
				
				Event e = Event.current;
				//If there was a left click 
				if(e.isMouse && e.button == 0)
				{		
					//and it was near or around the WP			
					Vector2 GUIpoint = HandleUtility.WorldToGUIPoint(camera.waypoints[i].position);
					Rect temp = new Rect(GUIpoint.x - 5, GUIpoint.y - 5, 10, 10);
					if(temp.Contains(e.mousePosition))
					{
						selectedIndex = i;
						camera.transform.position = camera.waypoints [selectedIndex].position;
						camera.transform.rotation = camera.waypoints [selectedIndex].rotation;
					}	
				}
			}
			
			for(int i = 0; i < camera.waypoints.Count - 1; i++)
			{
				//Draws a connecting line between all nodes in the list
				Handles.DrawLine(camera.waypoints[i].position, camera.waypoints[i + 1].position);
				
				//Draw a cone halfway between WPs indicating direction of line
				Vector3 direction = camera.waypoints[i+1].position - camera.waypoints[i].position;
				float halfDistance = direction.magnitude * 0.5f;
				direction.Normalize();
				
				Handles.ConeCap(camera.waypoints.Count + 1 * 5, camera.waypoints[i].position + (direction * halfDistance), Quaternion.LookRotation(direction),0.4f);				
			}
		
			/*The selected waypoint will be moved with the transform. This means handles in the scene view can be used
			to manipulate the selected WP and a preview of this WP position will be drawn in a window  */
			if (camera.GetComponent<Transform> () != null && camera.waypoints.Count > 0 && Application.isPlaying == false) 
			{
				camera.waypoints [selectedIndex].position = camera.transform.position;
				camera.waypoints [selectedIndex].rotation = camera.transform.rotation;
			}
					
			//Keeps the scene handles up to date
			HandleUtility.Repaint();
		}
		
		/*******************************************************************************************************************************
		GUI CLICK TEST
		This tests to see if the user has clicked on one of the gui interfaces, if so the WP that is linked to that interface is tracked
		*******************************************************************************************************************************/
		private bool GUIClickTest(int index)	
		{
			Event e = Event.current;
			
			//if the there was a left mouse button click within the last draw GUI rect
			if(e.isMouse && e.button == 0 && GUILayoutUtility.GetLastRect().Contains(e.mousePosition))
			{
				DI_Camera.WaypointCamera camera = (DI_Camera.WaypointCamera) target;
				//the index to the selected waypoint is updated
				selectedIndex = index;

				if(camera.GetComponent<Transform>() != null)
				{
					/*move gameobject's transform to selected waypoint to enable handle manipulation of waypoint and
					the camera component will handle drawing of a preview of the waypoint pos*/
					camera.gameObject.transform.position = camera.waypoints[selectedIndex].position;
					camera.gameObject.transform.rotation = camera.waypoints[selectedIndex].rotation;
				}
				return true;
			}
			
			return false;
		}	
	}
}
