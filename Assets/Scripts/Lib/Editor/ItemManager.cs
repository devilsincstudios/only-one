// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	This is the Item Manager Tool
// 	it handle the window tool called "item manager editor"
//	and allow the users to create and manage all the items needed on the game. 
//	Every item will be stored into an external file
//	and can be loaded whenever the user want.
//	TODO: 	- find item by name
//			- "create prefab" button
//
//	original work written by Nicola Valcasara

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

using System;

namespace DI_Editor {
	public class ItemManager : EditorWindow {
		
		public DI_Item.ItemDatabase database;
		public List<DI_Item.Item> items;
		int viewIndex =1;
		Vector2 scroll;
		Texture2D imgInventory;
		Rigidbody imgMesh;

		private bool showBasicInfo = false;
		private bool showEquipInfo = false;
		private bool showConsumableInfo = false;
		private bool showMissionInfo = false;
		private List<String> itemNames;
		private List<int> itemIndexes;

		[MenuItem("How Long/Items/Item Manager")]
		public static void Init() 
		{
			EditorWindow.GetWindow (typeof(ItemManager));
		}

		public void OnEnable()
		{
			database = DI_Item.ItemDatabase.getDatabase ();
			items= database.itemList;
			this.title = "Item Editor";
			itemNames = new List<String>();
			itemIndexes = new List<int>();
			updateItemNames();
		}
		
		public void updateItemNames()
		{
			itemNames.Clear();
			itemIndexes.Clear();

			for (int iteration = 0; iteration < items.Count; ++iteration) {
				itemNames.Add(items[iteration].itemName);
				itemIndexes.Add(iteration + 1);
			}
		}

		public void OnGUI()
		{
			GUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField("Current Item:");
			viewIndex = EditorGUILayout.IntPopup(viewIndex, itemNames.ToArray(), itemIndexes.ToArray());

			if (viewIndex > items.Count)
				viewIndex = items.Count;
			if (viewIndex < 1)
				viewIndex = 1;

			if (GUILayout.Button("Previous", GUILayout.ExpandWidth (false))) {
				if (viewIndex > 1)
					viewIndex--;
				GUI.FocusControl ("clearFocus");
			}

			if (viewIndex < items.Count) {
				if (GUILayout.Button ("Next", GUILayout.ExpandWidth (false))) {
					if (viewIndex < items.Count) {
						viewIndex++;
					}
					GUI.FocusControl("clearFocus");
				}
			}
			else {
				if (GUILayout.Button ("New", GUILayout.ExpandWidth (false))) {
					if (viewIndex == items.Count) {
						items.Add (new DI_Item.Item());
						updateItemNames();
						viewIndex++;
					}
					GUI.FocusControl("clearFocus");
				}
			}
			GUILayout.EndHorizontal ();
			
			GUI.SetNextControlName ("Item name");
			try {
				showBasicInfo = EditorGUILayout.Foldout(showBasicInfo, "Basic Info "  + items[viewIndex-1].itemName);
				if (showBasicInfo) {
					items[viewIndex-1].itemName = EditorGUILayout.TextField ("Name", items[viewIndex-1].itemName as String);
					items[viewIndex-1].itemId = viewIndex - 1;

					GUILayout.BeginHorizontal();
					GUILayout.BeginVertical ();
					imgInventory = EditorGUILayout.ObjectField ("Icon", Resources.Load<Texture2D>(items[viewIndex-1].imgItem_path), typeof (Texture2D), false) as Texture2D;
					if (imgInventory != null) {
						items[viewIndex-1].imgItem_path = "Icons/Items/" + imgInventory.name;
					}

					GUILayout.EndVertical ();
					GUILayout.EndHorizontal ();
					
					GUILayout.BeginVertical();
					GUILayout.BeginHorizontal();
					imgMesh = EditorGUILayout.ObjectField ("RigidBody", (Rigidbody)AssetDatabase.LoadAssetAtPath (items[viewIndex-1].meshItem_path, typeof(Rigidbody)), typeof(Rigidbody), false, GUILayout.ExpandWidth (false)) as Rigidbody;
					items[viewIndex-1].meshItem_path = AssetDatabase.GetAssetPath (imgMesh);
					EditorGUILayout.LabelField("Slots:", GUILayout.Width(50));
					items[viewIndex-1].slots = EditorGUILayout.IntPopup(items[viewIndex-1].slots, new string[] {"One", "Two", "Three"}, new int[] {1, 2, 3}, GUILayout.ExpandWidth (false));
					GUILayout.EndHorizontal();
					GUILayout.EndVertical();

					GUILayout.BeginHorizontal();
					items[viewIndex-1].weight = EditorGUILayout.FloatField("Weight", items[viewIndex-1].weight);
					items[viewIndex-1].value = EditorGUILayout.FloatField("Value", items[viewIndex-1].value);
					GUILayout.EndHorizontal();

					GUILayout.BeginHorizontal();
					if (items[viewIndex-1].stackable) {
						items[viewIndex-1].maxStackSize = EditorGUILayout.IntField("Max Stack Size", items[viewIndex-1].maxStackSize);
						items[viewIndex-1].stackable = EditorGUILayout.ToggleLeft("Stackable", items[viewIndex-1].stackable);
					}
					else {
						GUIStyle style = new GUIStyle();
						style.alignment = TextAnchor.MiddleRight;
						//GUILayout.Space(200);
						items[viewIndex-1].stackable = EditorGUILayout.ToggleLeft("Stackable", items[viewIndex-1].stackable, style);
					}
					GUILayout.EndHorizontal();

					items[viewIndex-1].type = (DI_Item.ItemTypes)EditorGUILayout.EnumPopup("Type", items[viewIndex-1].type);
					GUILayout.BeginHorizontal ();
					if (items[viewIndex-1].type == DI_Item.ItemTypes._AMMO) {
						items[viewIndex-1].ammoType = (DI_Item.AmmoType)EditorGUILayout.EnumPopup("Ammo Type", items[viewIndex-1].ammoType);
					}
					GUILayout.EndHorizontal();

					GUILayout.BeginHorizontal ();
					EditorGUILayout.LabelField ("Description", GUILayout.MaxWidth (70));
					scroll = EditorGUILayout.BeginScrollView (scroll);
					EditorStyles.textField.wordWrap = true;
					items[viewIndex-1].description = EditorGUILayout.TextArea (items[viewIndex-1].description, GUILayout.Height(50));
					EditorGUILayout.EndScrollView ();
					GUILayout.EndHorizontal();
				}

				switch (items[viewIndex-1].type) {
					case DI_Item.ItemTypes._WEAPON:
					case DI_Item.ItemTypes._ARMOR:
						showEquipInfo = EditorGUILayout.Foldout(showEquipInfo, "Equipment Info");
						items[viewIndex-1].bIsEquipable = true;
						if (showEquipInfo) {
							if (items[viewIndex-1].type == DI_Item.ItemTypes._WEAPON) {
								GUILayout.BeginHorizontal ();	
									items[viewIndex-1].minDamage = EditorGUILayout.FloatField("Min Damage", items[viewIndex-1].minDamage);
									items[viewIndex-1].maxDamage = EditorGUILayout.FloatField("Max Damage", items[viewIndex-1].maxDamage);
								GUILayout.EndHorizontal();

								GUILayout.BeginHorizontal ();
									items[viewIndex-1].ammoType = (DI_Item.AmmoType)EditorGUILayout.EnumPopup("Ammo Type", items[viewIndex-1].ammoType);
								GUILayout.EndHorizontal();
								if (items[viewIndex-1].ammoType != DI_Item.AmmoType._UNASSIGNED) {
								    GUILayout.BeginHorizontal ();
										items[viewIndex-1].maxAmmo = EditorGUILayout.IntField("Max Ammo", items[viewIndex-1].maxAmmo);
									GUILayout.EndHorizontal();
								}
							}
							else {
								GUILayout.BeginHorizontal ();
									items[viewIndex-1].damageReduction = EditorGUILayout.FloatField("Damage Reduction", items[viewIndex-1].damageReduction);
								GUILayout.EndHorizontal();
							}
							GUILayout.BeginHorizontal ();
								items[viewIndex-1].mountPoint = (DI_Bones.AttachmentTypes)EditorGUILayout.EnumPopup("Mount Point", items[viewIndex-1].mountPoint);
							GUILayout.EndHorizontal();

							GUILayout.BeginHorizontal ();
								items[viewIndex-1].maxDurability = EditorGUILayout.IntField("Max Durability", items[viewIndex-1].maxDurability);
							GUILayout.EndHorizontal();
						}
					break;

					case DI_Item.ItemTypes._CONSUMABLE:
						showConsumableInfo = EditorGUILayout.Foldout(showConsumableInfo, "Consumable Info");
						items[viewIndex-1].bIsConsumable = true;
						if (showConsumableInfo) {
							GUILayout.BeginHorizontal ();
								items[viewIndex-1].effect = (DI_Item.ConsumableEffect)EditorGUILayout.EnumPopup("Consumable Effect", items[viewIndex-1].effect);
							GUILayout.EndHorizontal();
							
							if (items[viewIndex-1].effect == DI_Item.ConsumableEffect._HEAL_HP
						    || items[viewIndex-1].effect == DI_Item.ConsumableEffect._HEAL_STAM
						    || items[viewIndex-1].effect == DI_Item.ConsumableEffect._HEAL_BOTH) {
								GUILayout.BeginHorizontal ();
									items[viewIndex-1].amountHealed = EditorGUILayout.FloatField("Healing Amount", items[viewIndex-1].amountHealed);
								GUILayout.EndHorizontal();
							}
							else if (items[viewIndex-1].effect == DI_Item.ConsumableEffect._BUFF) {
								GUILayout.BeginHorizontal ();
									items[viewIndex-1].statusEffect = (DI_Game.StatusTypes)EditorGUILayout.EnumPopup("Status Effect", items[viewIndex-1].statusEffect);
									items[viewIndex-1].buffTime = EditorGUILayout.FloatField("Buff Time", items[viewIndex-1].buffTime);
								GUILayout.EndHorizontal();
							}
							else if (items[viewIndex-1].effect == DI_Item.ConsumableEffect._CURE) {
								GUILayout.BeginHorizontal ();
									items[viewIndex-1].statusEffect = (DI_Game.StatusTypes)EditorGUILayout.EnumPopup("Status Effect", items[viewIndex-1].statusEffect);
								GUILayout.EndHorizontal();
							}
						}
					break;

					case DI_Item.ItemTypes._MISSION:
						showMissionInfo = EditorGUILayout.Foldout(showMissionInfo, "Mission Info");
						items[viewIndex-1].bIsMission = true;
						if (showMissionInfo) {
						}
					break;

					case DI_Item.ItemTypes._AMMO:
					break;
					case DI_Item.ItemTypes._JUNK:
					break;
					case DI_Item.ItemTypes._PART:
					case DI_Item.ItemTypes._TRAP:
					break;
				}

//				switch(items[viewIndex-1].type) {
//				case DI_Item.ItemTypes._AMMO:
//					items[viewIndex-1].ammoType = (DI_Item.AmmoType)EditorGUILayout.EnumPopup("Type", items[viewIndex-1].ammoType);
//					items[viewIndex-1].minDamage = EditorGUILayout.FloatField ("Damage", items[viewIndex-1].minDamage);
//					items[viewIndex-1].maxDamage = EditorGUILayout.FloatField ("Damage", items[viewIndex-1].maxDamage);
//					break;
//				case DI_Item.ItemTypes._HEALING:
//					items[viewIndex-1].amountHealed = EditorGUILayout.FloatField ("Health damage", items[viewIndex-1].amountHealed);
//					break;
//				}

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.Space();
				if (GUILayout.Button ("delete", GUILayout.ExpandWidth(true))) {
					if (EditorUtility.DisplayDialog("Confirm Deletion", "Are you sure you want to delete " + items[viewIndex-1].itemName + "?", "Yes", "No")) {
						if (items.Count ==1) {
							DI_Item.ItemDatabase.createNewDatabase ();
						}
						else {
							items.Remove (items[viewIndex-1]);
							GUI.FocusControl ("clearFocus");
						}
						updateItemNames();
					}
				}
				if (GUILayout.Button ("save", GUILayout.ExpandWidth(true))) {
					DI_Item.ItemDatabase.saveDatabase(database);
					updateItemNames();
				}
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.Space();
			}
			catch (Exception err) {
				Debug.LogException(err);
			}
		}
	}
}
