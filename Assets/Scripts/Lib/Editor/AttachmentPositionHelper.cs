// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Web;
using System.IO;
using System.Xml.Linq;

namespace DI_Editor
{
	[CustomEditor(typeof(DI_Bones.AttachToPlayer))]
	public class AttachmentPositionHelper : Editor
	{
		private DI_Bones.AttachToPlayer attachToPlayer;
		private GameObject bone;
		private DI_Bones.AttachmentPoints attachmentPoints;
		private string playerName;
		private Color originalColor;

		public void OnEnable()
		{
			attachToPlayer = (DI_Bones.AttachToPlayer) target;
			playerName = "";
			originalColor = GUI.color;
		}

		public void updateOffsets()
		{
			attachToPlayer.attachmentOffsetPositon = attachToPlayer.gameObject.transform.localPosition;
			attachToPlayer.attachmentOffsetRotation = new Vector3(
				attachToPlayer.gameObject.transform.localRotation.x, 
				attachToPlayer.gameObject.transform.localRotation.y,
				attachToPlayer.gameObject.transform.localRotation.z
			);
		}

		public override void OnInspectorGUI()
		{
			EditorGUIUtility.LookLikeControls();

			EditorGUILayout.BeginVertical();
			attachToPlayer.player = (GameObject) EditorGUILayout.ObjectField("Player", attachToPlayer.player, typeof(GameObject), true);
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			attachToPlayer.attachmentPoint = (DI_Bones.AttachmentTypes) EditorGUILayout.EnumPopup("Attachment Point", attachToPlayer.attachmentPoint);
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			if (GUILayout.Button("Move to attachment point.")) {
				if (attachToPlayer.player != null) {
					// User has updated the player object.
					if (attachToPlayer.player.name != playerName) {
						playerName = attachToPlayer.player.name;
						try {
							attachmentPoints = attachToPlayer.player.GetComponent<DI_Bones.AttachmentPoints>();
							bone = attachmentPoints.getBone(attachToPlayer.attachmentPoint);
							attachToPlayer.gameObject.transform.parent = bone.transform;
							attachToPlayer.gameObject.transform.localPosition = Vector3.zero;
							attachToPlayer.gameObject.transform.localRotation = Quaternion.identity;
						}
						catch (Exception error) {
							Debug.LogException(error);
						}
					}
				}
			}
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			if (GUILayout.Button("Update offsets")) {
				updateOffsets();
			}
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			if (GUILayout.Button("Save offsets")) {
				updateOffsets();
				saveOffsets(attachToPlayer.gameObject.name);
			}
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
			if (!File.Exists("Settings\\" + attachToPlayer.gameObject.name + ".xml")) {
				GUI.color = Color.red;
				GUI.enabled = false;
			}

			if (GUILayout.Button("Load offsets")) {
				loadOffsets(attachToPlayer.gameObject.name);
			}

			GUI.enabled = true;
			EditorGUILayout.EndVertical();

			GUI.color = originalColor;
			EditorGUILayout.BeginVertical();
				GUI.enabled = false;
				EditorGUILayout.Vector3Field("Local Position Offset", attachToPlayer.attachmentOffsetPositon);
			EditorGUILayout.EndVertical();

			EditorGUILayout.BeginVertical();
				EditorGUILayout.Vector3Field("Local Rotation Offset", attachToPlayer.attachmentOffsetRotation);
				GUI.enabled = true;
			EditorGUILayout.EndVertical();
		}

		private void saveOffsets(string objectName) {
			try {
				XElement root = new XElement("root");
				XElement objectRoot = new XElement(objectName);
				XElement positionOffset = new XElement("position");
				positionOffset.SetAttributeValue("x", attachToPlayer.attachmentOffsetPositon.x);
				positionOffset.SetAttributeValue("y", attachToPlayer.attachmentOffsetPositon.y);
				positionOffset.SetAttributeValue("z", attachToPlayer.attachmentOffsetPositon.z);
				XElement rotationOffset = new XElement("rotation");
				rotationOffset.SetAttributeValue("x", attachToPlayer.attachmentOffsetRotation.x);
				rotationOffset.SetAttributeValue("y", attachToPlayer.attachmentOffsetRotation.y);
				rotationOffset.SetAttributeValue("z", attachToPlayer.attachmentOffsetRotation.z);

				objectRoot.Add(positionOffset);
				objectRoot.Add(rotationOffset);
				root.Add(objectRoot);

				if (!Directory.Exists("Settings\\")) {
					Directory.CreateDirectory("Settings\\");
				}
				root.Save("Settings\\" + objectName + ".xml");
			}
			catch (Exception error) {
				Debug.LogException(error);
			}
		}

		private void loadOffsets(string objectName)
		{
			if (File.Exists("Settings\\" + objectName + ".xml")) {
				try {
					XElement config = XElement.Load("Settings\\" + objectName + ".xml");
					XElement objectRoot = config.Element(objectName);
					XElement position = objectRoot.Element("position");
					attachToPlayer.attachmentOffsetPositon.x = float.Parse(position.Attribute("x").Value);
					attachToPlayer.attachmentOffsetPositon.y = float.Parse(position.Attribute("y").Value);
					attachToPlayer.attachmentOffsetPositon.z = float.Parse(position.Attribute("z").Value);
					XElement rotation = objectRoot.Element("rotation");
					attachToPlayer.attachmentOffsetRotation.x = float.Parse(rotation.Attribute("x").Value);
					attachToPlayer.attachmentOffsetRotation.y = float.Parse(rotation.Attribute("y").Value);
					attachToPlayer.attachmentOffsetRotation.z = float.Parse(rotation.Attribute("z").Value);
				}
				catch (Exception error) {
					Debug.LogException(error);
				}
			}
		}
	}
}
