// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace DI_Game
{
	public static class ScoreIndex
	{
		public static List<Score> scores = new List<Score>();

		public static string serialize()
		{
			return BinarySerializer<List<Score>>.serialize(scores);
		}

		public static List<Score> deserialize(string scoreIndexString)
		{
			return BinarySerializer<List<Score>>.deserialize(scoreIndexString);
		}

		public static void updateIndex(string serializedIndex)
		{
			scores = BinarySerializer<List<Score>>.deserialize(serializedIndex);
		}

		public static void updateIndex(List<Score> index)
		{
			scores = index;
		}

		public static void printTopTen()
		{
			string debugTopTen = "Top 10 Scores: \n";
			if (scores.Count == 0) {
				loadScores();
			}
			foreach (Score _score in scores.ToArray()) {
				debugTopTen = debugTopTen + _score.getIntials() + ":" + _score.getScore() + "\n";
			}
			Debug.Log(debugTopTen);
		}

		public static void loadScores()
		{
			string scoreSerialized = PlayerPrefs.GetString("Scores", "");
			if (scoreSerialized != "") {
				Debug.Log("Score Serialized: " + scoreSerialized);
				Debug.Log("Score DeSerialzed: " + deserialize(scoreSerialized));
				updateIndex(scoreSerialized);
			}
		}

		public static void addScore(Score score)
		{
			int iteration = 0;
			if (scores.Count == 0) {
				scores.Insert(0, score);
				Debug.Log("Adding score: " + score.getIntials() + ":" + score.getScore());
			}
			else {
				List<Score> scoresCopy = scores;

				foreach (Score _score in scoresCopy.ToArray()) {
					if (score.getScore() >= _score.getScore()) {
						scores.Insert(iteration, score);
						Debug.Log("Adding score: " + score.getIntials() + ":" + score.getScore());
						break;
					}
					++iteration;
				}
				if (scores.Count > 10) {
					scores.RemoveRange(10, scores.Count - 10);
				}
			}
			printTopTen();
			PlayerPrefs.SetString("Scores", serialize());
			Debug.Log("Setting Scores to: " + serialize());
			PlayerPrefs.Save();
		}
	}
}