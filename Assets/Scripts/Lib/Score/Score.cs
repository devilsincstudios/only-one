// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.IO;

namespace DI_Game
{
	[Serializable()]
	public class Score
	{
//		private List<DI_Entity.EntityTypes> enemiesKilledIndex;
//		private List<int> enemiesKilledValues;
//		private List<DI_Entity.EntityTypes> powerupsUsedIndex;
//		private List<int> powerupsUsedValues;
//		private List<DI_Entity.EntityTypes> enemiesMissedIndex;
//		private List<int> enemiesMissedValues;
//		private List<DI_Entity.EntityTypes> powerupsMissedIndex;
//		private List<int> powerupsMissedValues;
		private int score;
		private string intials;
		private int multiplier;
		private string fileName;

		public Score()
		{
//			enemiesKilledIndex = new List<DI_Entity.EntityTypes>();
//			enemiesKilledValues = new List<int>();
//			powerupsUsedIndex = new List<DI_Entity.EntityTypes>();
//			powerupsUsedValues = new List<int>();

//			enemiesMissedIndex = new List<DI_Entity.EntityTypes>();
//			enemiesMissedValues = new List<int>();
//			powerupsMissedIndex = new List<DI_Entity.EntityTypes>();
//			powerupsMissedValues = new List<int>();

			score = 0;
			intials = "";
		}

		public void setFileName(string _fileName)
		{
			fileName = _fileName;
		}

		public string getFileName()
		{
			return fileName;
		}

		public void setMultiplier(int _multiplier)
		{
			multiplier = _multiplier;
		}

		public int getMultiplier()
		{
			return multiplier;
		}

		// Helper function to avoid getting the score first.
		public void addPoints(int points)
		{
			setScore(score + points * multiplier);
		}

		public void setScore(int _score)
		{
			score = _score;
		}

		public string getIntials()
		{
			return intials;
		}

		public void setIntials(string _intials)
		{
			intials = _intials;
		}

		public int getScore()
		{
			return score;
		}

//		public int getEnemyKills(DI_Entity.Enemy _enemy)
//		{
//			return getEnemyKills(_enemy.type);
//		}
//
//		public int getEnemyKills(DI_Entity.EntityTypes _type)
//		{
//			if (enemiesKilledIndex.Contains(_type)) {
//				int index = enemiesKilledIndex.IndexOf(_type);
//				return enemiesKilledValues[index];
//			}
//			else {
//				return 0;
//			}
//		}

//		public int getEnemyMisses(DI_Entity.Enemy _enemy)
//		{
//			return getEnemyMisses(_enemy.type);
//		}
//
//		public int getEnemyMisses(DI_Entity.EntityTypes _type)
//		{
//			if (enemiesMissedIndex.Contains(_type)) {
//				int index = enemiesMissedIndex.IndexOf(_type);
//				return enemiesMissedValues[index];
//			}
//			else {
//				return 0;
//			}
//		}

//		public int getPowerupUses(DI_Entity.EntityTypes _powerup)
//		{
//			if (powerupsUsedIndex.Contains(_powerup)) {
//				int index = powerupsUsedIndex.IndexOf(_powerup);
//				return powerupsUsedValues[index];
//			}
//			else {
//				return 0;
//			}
//		}
//
//		public int getPowerupMisses(DI_Entity.EntityTypes _powerup)
//		{
//			if (powerupsMissedIndex.Contains(_powerup)) {
//				int index = powerupsMissedIndex.IndexOf(_powerup);
//				return powerupsMissedValues[index];
//			}
//			else {
//				return 0;
//			}
//		}
//
//		public void enemyKilled(DI_Entity.Enemy _enemy)
//		{
//			if (!enemiesKilledIndex.Contains(_enemy.type)) {
//				enemiesKilledIndex.Add(_enemy.type);
//			}
//
//			int index = enemiesKilledIndex.IndexOf(_enemy.type);
//
//			if (enemiesKilledValues.Count > index) {
//				enemiesKilledValues[index] = enemiesKilledValues[index] + 1;
//			}
//			else {
//				enemiesKilledValues.Add(1);
//			}
//
//			addPoints(_enemy.points);
//		}
//
//		public void powerupUsed(DI_Entity.EntityTypes _powerup, int _points)
//		{
//			if (!powerupsUsedIndex.Contains(_powerup)) {
//				powerupsUsedIndex.Add(_powerup);
//			}
//			
//			int index = powerupsUsedIndex.IndexOf(_powerup);
//			
//			if (powerupsUsedValues.Count > index) {
//				powerupsUsedValues[index] = powerupsUsedValues[index] + 1;
//			}
//			else {
//				powerupsUsedValues.Add(1);
//			}
//			addPoints(_points);
//		}
//
//		public void enemyMissed(DI_Entity.Enemy _enemy)
//		{
//			if (!enemiesMissedIndex.Contains(_enemy.type)) {
//				enemiesMissedIndex.Add(_enemy.type);
//			}
//			
//			int index = enemiesMissedIndex.IndexOf(_enemy.type);
//			
//			if (enemiesMissedValues.Count > index) {
//				enemiesMissedValues[index] = enemiesMissedValues[index] + 1;
//			}
//			else {
//				enemiesMissedValues.Add(1);
//			}
//		}
//
//		public void powerupMissed(DI_Entity.EntityTypes _powerup)
//		{
//			if (!powerupsMissedIndex.Contains(_powerup)) {
//				powerupsMissedIndex.Add(_powerup);
//			}
//			
//			int index = powerupsMissedIndex.IndexOf(_powerup);
//			
//			if (powerupsMissedValues.Count > index) {
//				powerupsMissedValues[index] = powerupsMissedValues[index] + 1;
//			}
//			else {
//				powerupsMissedValues.Add(1);
//			}
//		}
	}
}