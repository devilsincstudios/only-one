// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace DI_Game
{
	public static class ScoreKeeper
	{
		public static Score score = new Score();

		public static void reset()
		{
			score = new Score();
		}

		public static void saveScore()
		{
			ScoreIndex.addScore(score);
		}
	}
}