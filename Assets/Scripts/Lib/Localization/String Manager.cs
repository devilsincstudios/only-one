﻿using UnityEngine;
using System.Collections;

/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Localization String Manager
*
*/
	
namespace DI_Language
{
	public static class StringManager 
	{
	/*******************************************************************************************************************************
	Members
	*******************************************************************************************************************************/		
		private static Hashtable localizedStrings = new Hashtable();
		
		private static UserLanguage language = UserLanguage.ENGLISH;
		public static UserLanguage Language
		{
			get{return language;}
			set{language = value; LoadStrings();}
		}
	
	/*******************************************************************************************************************************
	CONSTRUCTOR
	*******************************************************************************************************************************/
		// Use this for initialization
		static StringManager() 
		{
			language = UserLanguage.ENGLISH; //TODO: Load from player prefabs or location of settings			
			LoadStrings();	
		}
		
	/*******************************************************************************************************************************
	LOAD STRINGS
	Loads the string ID's and the relevant strings according to language
	*******************************************************************************************************************************/
		private static void LoadStrings()
		{	
			if(localizedStrings.Count >= 1)
			{
				localizedStrings.Clear();
			}
			
			TextAsset stringsFile = null;
			
			switch(Language)
			{
			case UserLanguage.ENGLISH:
				stringsFile = Resources.Load ("English") as TextAsset;
				break;
			case UserLanguage.FRENCH:
				stringsFile = Resources.Load ("French") as TextAsset;
				break;
			case UserLanguage.GERMAN:
				stringsFile = Resources.Load ("German") as TextAsset;
				break;
			case UserLanguage.SPANISH:
				stringsFile = Resources.Load ("Spanish") as TextAsset;
				break;
			case UserLanguage.ITALIAN:
				stringsFile = Resources.Load ("Italian") as TextAsset;
				break;
			case UserLanguage.PORTUGUESE:
				stringsFile = Resources.Load ("Portuguese") as TextAsset;
				break;
			}
			
			//Split up the file's text with any new lines or commas creating multiple strings
			char[] seperators = {"\n"[0], ","[0]};
			string[] loadedStrings = stringsFile.text.Split(seperators,System.StringSplitOptions.RemoveEmptyEntries);
			
			
			for(int i = 0; i < loadedStrings.Length; i+= 2)
			{	
				//If key is not used already
				if(!localizedStrings.ContainsKey (loadedStrings[i]))
				{	
					//we add the key(dev's language) and the string value(user's language) to the hashtable
					localizedStrings.Add(loadedStrings[i], loadedStrings[i+1]);
					
					string msg = string.Format("The string : \"{0}\" was added to the Localization Hash Table with key  \"{1}\"", localizedStrings[loadedStrings[i]], loadedStrings[i]);
					Debug.Log (msg);
				}
				
				else
				{
					string msg = string.Format ("When adding the string \"{0}\" to the Localization Hash Table, it's Key \"{1}\" already has a string value associated with it : \"{2}\"", loadedStrings[i+1], loadedStrings[i], localizedStrings[loadedStrings[i]]);
					Debug.LogError (msg); 
				}			
			}
			
			//Finished with the resource file
			stringsFile = null;
		}
	
	/*******************************************************************************************************************************
	GET STRING 
	Anytime we need access to a game string the ID must be called rather than the sring itself so the string is returned in the
	appropreate language
	*******************************************************************************************************************************/
		public static string GetString(string HashID)
		{
			if(!localizedStrings.ContainsKey(HashID))
			{
				Debug.LogError(string.Format("The hash ID value \"{0}\" does not exist", HashID));
				return null;
			}
			
			else
			{
				Debug.Log (string.Format ("Hash ID \"{0}\" was used to return string \"{1}\"", HashID, localizedStrings[HashID]));
				return localizedStrings[HashID] as string;
			}
		}
	}
}
