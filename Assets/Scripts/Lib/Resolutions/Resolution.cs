// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	TODO: Include a description of the file here.
// *
// */

using System;

namespace DI_Game
{
	[Serializable]
	public class Resolution
	{
		public ResolutionRatios ratio;
		public float minX;
		public float maxX;
		public float minY;
		public float maxY;
	}
}