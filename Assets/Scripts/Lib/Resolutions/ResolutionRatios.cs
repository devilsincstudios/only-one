// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	TODO: Include a description of the file here.
// *
// */

using System;

namespace DI_Game
{
	[Serializable]
	public enum ResolutionRatios
	{
		FIVE_FOUR,
		FOUR_THREE,
		THREE_TWO,
		SIXTEEN_TEN,
		SIXTEEN_NINE,
	}
}