// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI_Game
{
	public static class StatusLib
	{
		public static float getMaxEffectDuration()
		{
			if (PlayerPrefs.HasKey("Game/Status Effects/Max Duration")) {
				return PlayerPrefs.GetFloat("Game/Status Effects/Max Duration", 3600.0f);
			}
			else {
				PlayerPrefs.SetFloat("Game/Status Effects/Max Duration", 3600.0f);
				return 3600.0f;
			}
		}
	}
}