// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI_Bones
{
	public class BoneCollider
	{
		public ColliderTypes type;
		public Vector3 center;
		public float radius;
		public float height;
		public int direction;
		public Vector3 size;
		public bool isTrigger = false;

		public void addCollider(GameObject gameObject)
		{
			switch (type) {
			case ColliderTypes.BOX:
				if (gameObject.GetComponent<BoxCollider>() == null) {
					gameObject.AddComponent<BoxCollider>();
				}
				BoxCollider boxCollider = gameObject.GetComponent<BoxCollider>();
				boxCollider.size = size;
				boxCollider.center = center;
				boxCollider.isTrigger = isTrigger;
				break;
			case ColliderTypes.CAPSULE:
				if (gameObject.GetComponent<CapsuleCollider>() == null) {
					gameObject.AddComponent<CapsuleCollider>();
				}
				CapsuleCollider capsuleCollider = gameObject.GetComponent<CapsuleCollider>();
				capsuleCollider.height = height;
				capsuleCollider.center = center;
				capsuleCollider.radius = radius;
				capsuleCollider.direction = direction;
				capsuleCollider.isTrigger = isTrigger;
				break;
			case ColliderTypes.SPHERE:
				if (gameObject.GetComponent<SphereCollider>() == null) {
					gameObject.AddComponent<SphereCollider>();
				}
				SphereCollider sphereCollider = gameObject.GetComponent<SphereCollider>();
				sphereCollider.center = center;
				sphereCollider.radius = radius;
				sphereCollider.isTrigger = isTrigger;
				break;
			}
		}
	}
}