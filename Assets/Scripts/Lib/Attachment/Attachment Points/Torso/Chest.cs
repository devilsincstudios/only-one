// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//


using System.Collections.Generic;
using UnityEngine;

namespace DI_Bones
{
	public class Chest : AttachmentPoint
	{
		public Chest()
		{
			aliases = new List<string>();
			aliases.Add("Spine2");
			attachmentPoint = AttachmentTypes.CHEST;
			collider = new DI_Bones.BoneCollider();
			collider.type = ColliderTypes.BOX;
			collider.center = new Vector3(0, 0f, 0f);
			collider.size = new Vector3(0.4f, 0.4f, 0.3f);
			collider.isTrigger = true;
		}
	}
}