// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;
using UnityEngine;

namespace DI_Bones
{
	public class LeftLegUpper : AttachmentPoint
	{
		public LeftLegUpper()
		{
			aliases = new List<string>();
			aliases.Add("LeftUpLeg");
			attachmentPoint = AttachmentTypes.LEFT_LEG_UPPER;
			collider = new DI_Bones.BoneCollider();
			collider.type = ColliderTypes.CAPSULE;
			collider.radius = 0.08f;
			collider.center = new Vector3(0, 0.2f, 0);
			collider.height = 0.55f;
			collider.direction = 1;
			collider.isTrigger = true;
		}
	}
}