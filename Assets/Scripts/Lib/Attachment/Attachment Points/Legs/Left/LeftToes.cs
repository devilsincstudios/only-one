// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//


using System.Collections.Generic;

namespace DI_Bones
{
	public class LeftToes : AttachmentPoint
	{
		public LeftToes()
		{
			aliases = new List<string>();
			aliases.Add("LeftToe_End");
			aliases.Add("LeftFootToeBase_End");
			attachmentPoint = AttachmentTypes.LEFT_TOES;
		}
	}
}