// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//


using System.Collections.Generic;

namespace DI_Bones
{
	public class RightToes : AttachmentPoint
	{
		public RightToes()
		{
			aliases = new List<string>();
			aliases.Add("RightToe_End");
			aliases.Add("RightFootToeBase_End");
			attachmentPoint = AttachmentTypes.RIGHT_TOES;
		}
	}
}