// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;

namespace DI_Bones
{
	public class RightSole : AttachmentPoint
	{
		public RightSole()
		{
			aliases = new List<string>();
			aliases.Add("RightToeBase");
			attachmentPoint = AttachmentTypes.RIGHT_SOLE;
			collider = new DI_Bones.BoneCollider();
			collider.type = ColliderTypes.BOX;
			collider.center = new Vector3(0, -0.06f, -0.05f);
			collider.size = new Vector3(0.1f, 0.3f, 0.1f);
			collider.isTrigger = true;
		}
	}
}