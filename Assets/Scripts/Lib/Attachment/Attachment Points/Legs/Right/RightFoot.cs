// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//


using System.Collections.Generic;

namespace DI_Bones
{
	public class RightFoot : AttachmentPoint
	{
		public RightFoot()
		{
			aliases = new List<string>();
			aliases.Add("RightToeBase");
			attachmentPoint = AttachmentTypes.RIGHT_FOOT;
		}
	}
}