// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;

namespace DI_Bones
{
	public class RightLegLower : AttachmentPoint
	{
		public RightLegLower()
		{
			aliases = new List<string>();
			aliases.Add("RightLeg");
			attachmentPoint = AttachmentTypes.RIGHT_LEG_LOWER;
			collider = new DI_Bones.BoneCollider();
			collider.type = ColliderTypes.CAPSULE;
			collider.radius = 0.08f;
			collider.center = new Vector3(0, 0.25f, 0);
			collider.height = 0.5f;
			collider.isTrigger = true;
			collider.direction = 1;
		}
	}
}