// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;
using UnityEngine;

namespace DI_Bones
{
	public class LeftArmUpper : AttachmentPoint
	{
		public LeftArmUpper()
		{
			aliases = new List<string>();
			aliases.Add("LeftArm");
			attachmentPoint = AttachmentTypes.LEFT_ARM_UPPER;
			collider = new DI_Bones.BoneCollider();
			collider.type = ColliderTypes.CAPSULE;
			collider.radius = 0.07f;
			collider.center = new Vector3(0f, 0.15f, 0f);
			collider.height = 0.35f;
			collider.isTrigger = true;
			collider.direction = 1;
		}
	}
}