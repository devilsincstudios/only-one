// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//


using System.Collections.Generic;

namespace DI_Bones
{
	public class LeftShoulder : AttachmentPoint
	{
		public LeftShoulder()
		{
			aliases = new List<string>();
			aliases.Add("LeftShoulder");
			attachmentPoint = AttachmentTypes.LEFT_SHOULDER;
		}
	}
}