// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;

namespace DI_Bones
{
	public class LeftHand : AttachmentPoint
	{
		public LeftHand()
		{
			aliases = new List<string>();
			aliases.Add("LeftHand");
			attachmentPoint = AttachmentTypes.LEFT_HAND;
		}
	}
}