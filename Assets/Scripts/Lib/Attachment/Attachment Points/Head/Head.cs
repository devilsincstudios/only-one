// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;
using UnityEngine;

namespace DI_Bones
{
	public class Head : AttachmentPoint
	{
		public Head()
		{
			aliases = new List<string>();
			aliases.Add("Head");
			attachmentPoint = AttachmentTypes.HEAD;
			collider = new DI_Bones.BoneCollider();
			collider.type = ColliderTypes.CAPSULE;
			collider.radius = 0.1f;
			collider.center = new Vector3(0f, 0.1f, 0.05f);
			collider.height = 0.24f;
			collider.isTrigger = true;
			collider.direction = 1;
		}
	}
}