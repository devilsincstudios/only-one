// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// AttachmentTypes.cs - Enum to keep track of valid attachment points for a character.
//

namespace DI_Bones {
	public enum AttachmentTypes {
		CHEST,
		HEAD,
		LEFT_ANKLE,
		LEFT_ARM_LOWER,
		LEFT_ARM_UPPER,
		LEFT_FOOT,
		LEFT_HAND,
		LEFT_LEG_LOWER,
		LEFT_LEG_UPPER,
		LEFT_SHOULDER,
		LEFT_SOLE,
		LEFT_TOES,
		NECK,
		PELVIS,
		RIGHT_ANKLE,
		RIGHT_ARM_LOWER,
		RIGHT_ARM_UPPER,
		RIGHT_FOOT,
		RIGHT_HAND,
		RIGHT_LEG_LOWER,
		RIGHT_LEG_UPPER,
		RIGHT_SHOULDER,
		RIGHT_SOLE,
		RIGHT_TOES,
		UNASSIGNED
	}
}
