/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Library of celestial math to determine the specific location of the sun on a given date at a given lat, long.
*	This is massively overkill for what we actually need in the day/night cycle.
*
*/

using System;

// TODO Remove this class as I don't think it is actually used

public static class CelestialTime {
	private static double toJulianDate(DateTime date) {
		return date.ToOADate() + 2415018.5;
	}
	
	// This function assumes GMT +0 timezone.
	public static double getCelestialRotation(DateTime date, double lat, double lon) {
		// 
		// http://en.wikipedia.org/wiki/Position_of_the_Sun
		// http://answers.yahoo.com/question/index?qid=20080630193348AAh4zNZ
		// http://en.wikipedia.org/wiki/Hour_angle
		// http://en.wikipedia.org/wiki/Sidereal_time
		//
		
		double celstialDate = CelestialTime.toJulianDate(date);
		
		// The mean longitude of the Sun, corrected for the aberration of light, is:
		double longitude = 280.460 + 0.9856474;
		// The mean anomaly of the Sun (actually, of the Earth in its orbit around the Sun, but it is convenient to pretend the Sun orbits the Earth), is:
		double anomaly = 357.528 + 0.9856003;
		// Put L and g in the range 0° to 360° by adding or subtracting multiples of 360° as needed.
		while (longitude > 360) {
			longitude = longitude - 360;
		}
		while (anomaly > 360) {
			anomaly = anomaly - 360;
		}
		// Finally, the ecliptic longitude of the Sun is:
		double eclipticLongitude = longitude + 1.915f * Math.Sin (anomaly) + 0.20f * Math.Sin(2 * anomaly);
		//The ecliptic latitude of the Sun is nearly:
 		//\beta = 0 , as the ecliptic latitude of the Sun never exceeds 0°.00033,[4]
		//and the distance of the Sun from the Earth, in astronomical units, is:
		//double distance = 1.00014 - 0.01671 * Math.Cos (anomaly) - 0.00014 * Math.Cos(2 * anomaly);
		//\lambda, \beta and R form a complete position of the Sun in the ecliptic coordinate system. This can be converted to the equatorial coordinate system by calculating the obliquity of the ecliptic, \epsilon, and continuing:
		// Right ascension,
 		//\alpha = \arctan(\cos \epsilon \tan \lambda), where \alpha is in the same quadrant as \lambda,
		double tilt = 23.439 - 0.0000004;
		double rightAscension = Math.Atan(Math.Cos(tilt) * Math.Tan(eclipticLongitude));
		//and declination,
 		//\delta = \arcsin(\sin \epsilon \sin \lambda).	
		//double declination = Math.Asin(Math.Sin(tilt) * Math.Sin (eclipticLongitude));
		//Spring  = March 20th
		//Summer = June 21th
		//Fall = Sept 21st
		//Winter = Dec 21st
		//double declinationAngle = Math.Asin(Math.Sin(-1 * tilt) * Math.Cos ((360/365.24) * (celstialDate + 10) + 360/Math.PI * 0.0167 * Math.Sin((360/365.24) * (celstialDate - 2))));
		
		// Greenwich Sidereal Time in degrees
		double GST = 280.461 + 360.98564737 * celstialDate;
		while (GST > 360) {
			GST = GST - 360;
		}
		double localSideRealTime = (GST - lat) + 360;
		double localHourAngle = localSideRealTime - rightAscension;
		while (localHourAngle > 360) {
			localHourAngle = localHourAngle - 360;
		}
		return localHourAngle;
	}
	
	public static double getBrightness(double sunAngle) {
		while (sunAngle > 180) {
			sunAngle = sunAngle - 180;
		}
		
		//Capped at 0.5
		return ((1.8 * sunAngle) / 100) / 2;
	}
}