// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DI_Game {
	public class MaterialSound : MonoBehaviour {
		public Materials materialType;
		public List<AudioClip> sounds;
	}
}
