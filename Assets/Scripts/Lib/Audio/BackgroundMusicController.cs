﻿using UnityEngine;
using System.Collections;

/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	BACKGROUND MUSIC CONTROLLER
*   Controls the playing and swapping of background music through use of the event system
*
*/


namespace DI_Audio
{
	public class BackgroundMusicController : MonoBehaviour 
	{
		/*******************************************************************************************************************************
		MEMBERS
		*******************************************************************************************************************************/	
		[Range(0f,1f)]
		public float Volume = 1f; //The default volume of all background music
		public float FadeSpeed = 1f;
		
		//TYPES OF BACKGROUND MUSIC
		public AudioClip[] CombatMusic = null;
		public AudioClip[] PauseMusic = null;
		public AudioClip[] MainMenuMusic = null;
		//TODO: All types of game states' that require unique background audio must have their own array
		
		private BGMusicStates CurrentState = BGMusicStates.NO_AUDIO;
		private AudioSource MusicA ; 
		private AudioSource MusicB;     //2 Audio sources so can fade in and out of different audio by changing volume
		
		/*******************************************************************************************************************************
		START
		*******************************************************************************************************************************/
		void Start () 
		{
			MusicA = new AudioSource();
			MusicB = new AudioSource();
			
			//Give high priority so always plays
			MusicA.priority = 0;
			MusicB.priority = 0;
			
			//music will play even if we pause the AudioListener component
			MusicA.ignoreListenerPause = true;
			MusicB.ignoreListenerPause = true;
			
			//Do not loop tracks (will go to similiar type of BG track or will loop manually)
			MusicA.loop = false;
			MusicB.loop = false;
			
			//TODO:AddListeners
			DI_Events.EventCenter.addListener("Combat Started", StartCombatMusic);
			
			//TODO:Make audiosources play at a position where they are always heard(cannot remember technical term)
		}
		
		
		/*******************************************************************************************************************************
		ONDESTROY
		*******************************************************************************************************************************/
		void OnDestroy()
		{
			//TODO:RemoveListers
			DI_Events.EventCenter.removeListener("Combat Started", StartCombatMusic);
		}
		
		
		/*******************************************************************************************************************************
		UPDATE
		*******************************************************************************************************************************/
		void Update () 
		{
			//Get a reference to the currently playing source
			AudioSource current = MusicA.isPlaying ? MusicA:MusicB;
			
			//If the music clip has finished
			if(current.time >= current.clip.length)
			{
				//Start a track from the same type of music
				switch (CurrentState)
				{
				case BGMusicStates.NO_AUDIO:
					break;
				case BGMusicStates.COMBAT:
					GetNewTrack(current, CombatMusic);
					break;
				case BGMusicStates.MAIN_MENU:
					GetNewTrack(current, MainMenuMusic); 
					break;
				case BGMusicStates.PAUSE_MENU:
					GetNewTrack(current, PauseMusic);
					break;
				}	
			}
		}
		
		
		/*******************************************************************************************************************************
		START COMBAT MUSIC
		*******************************************************************************************************************************/
		void StartCombatMusic()
		{
			//Track the current type of music
			CurrentState = BGMusicStates.COMBAT;
			
			ChangeMusic(CombatMusic);
		}
		
		
		/*******************************************************************************************************************************
		FADE
		The In Audio Source reference will slowly increase in volume while the Out AudioSource reference will slowly decrease in volume
		to create a Fade effect. Parameters are used as MusicA and MusicB could both be the currently playing track at one time
		*******************************************************************************************************************************/
		IEnumerator Fade(AudioSource In, AudioSource Out)
		{
			Debug.Log (string.Format ("Begun fading to {0} background music : \" {1} \"", CurrentState.ToString(), In.clip.name));
			
			//Begin to play the upcoming track
			In.Play();
			
			//While they are still fading between each other
			while(In.volume < Volume && Out.volume > 0.1f)
			{
				//Increase in volume
				In.volume = Mathf.Lerp(In.volume, Volume, Time.deltaTime * FadeSpeed);
				//Decrease the outgoing music's voume
				Out.volume = Mathf.Lerp(Out.volume, 0.1f, Time.deltaTime * FadeSpeed);
				yield return null;
			}
			
			//We will reach this code once they have faded successfully
			In.volume = Volume;
			//The outgoing audio source is no longer needed until the next fade
			Out.volume = 0f;
			Out.Stop();
			Out.clip = null;	
			
			Debug.Log (string.Format ("Finished fading to {0} background music : \" {1} \"", CurrentState.ToString(), In.clip.name));
		}
		
		
		/*******************************************************************************************************************************
		GET NEW TRACK
		Takes in the currently playing source and the picks a clip to change to from the array of clips
		*******************************************************************************************************************************/
		void GetNewTrack(AudioSource current, AudioClip[] clips)
		{
			//if there is more than 1 track in array
			if(clips.Length != 1)
			{
				//pick a random track from the list
				int rand = UnityEngine.Random.Range(0, clips.Length - 1);
				while(current.clip == CombatMusic[rand])
				{
					//get a new random track if it picked the same track
					rand = UnityEngine.Random.Range(0, CombatMusic.Length - 1);
				}
				current.clip = CombatMusic[rand];
			}
			
			//set the time to 0 so starts track from beginning
			current.time = 0f;
			
			Debug.Log (string.Format ("New {0} track started : \" {1} \"",CurrentState.ToString(), current.clip.name));
		}
		
		
		/*******************************************************************************************************************************
		CHANGE MUSIC
		When the music states change this function fades out the old music type and fades in a new type (determined by the array parameter)
		*******************************************************************************************************************************/
		void ChangeMusic(AudioClip[] clips)
		{
			//Pick a random track from the array of combat music
			int rand = UnityEngine.Random.Range(0,clips.Length - 1);
			
			//if A is the current playing source
			if(MusicA.isPlaying)
			{
				//Set B as the new track to play
				MusicB.clip = clips[rand];
				//And fade them
				StartCoroutine(Fade(MusicB,MusicA));
			}
			
			else
			{
				//Else visa versa
				MusicA.clip = clips[rand];
				StartCoroutine(Fade(MusicA,MusicB));
			}
		}
		
		
		//TODO: Create event handlers which change new music, and set relevant BGMusicState
	}
}
