﻿// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
//  BACKGROUND MUSIC STATES
//	Each of these game states will have their own unique background music or audio
//

namespace DI_Audio
{
	public enum BGMusicStates
	{
		NO_AUDIO,
		COMBAT,
		PAUSE_MENU,
		MAIN_MENU
		//TODO: Complete List
	}
}