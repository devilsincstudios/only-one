// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

namespace DI_Item
{
	public static class InventoryManager
	{
		public static List<string> getInventoryDescriptions()
		{
			try {
				if (!Directory.Exists("SavedData")) {
					Directory.CreateDirectory("SavedData");
				}
			}
			catch (DirectoryNotFoundException err) {
				Debug.LogException(err);
				Directory.CreateDirectory("SavedData");
			}

			List<string> inventoryData = new List<string>();
			foreach (string file in Directory.GetFiles("SavedData")) {
				string fileName = file;
				fileName = fileName.Replace("SavedData\\inventory", "");
				fileName = fileName.Replace(".xml", "");
				int inventoryId;
				if (int.TryParse(fileName, out inventoryId)) {
					Inventory inventory = loadInventory(inventoryId);
					inventoryData.Add(inventory.inventoryDescription);
				}
			}
			return inventoryData;
		}

		public static List<string> getInventoryNames()
		{
			try {
				if (!Directory.Exists("SavedData")) {
					Directory.CreateDirectory("SavedData");
				}
			}
			catch (DirectoryNotFoundException err) {
				Debug.LogException(err);
				Directory.CreateDirectory("SavedData");
			}

			List<string> inventoryData = new List<string>();
			foreach (string file in Directory.GetFiles("SavedData")) {
				string fileName = file;
				fileName = fileName.Replace("SavedData\\inventory", "");
				fileName = fileName.Replace(".xml", "");
				int inventoryId;
				if (int.TryParse(fileName, out inventoryId)) {
					Inventory inventory = loadInventory(inventoryId);
					inventoryData.Add(inventory.inventoryName);
				}
			}
			return inventoryData;
		}

		public static List<int> getInventoryIds()
		{
			try {
				if (!Directory.Exists("SavedData")) {
					Directory.CreateDirectory("SavedData");
				}
			}
			catch (DirectoryNotFoundException err) {
				Debug.LogException(err);
				Directory.CreateDirectory("SavedData");
			}

			List<int> inventoryIds = new List<int>();
			foreach (string file in Directory.GetFiles("SavedData")) {
				string fileName = file;
				fileName = fileName.Replace("SavedData\\inventory", "");
				fileName = fileName.Replace(".xml", "");
				int inventoryId;
				if (int.TryParse(fileName, out inventoryId)) {
					inventoryIds.Add(inventoryId);
				}
			}
			return inventoryIds;
		}

		public static void saveInventory(Inventory inventory, int inventoryId)
		{
			string inventoryName = "SavedData/inventory" + inventoryId + ".xml";
			try {
				if (!Directory.Exists("SavedData")) {
					Directory.CreateDirectory("SavedData");
				}
			}
			catch (DirectoryNotFoundException err) {
				Debug.LogException(err);
				Directory.CreateDirectory("SavedData");
			}

			XmlSerializer ser = new XmlSerializer(typeof(Inventory));
			
			using (StreamWriter writer = new StreamWriter(inventoryName))
			{
				ser.Serialize (writer, inventory);
			}
		}

		public static int getNextInventoryId()
		{
			int inventoryId = PlayerPrefs.GetInt("Game/Inventory/Last Id", 0) + 1;
			PlayerPrefs.SetInt("Game/Inventory/Last Id", inventoryId);
			return inventoryId;
		}

		public static int cloneInventory(int id)
		{
			Inventory previousInventory = loadInventory(id);
			previousInventory.inventoryId = getNextInventoryId();
			saveInventory(previousInventory, previousInventory.inventoryId);
			return previousInventory.inventoryId;
		}

		public static void deleteInventory(int id)
		{
			string inventoryName = "SavedData/inventory" + id + ".xml";
			File.Delete(inventoryName);
		}

		public static Inventory loadInventory(int id)
		{
			string inventoryName = "SavedData/inventory" + id + ".xml";
			Debug.Log("Attempting to load inventory: " + id);
			try {
				if (!Directory.Exists("SavedData")) {
					Directory.CreateDirectory("SavedData");
				}
			}
			catch (DirectoryNotFoundException err) {
				Debug.LogException(err);
				Directory.CreateDirectory("SavedData");
			}

			try {
				if (File.Exists (inventoryName)) {
					Inventory _inventory;
					XmlSerializer ser = new XmlSerializer(typeof (Inventory));
					StreamReader reader;
					reader = new StreamReader(inventoryName);
					
					_inventory = (Inventory)ser.Deserialize (reader);
					reader.Close();
					return _inventory;
				}
				else {
					Inventory _inventory = new Inventory();
					_inventory.inventoryId = getNextInventoryId();
					return _inventory;
				}
			}
			catch (FileNotFoundException err) {
				Debug.LogException(err);
				Inventory _inventory = new Inventory();
				_inventory.inventoryId = getNextInventoryId();
				return _inventory;
			}
		}
	}
}