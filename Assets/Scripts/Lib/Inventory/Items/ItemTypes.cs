// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: The Item Manager
//
//------------------------------------------------------------------------------

namespace DI_Item
{
	public enum ItemTypes {
		_WEAPON,
		_ARMOR,
		_TRAP,
		_MISSION,
		_CONSUMABLE,
		_PART,
		_JUNK,
		_AMMO,
		_UNASSIGNED,
	}
}