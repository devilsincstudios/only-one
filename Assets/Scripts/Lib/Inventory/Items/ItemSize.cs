// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_Item {
	public enum ItemSize {
		ONE_BY_ONE,
		ONE_BY_TWO,
		ONE_BY_THREE,
		TWO_BY_ONE,
		TWO_BY_TWO,
		TWO_BY_THREE,
		THREE_BY_ONE,
		THREE_BY_TWO,
		THREE_BY_THREE
	}
}