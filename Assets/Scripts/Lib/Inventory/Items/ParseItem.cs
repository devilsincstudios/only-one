// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

namespace DI_Item
{
	public static class ParseItem
	{
		public static int[] parseSize(ItemSize itemSize)
		{
			int sizeVertical = 1;
			int sizeHorizontal = 1;

			switch (itemSize) {
			case ItemSize.ONE_BY_ONE:
				sizeVertical = 1;
				sizeHorizontal = 1;
				break;
			case ItemSize.ONE_BY_TWO:
				sizeVertical = 1;
				sizeHorizontal = 2;
				break;
			case ItemSize.ONE_BY_THREE:
				sizeVertical = 1;
				sizeHorizontal = 3;
				break;
				
			case ItemSize.TWO_BY_ONE:
				sizeVertical = 2;
				sizeHorizontal = 1;
				break;
			case ItemSize.TWO_BY_TWO:
				sizeVertical = 2;
				sizeHorizontal = 2;
				break;
			case ItemSize.TWO_BY_THREE:
				sizeVertical = 2;
				sizeHorizontal = 3;
				break;
				
			case ItemSize.THREE_BY_ONE:
				sizeVertical = 3;
				sizeHorizontal = 1;
				break;
			case ItemSize.THREE_BY_TWO:
				sizeVertical = 3;
				sizeHorizontal = 2;
				break;
			case ItemSize.THREE_BY_THREE:
				sizeVertical = 3;
				sizeHorizontal = 3;
				break;
			default:
				sizeVertical = 1;
				sizeHorizontal = 1;
				break;
			}
			int[] returnValue = { sizeVertical, sizeHorizontal };
			return returnValue;
		}
	}
}