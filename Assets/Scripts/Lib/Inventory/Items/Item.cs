// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

// TODO Split this out into seperate item classes for item types.
namespace DI_Item {
	[Serializable]
	[XmlRoot("Items")]
	public class Item
	{
		[XmlAttribute("ItemData")]
		public string itemName = "Item Name";
		public string description = "brief description";
		public int itemId;
		public string imgItem_path = "Assets/GUI/emptyIconItem.png";
		public string meshItem_path = "Assets/";
		public float weight = 1.0f;
		public float value = 1.0f;
		public int maxDurability = 1;
		public int currentDurability = 1;
		public bool bIsMission = false;
		public bool bIsConsumable = false;
		public bool bIsEquipable = false;
		public ItemTypes type = ItemTypes._UNASSIGNED;
		public AmmoType ammoType = AmmoType._UNASSIGNED;
		public float minDamage = 1;
		public float maxDamage = 1;
		public int maxAmmo = 0;
		public int currentAmmo = 0;
		public float amountHealed = 1;
		public int slots = 1;
		public bool stackable = false;
		public int maxStackSize = 1;
		public int stackSize = 1;
		public float damageReduction = 0;
		public DI_Bones.AttachmentTypes mountPoint = DI_Bones.AttachmentTypes.UNASSIGNED;
		public ConsumableEffect effect = ConsumableEffect._UNASSIGNED;
		public DI_Game.StatusTypes statusEffect = DI_Game.StatusTypes.UNASSIGNED;
		public float buffTime = 0.0f;
	}
}