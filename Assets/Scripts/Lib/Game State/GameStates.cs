/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	ENUM for game states.
*
*/

namespace DI_Game {
	public enum GameState {
		PAUSED,
		IN_MENU,
		PLAYING,
		DEAD
	}
}
