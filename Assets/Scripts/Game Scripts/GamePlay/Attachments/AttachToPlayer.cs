﻿/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Attaches an object to the player on game start.
*
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DI_Bones {
	[AddComponentMenu("Attachments/Attach To Player")]
	public class AttachToPlayer : MonoBehaviour {
		
		public GameObject player;
		private AttachmentPoints attachmentPoints;
		public AttachmentTypes attachmentPoint;
		public Vector3 attachmentOffsetPositon;
		public Vector3 attachmentOffsetRotation;

		void OnEnable() {
			attachmentPoints = player.GetComponent<AttachmentPoints>();
			if (attachmentPoints != null) {
				attachmentPoints.attachToBone(attachmentPoint, this.gameObject, attachmentOffsetPositon, attachmentOffsetRotation);
			}
		}
	}	
}
