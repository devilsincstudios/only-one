// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using System.Collections.Generic;
using System;

namespace DI_Game.Attachments
{
	[AddComponentMenu("Attachments/Laser Sight")]
	public class LaserSight : MonoBehaviour
	{
		public LineRenderer lineRenderer;
		public RaycastHit hit;

		public void OnEnable()
		{
			lineRenderer = GetComponent<LineRenderer>();
		}

		public void LateUpdate()
		{
			if (lineRenderer != null) {
				lineRenderer.SetPosition(0, this.transform.position);
				if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity)) {
					lineRenderer.SetPosition(1, hit.point);
				}
			}
		}
	}
}
