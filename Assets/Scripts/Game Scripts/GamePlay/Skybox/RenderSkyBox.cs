﻿/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/
// http://docs.unity3d.com/Documentation/ScriptReference/Camera.RenderToCubemap.html
using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RenderSkyBox : MonoBehaviour {
	public LayerMask layerMask;
	public int cubeMapSize = 256;
	
	private RenderTexture cubeMap;
	public GameObject cubeCam;
	
	void LateUpdate () {
		if (!cubeCam) {
			cubeCam = new GameObject("CubeMapCamera", typeof(Camera));
			cubeCam.camera.transform.rotation = Quaternion.identity;
			cubeCam.camera.renderingPath = RenderingPath.Forward;
			cubeCam.camera.nearClipPlane = 0.001f;
			cubeCam.camera.farClipPlane = 250f;
			cubeCam.camera.enabled = true;
			cubeCam.camera.aspect = 1;
			cubeCam.camera.clearFlags = CameraClearFlags.Depth;
		}
		cubeCam.camera.transform.position = transform.position;
		cubeCam.camera.cullingMask = layerMask;
		
		if (!cubeMap) {
			cubeMap = new RenderTexture(cubeMapSize, cubeMapSize, 16);
			cubeMap.isCubemap = true;
			cubeMap.isPowerOfTwo = true;
			cubeMap.useMipMap = true;
			cubeMap.filterMode = FilterMode.Trilinear;
			cubeMap.wrapMode = TextureWrapMode.Clamp;
		}
		
		//int faceToRender = 1 << (Time.frameCount % 6);
		cubeCam.camera.RenderToCubemap(cubeMap, 63);
		RenderSettings.skybox.SetTexture("_Cube", cubeMap);
	}
	
	void onDisable() {
		DestroyImmediate(cubeCam);
		DestroyImmediate(cubeMap);
	}
}