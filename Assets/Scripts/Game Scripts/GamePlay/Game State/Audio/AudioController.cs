// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace DI_GameState.Audio
{
	public class AudioController
	{
		public Dictionary<string, AudioSource> audioSources;
		public int currentSource;
		public int nextSource;
		public int previousSource;
		public float crossFadeTime;
		public float crossFadeDeltaTime;

		public AudioController()
		{
			audioSources = new Dictionary<string, AudioSource>();
			currentSource = 0;
			nextSource = 0;
			crossFadeTime = 0;
			crossFadeDeltaTime = 0;
		}

		public void addSource(string name, AudioSource source)
		{
			if (audioSources.ContainsKey(name)) {
				audioSources[name] = source;
			}
			else {
				audioSources.Add(name, source);
			}
		}
	}
}
