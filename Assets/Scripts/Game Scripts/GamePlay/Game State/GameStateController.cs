/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

/*
 * 
 //TODO Clean this up, it already has functions and stuff in it that have no right to be here.
 * 
 */

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

using DI_GameState.Entity;
using DI_GameState.Audio;
using DI_GameState.Input;
using DI_GameState.Score;
using DI_GameState.Time;
using DI_GameState.Wave;
using DI_GameState.Weather;

namespace DI_GameState {
	[AddComponentMenu("Game State/Controller")]
	public class GameStateController : MonoBehaviour
	{
		private SurvivorController survivorController = new SurvivorController();
		private WeatherController weatherController = new WeatherController();
		private TimeController timeController = new TimeController();
		private InputController inputController = new InputController();
		private WaveController waveController = new WaveController();
		private ScoreController scoreController = new ScoreController();
		private SniperController sniperController = new SniperController();
		private PlacementController placementController = new PlacementController();
		private OutpostController outpostController = new OutpostController();
		private LandingZoneController landingZoneController = new LandingZoneController();
		private ItemController itemController = new ItemController();
		private HelicopterController helicopterController = new HelicopterController();
		private PlayerController playerController = new PlayerController();
		private EnemyController enemyController = new EnemyController();
		private SoldierController soldierController = new SoldierController();
		private AudioController audioController = new AudioController();

		// Players
		private List<DI_Game.Entity.Player> playerList;

		private Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> p1keyStates = DI_Input.InputManager.getKeyStates(1);
		private Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> p2keyStates = DI_Input.InputManager.getKeyStates(2);
		
		private DI_Game.GameState gameState;

		public void Start()
		{
			// Load saved/default keybindings.
			DI_Input.InputManager.loadKeyBindings();

			// Until we get an actual gui setup for this just hard code the controller type.
			DI_Input.InputManager.setPlayerControllerType(1, DI_Input.ControllerType.PLAYER_ONE_KEYBOARD);
			DI_Input.InputManager.setPlayerControllerType(2, DI_Input.ControllerType.PLAYER_TWO_CONTROLLER_ONE);

			// Register state changed listener.
			DI_Events.EventCenter<DI_Game.GameState>.addListener("onGameStateChange", handleGameStateChanged);
			// Register input listener.
			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.addListener("onInput", keyListener);

			gameState = DI_Game.GameState.PLAYING;
		}

		public void LateUpdate()
		{
			try {
				manageInput(1);
				if (playerList.Count == 2) {
					manageInput(2);
				}
			}
			catch (Exception err) {
				UnityEngine.Debug.LogException(err);
			}
		}

		// Get the Dictionary of the current keystates so we can save a poll step in each script that needs inputs.
		//=================================================================================================
		public Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> getP1KeyStates()
		{
			return p1keyStates;
		}

		public Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> getP2KeyStates()
		{
			return p2keyStates;
		}

		public DI_Input.KeyStates getKeyState(DI_Input.RegisteredKeys key, int player)
		{
			if (player == 1) {
				return p1keyStates[key];
			}
			else {
				return p2keyStates[key];
			}
		}

		//=================================================================================================

		// Game State handler
		//=================================================================================================
		public DI_Game.GameState getGameState()
		{
			return gameState;
		}

		public void handleGameStateChanged(DI_Game.GameState state)
		{
			gameState = state;
		}

		//=================================================================================================
		// Listener for key events. This should be the standard listener for key events.
		// This should handle global changes but should not make local changes
		// For example, do not toggle the flashlight from here.
		// It should be part of the charactor class.
		// Its only here as a test and does nothing at the moment.
		// Bringing up the game menu is a good example of what should be in the global listener.
		//=================================================================================================
		public void keyListener(DI_Input.RegisteredKeys key, DI_Input.KeyStates state, int player)
		{
			if (key == DI_Input.RegisteredKeys.DEV_MENU && player == 1) {
				Application.LoadLevel("MainMenu");
			}
			if (key == DI_Input.RegisteredKeys.DEV_EXIT && player == 1) {
				Application.Quit();
			}
		}

		// Changing the players changes the viewport to be split screen veritical split.
		// P1 on top / P2 on bottom.
		//=================================================================================================
		public void setPlayers(int numberOfPlayers)
		{
//			switch (players) {
//			case 1:
//				player1Camera.camera.fieldOfView = 70;
//				player1Camera.camera.rect = new Rect(0f, 0f, 1f, 1f);
//				player2Camera.SetActive(false);
//				numberOfPlayers = 1;
//				DI_Events.EventCenter<int>.invoke("onNumberOfPlayersChanged", players);
//				break;
//			case 2:
//				player1Camera.camera.fieldOfView = 30;
//				player1Camera.camera.rect = new Rect(0f, 0.5f, 1f, 0.5f);
//				player2Camera.camera.fieldOfView = 30;
//				player2Camera.camera.rect = new Rect(0f, 0f, 1f, 0.5f);
//				player2Camera.SetActive(true);
//				numberOfPlayers = 2;
//				DI_Events.EventCenter<int>.invoke("onNumberOfPlayersChanged", players);
//				break;
//			default:
//				// Ignore this
//				break;
//			}
		}

		public int getPlayers()
		{
			return playerList.Count;
		}

		//=================================================================================================

		// Send input state changed notification events.
		//=================================================================================================
		private void manageInput(int player)
		{
			// Store a copy of the currently pressed keys, compare it the last state of the keys.
			// If we have any changes send an event.
			// If we don't add this logic the game takes even more of a performance hit.
			// This part must be as lean as we can make it or we will suffer frame rate hits as its running on fixed update.

			// TODO: Add more logic to this to help prevent some of event calls.
			if (Application.isPlaying) {
				Dictionary<DI_Input.RegisteredKeys, DI_Input.KeyStates> currentKeyStates = DI_Input.InputManager.getKeyStates(1);
				if (player == 1) {
					if (p1keyStates != null) {
						foreach (KeyValuePair<DI_Input.RegisteredKeys, DI_Input.KeyStates> entry in currentKeyStates) {
							if (p1keyStates[entry.Key] != currentKeyStates[entry.Key]) {
								DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.invoke("onInput", entry.Key, entry.Value, 1);
							}
						}
					}
					p1keyStates = currentKeyStates;
				}
				else {
					if (p2keyStates != null) {
						foreach (KeyValuePair<DI_Input.RegisteredKeys, DI_Input.KeyStates> entry in currentKeyStates) {
							if (p2keyStates[entry.Key] != currentKeyStates[entry.Key]) {
								DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.invoke("onInput", entry.Key, entry.Value, 2);
							}
						}
					}
					p2keyStates = currentKeyStates;
				}
			}
		}
		//=================================================================================================

		public void onDisable()
		{
			// Deregister game state listener.
			DI_Events.EventCenter<DI_Game.GameState>.removeListener("onGameStateChange", handleGameStateChanged);
			// Deregister input listener.
			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.removeListener("onInput", keyListener);
		}

		// TODO: Fix the exception thrown about keystate in main thread.
		// I think it has to do with unloading the scene is called before exit and because the scene is unloaded for a moment before it exits it throws the exception.
		public void OnApplicationQuit()
		{
			onDisable();
			DI_Debug.Logger.Instance.cleanUp();
		}
	}
}
