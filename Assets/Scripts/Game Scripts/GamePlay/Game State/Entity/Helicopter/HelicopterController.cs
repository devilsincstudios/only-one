// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace DI_GameState.Entity
{
	public class HelicopterController : EntityController
	{
		private DI_Game.Entity.Helicopter sniper;
		
		new public void OnAwake()
		{
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onHelicopterSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onHelicopterDespawn", handleDespawn);
			base.OnAwake();
		}
		
		new public void handleSpawn(DI_Game.Entity.Entity _entity)
		{
			sniper = (DI_Game.Entity.Helicopter) _entity;
			base.handleSpawn(_entity);
		}
		
		new public void handleDespawn(DI_Game.Entity.Entity _entity)
		{
			sniper = null;
			base.handleDespawn(_entity);
		}
		
		new public void OnDestroy()
		{
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onHelicopterSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onHelicopterDespawn", handleDespawn);
			base.OnDestroy();
		}
		
		public DI_Game.Entity.Helicopter getHelicopter()
		{
			return sniper;
		}
	}
}