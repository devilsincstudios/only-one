// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections.Generic;
using DI_Game.Entity;

namespace DI_GameState.Entity
{
	public class EntityController : MonoBehaviour
	{
		public Dictionary<EntityType, List<DI_Game.Entity.Entity>> entities;

		public void OnAwake()
		{
			entities = new Dictionary<EntityType, List<DI_Game.Entity.Entity>>();
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onDespawn", handleDespawn);
		}

		public void handleSpawn(DI_Game.Entity.Entity _entity)
		{
			if(!entities.ContainsKey(_entity.type)) {
				entities.Add(_entity.type, new List<DI_Game.Entity.Entity>());
			}
			entities[_entity.type].Add(_entity);
		}

		public void handleDespawn(DI_Game.Entity.Entity _entity)
		{
			// Make sure we aren't trying to remove a non-existant key.
			if (entities.ContainsKey(_entity.type)) {
				if (entities[_entity.type].Contains(_entity)) {
					entities[_entity.type].Remove(_entity);
				}
			}
		}

		public void OnDestroy()
		{
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onDespawn", handleDespawn);
		}
	}
}