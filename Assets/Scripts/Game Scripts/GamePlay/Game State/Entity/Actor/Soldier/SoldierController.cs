// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace DI_GameState.Entity
{
	public class SoldierController : EntityController
	{
		private List<DI_Game.Entity.Entity> entityList;
		
		new public void OnAwake()
		{
			entityList = new List<DI_Game.Entity.Entity>();
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onSoldierSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onSoldierDespawn", handleDespawn);
			base.OnAwake();
		}
		
		new public void handleSpawn(DI_Game.Entity.Entity _entity)
		{
			if (!entityList.Contains(_entity)) {
				entityList.Add(_entity);
			}
			base.handleSpawn(_entity);
		}
		
		new public void handleDespawn(DI_Game.Entity.Entity _entity)
		{
			if (entityList.Contains(_entity)) {
				entityList.Remove(_entity);
			}
			base.handleDespawn(_entity);
		}
		
		new public void OnDestroy()
		{
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onSoldierSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onSoldierDespawn", handleDespawn);
			base.OnDestroy();
		}
	}
}