// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections.Generic;

namespace DI_GameState.Entity
{
	public class ActorController : EntityController
	{
		
		new public void OnAwake()
		{
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onActorSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.addListener("onActorDespawn", handleDespawn);
			base.OnAwake();
		}
		
		new public void handleSpawn(DI_Game.Entity.Entity _entity)
		{
			base.handleSpawn(_entity);
		}
		
		new public void handleDespawn(DI_Game.Entity.Entity _entity)
		{
			base.handleDespawn(_entity);
		}
		
		new public void OnDestroy()
		{
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onActorSpawn", handleSpawn);
			DI_Events.EventCenter<DI_Game.Entity.Entity>.removeListener("onActorDespawn", handleDespawn);
			base.OnDestroy();
		}
	}
}