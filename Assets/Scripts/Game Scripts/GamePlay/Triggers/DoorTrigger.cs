// /*
// *
// * 	Devils Inc Studios
// * 	How Long
// * 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
// *	
// *	TODO: Include a description of the file here.
// *
// */

using UnityEngine;

namespace DI_Game {
[AddComponentMenu("Triggers/Doors/Door Trigger")]
	public class DoorTrigger : MonoBehaviour {
		// The handle of the door for IK target.
		public GameObject doorHandle;
		// The door itsef.
		public GameObject door;
		// Is this a double door?
		public bool doubleDoor = false;
		// The handle of the second door for IK target.
		public GameObject doorHandleTwo;
		// Second Door
		public GameObject doorTwo;

		// Should we be displaying a gui?
		private bool displayMessage;
		private int playerControlling;

		// The controller attached to the door.
		private DoorController doorOneController;
		private DoorController doorTwoController;
		// The game state controller.
		private DI_GameState.GameStateController GSC;

		public void Start() {
			// Get the door controller.
			doorOneController = door.GetComponent<DoorController>();
			doorTwoController = doorTwo.GetComponent<DoorController>();
			// Get the GSC
			GSC = GameObject.FindGameObjectWithTag("Master Mind").GetComponent<DI_GameState.GameStateController>();
			displayMessage = false;
		}

		public void OnGUI() {
			if (displayMessage) {
				if (playerControlling != 0) {
					Color previousColor = GUI.color;
					Color previousBackground = GUI.backgroundColor;
					int previousSize = GUI.skin.box.fontSize;

					// Display a message for the player
					GUILayout.BeginArea(new Rect(Screen.width / 2 - 100, Screen.height - 20, 200, 20));
					string openOrClose = "";
					if (doorOneController.isOpen) {
						openOrClose = "close";
					}
					else {
						openOrClose = "open";
					}


					GUI.color = Color.red;
					GUI.backgroundColor = Color.black;
					GUI.skin.box.fontSize = 20;

					GUILayout.Label("Press [" + DI_Input.BindManager.getKey(DI_Input.RegisteredKeys.INTERACT, playerControlling) + "] to " + openOrClose + " door.");
					GUILayout.EndArea();

					GUI.color = previousColor;
					GUI.backgroundColor = previousBackground;
					GUI.skin.box.fontSize = previousSize;
				}
			}
		}

		// When something enters the trigger zone.
		public void OnTriggerEnter(Collider triggeringObject)
		{
			// Is the entering object tagged as a player.
			if (triggeringObject.gameObject.tag == "Player One"
			    || triggeringObject.gameObject.tag == "Player Two")
			{
				playerControlling = triggeringObject.gameObject.GetComponent<CharacterMovement>().playerControlling;
				displayMessage = true;
			}
		}

		// While something is in our trigger zone
		public void OnTriggerStay(Collider triggeringObject)
		{
			// Is the entering object tagged as a player.
			if (triggeringObject.gameObject.tag == "Player One"
			    || triggeringObject.gameObject.tag == "Player Two")
			{
				// Check to see if we should attempt to trigger or not.
				if (!doorOneController.isOpening && !doorOneController.isClosing) {
					playerControlling = triggeringObject.gameObject.GetComponent<CharacterMovement>().playerControlling;
					// If the player is pressing the interact button.
					if (GSC.getKeyState(DI_Input.RegisteredKeys.INTERACT, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
						// TODO add animation for player.
						// Open the door if its close, close the door if its open.
						if (doorOneController.isOpen) {
							doorOneController.closeDoor();
							if (doubleDoor) {
								doorTwoController.closeDoor();
							}
						}
						else {
							doorOneController.openDoor();
							if (doubleDoor) {
								doorTwoController.openDoor();
							}
						}
					}
				}
			}
		}

		// When something exits our trigger zone
		public void OnTriggerExit(Collider triggeringObject) {
			// Is the entering object tagged as a player.
			if (triggeringObject.gameObject.tag == "Player One"
			    || triggeringObject.gameObject.tag == "Player Two")
			{
				Debug.Log("Player Exiting Trigger.");
				displayMessage = false;
			}
		}
	}
}