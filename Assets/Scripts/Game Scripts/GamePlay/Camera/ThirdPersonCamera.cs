/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Third person camera
*
/// <summary>
/// Third person camera.
/// </summary>
//TODO: - THE AVATAR GETS ROTATED IN THE Y AXES WHEN RUNNING STRAIGHT (SO RUNS WONKY) WHILE  THE TURNSPEED ANIMATOR VALUE IS 0 MEANING THE DEFAULT OFFSET 
	DOESNT GET UPDATED.  WHICH WILL MEAN IT JUMPS WHEN THE TURN VALUE CHANGES FROM 0 AGAIN
			
		- THE JUMP TO AIM POSITION IS TOO SLOW AND NEEDS ROTATION LERPED ALSO
		
		- RETURNING FROM AIM POSITION IS NOT AS DESIRED AND DOES NOT TRIGGER WHEN IT HAS RETURNED TO DEFAULT SO GET STUCK IN THE RETURN_FROM_AIM STATE
		MEANING USER CAN NOT ROTATE 
		
		-CAMERA NEAR CLIP PLANE SHOULD BE GIVEN COLLIDER SO THAT WHEN IT ENTERS PLAYER THE PLAYER MESH IS NOT RENDERED AVOIDING SEEING INSIDE PLAYER
*/

using UnityEngine;
using System.Collections;

namespace DI_Camera {
	[AddComponentMenu("Cameras/Third Person")]
	public class ThirdPersonCamera : MonoBehaviour {

		/*******************************************************************************************************************************
		MEMBERS
		*******************************************************************************************************************************/
		public GameObject avatar;
		private LayerMask mask;
		public ThirdPersonCameraStates state;
			
		//PLAYER ANIMATOR REFERENCES	
		private Animator animator;
		private int turnState = Animator.StringToHash ("Base Layer.Turning");
		
		//POSITIONS//
		public GameObject defaultPosition;
		public GameObject aimPosition;
		public GameObject cameraTarget; //What the camera should be looking at when rotating
		private Vector3 defaultOffset; //offset vector of camera from targetpos when not rotated
		private float distanceTrigger = 0.07f; //The distance a position needs to be to its target to be considered the same position
		private Vector3[] _nearPlaneCorners = new Vector3[4];
		
		//TIMER VALUES//
		private float returnTimer = 0f;
		private const float TIMER_LIMIT = 2f;

		//ROTATION VALUES//
		public float xRotation; //The rotations to orbit around the player
		public float yRotation;
		public float xRotationMax;//The orbit rotations limits
		public float yRotationMax;
		Quaternion offsetRotation; // The rotation of the camera offset position caused by the avatar themselves turning in the y axes
		Quaternion startingAvatarRotation; //the rotation of the player when loaded 
		public const float ROTATION_TRIGGER = 0.01f; //how close a rotation value needs to be to 0 to be considered 0;
		
		
		//SPEEDS//
		public float xRotationSpeed;
		public float yRotationSpeed;
		public float aimSnapSpeed;
		public float aimToDefaultSpeed;
		public float orbitToDefaultSpeed;
				
		//FLAGS
		private bool isAiming;
	
		private float maxDistance;

		private DI_GameState.GameStateController GSC;

		/*******************************************************************************************************************************
		START
		*******************************************************************************************************************************/
		public void Start() {
			// This is used when raycasting to ignore the player as a raycast target
			// This prevents the camera from thinking the player is a camera blocking object.
			// Add avatar's own layer to mask
			mask = 1 << avatar.gameObject.layer;
			// Add Ignore Raycast layer to mask
			mask |= 1 << LayerMask.NameToLayer("Ignore Raycast");
			// Invert mask
			mask = ~mask;
			
			// Set the max distance to raycast out.
			maxDistance = Vector3.Distance(defaultPosition.transform.position,aimPosition.transform.position);	
			
			defaultOffset = defaultPosition.transform.position - cameraTarget.transform.position;
			
			state = ThirdPersonCameraStates.DEFAULT;
			
			animator = gameObject.transform.parent.GetComponentInChildren<Animator>();
			
			//Gets the starting rotation of the players avatar //must be at index 1
			startingAvatarRotation = avatar.transform.rotation;
			
			offsetRotation = Quaternion.identity;

			GSC = GameObject.FindGameObjectWithTag("Master Mind").GetComponent<DI_GameState.GameStateController>();
		}
		
		/*******************************************************************************************************************************
		UPDATE
		*******************************************************************************************************************************/
		public void Update()
		{
			//Soley for debug purposes to know if behind shoulder vector is correct
			Vector3 debug = offsetRotation * defaultOffset;
			
			Debug.DrawRay(cameraTarget.transform.position, debug,Color.yellow);
		}
		
		/*******************************************************************************************************************************
		FIXED UPDATE
		*******************************************************************************************************************************/
		public void LateUpdate() 
		{
			// Only update the camera if we are playing.
			if (GSC.getGameState() == DI_Game.GameState.PLAYING) {
				//TODO: NEED TO ROTATE THE DEFAULT OFFSET USING THE ANIMATIONS TURN SPEED SOMEHOW SO THE DEFAULT OFFSET IS UP TO DATE (I.E THE VECTOR STILL GOES BEHIND AVATARS SHOULDER)
			
				float turnAngle = animator.GetFloat ("Turn Angle");
				
				if(turnAngle != 0)
				{
					//Calculate how much the avatar has turned in y axes from starting direction
					float yEuler = avatar.transform.rotation.eulerAngles.y - startingAvatarRotation.eulerAngles.y;
					
					//create quaternion to turn the default camera offset to stay in line with avatar
					offsetRotation = Quaternion.Euler(new Vector3(0f,yEuler,0f));
				}
				
				//a temp variable used throughout this method
				Vector3 distance; 
				
				//If user preses reset button
				//TODO: DETERMINE WHAT PLAYER THIS SCRIPT INSTANCE IS ATTACHED TO
//				if(DI_Input.InputManager.getKeyDown(DI_Input.RegisteredKeys.RESET_CAMERA, 1) == DI_Input.KeyStates.KEY_PRESSED)
//				{
//					//move back to default position
//					state = ThirdPersonCameraStates.DEFAULT;
//					transform.position = defaultPosition.transform.position;
//				}
				
				switch(state)
				{
				case ThirdPersonCameraStates.DEFAULT:
					transform.position = cameraTarget.transform.position + defaultOffset;
					
					if(DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_X,1) != 0f 
					   || DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_Y,1) != 0f)
					{
						//TODO: DETERMINE WHICH PLAYER THE CAMERA IS ATTACHED TO
						xRotation = Mathf.Clamp (xRotation - DI_Input.InputManager.getAxisValue (DI_Input.RegisteredKeys.CAMERA_Y, 1) * xRotationSpeed * Time.deltaTime, -xRotationMax, xRotationMax);
						yRotation = Mathf.Clamp (yRotation + DI_Input.InputManager.getAxisValue (DI_Input.RegisteredKeys.CAMERA_X, 1) * yRotationSpeed * Time.deltaTime, -yRotationMax, yRotationMax);
						state = ThirdPersonCameraStates.ORBITING;
					}
					
					else if(animator.GetFloat ("Speed") >= 3.0f)
					{
						state = ThirdPersonCameraStates.SPRINT_TRACKING;
					}
					break;
					
					
				case ThirdPersonCameraStates.GO_TO_AIM:
					//Distance between the position we want and the actual position of the cam
					distance = aimPosition.transform.position - transform.position; 
					
					//If camera is practically at the target
					if(distance.sqrMagnitude <= distanceTrigger * distanceTrigger)
					{
						//We position it at exactly the aim position
						transform.position = aimPosition.transform.position;
						state = ThirdPersonCameraStates.AIMING;
					}
					
					else
					{
						//If not we move it towards the aim pos
						transform.position = Vector3.Lerp (transform.position, aimPosition.transform.position, aimSnapSpeed * Time.deltaTime);
					}
					return;
					
					
				case ThirdPersonCameraStates.AIMING:
					transform.position = aimPosition.transform.position;
					transform.rotation = aimPosition.transform.rotation;
					return;
				
						
				case DI_Camera.ThirdPersonCameraStates.RETURN_FROM_AIM:
					//Distance between the position we want and the actual position of the cam
					distance = defaultPosition.transform.position - transform.position; 
					
					//If camera is practically at the target
					if(distance.sqrMagnitude <= distanceTrigger * distanceTrigger)
					{
						//We position it at exactly the default position
						transform.position = defaultPosition.transform.position;
						//The returning to default position process is complete and the user can now orbit the avatar
						state = ThirdPersonCameraStates.DEFAULT;
					}
					
					else
					{
						//If not we move it towards the default pos
						transform.position = Vector3.Lerp (transform.position, defaultPosition.transform.position, aimToDefaultSpeed * Time.deltaTime);			
					}
					break;
					
					
				case ThirdPersonCameraStates.ORBITING:
					//TODO: DETERMINE WHICH PLAYER THE CAMERA IS ATTACHED TO
					//If the player has stopped rotating the camera
					if(DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_X,1) == 0f 
					&& DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_Y,1) == 0f)
					{
						//Check see if player has not rotated for set period of time
						returnTimer += Time.deltaTime;
						if(returnTimer >= TIMER_LIMIT)
						{
							//If so then it is time to return the camera back to its default
							//rotationRatio = Mathf.Abs(yRotation / xRotation);
							state = ThirdPersonCameraStates.RETURN_FROM_ORBIT;
						}
					}
					
					else
					{
						returnTimer = 0f;
						//TODO: DETERMINE WHICH PLAYER THE CAMERA IS ATTACHED TO
						xRotation = Mathf.Clamp (xRotation - DI_Input.InputManager.getAxisValue (DI_Input.RegisteredKeys.CAMERA_Y, 1) * xRotationSpeed * Time.deltaTime, -xRotationMax, xRotationMax);
						yRotation = Mathf.Clamp (yRotation + DI_Input.InputManager.getAxisValue (DI_Input.RegisteredKeys.CAMERA_X, 1) * yRotationSpeed * Time.deltaTime, -yRotationMax, yRotationMax);
					}
					break;
					
					
					
				case ThirdPersonCameraStates.RETURN_FROM_ORBIT:	
					//If camera was returning to default but then user rotates again 
					if(DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_X,1) != 0f 
					   || DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_Y,1) != 0f)
					{
						//TODO: DETERMINE WHICH PLAYER THE CAMERA IS ATTACHED TO
						//Track the rotation and return to the orbiting state
						xRotation = Mathf.Clamp (xRotation - DI_Input.InputManager.getAxisValue (DI_Input.RegisteredKeys.CAMERA_Y, 1) * xRotationSpeed * Time.deltaTime, -xRotationMax, xRotationMax);
						yRotation = Mathf.Clamp (yRotation + DI_Input.InputManager.getAxisValue (DI_Input.RegisteredKeys.CAMERA_X, 1) * yRotationSpeed * Time.deltaTime, -yRotationMax, yRotationMax);
						state = ThirdPersonCameraStates.ORBITING;
					}
					
					else
					{
						//reduce rotations to move back to default position (behind avatar)
						xRotation = Mathf.Lerp (xRotation, 0f, orbitToDefaultSpeed * Time.deltaTime);
						yRotation = Mathf.Lerp (yRotation,0f, orbitToDefaultSpeed * Time.deltaTime);
						
						//If camera has moved back to the default position
						if(xRotation >= -ROTATION_TRIGGER && xRotation <= ROTATION_TRIGGER &&
						   yRotation >= -ROTATION_TRIGGER && yRotation <= ROTATION_TRIGGER)
						{
							//Reset rotations
							xRotation = 0f;
							yRotation = 0f;
							//change to default state
							state = ThirdPersonCameraStates.DEFAULT;
						}
					}
					break;
					
				case ThirdPersonCameraStates.SPRINT_TRACKING:
					//if player wants to orbit while spriting
					if(DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_X,1) != 0f 
					   || DI_Input.InputManager.getAxisValue(DI_Input.RegisteredKeys.CAMERA_Y,1) != 0f)
					{
						//change to default state and it will handle the rotation and set to orbiting next frame
						state = ThirdPersonCameraStates.DEFAULT;
					}
					
					//if player has slowed down we move back to the default position 
					else if(animator.GetFloat("Speed") < 3.0f)
					{
						state = ThirdPersonCameraStates.DEFAULT;
					}
					break;
				
				}
			
				CalculatePosition();
			}
		}
		
		/*******************************************************************************************************************************
		CALCULATE NEAR PLANE CORNERS
		*******************************************************************************************************************************/
		private void CalculateNearPlaneCorners(Vector3 CameraPosition)
		{
			float halfFOV = (float) ( (gameObject.camera.fieldOfView * 0.5) * Mathf.Deg2Rad);
			float nearClipDistance = gameObject.camera.nearClipPlane;
			
			//Calculate height and width of viewport
			float height = Mathf.Tan (halfFOV) * nearClipDistance;
			float width = height * gameObject.camera.aspect;
			
			//calculate local axis for this camera position
			Vector3 forward;
			if(state != ThirdPersonCameraStates.DEFAULT)
			{
				forward = transform.forward;
			}
			else
			{
				forward = defaultPosition.transform.forward;
			}
			//Debug.DrawRay (CameraPosition, forward, Color.red);
			Vector3 right = Vector3.Cross(Vector3.up, forward);
			//Debug.DrawRay (CameraPosition, right, Color.blue);
			Vector3 up = Vector3.Cross(forward, right);
			//Debug.DrawRay (CameraPosition, up, Color.yellow);
			
			forward.Normalize();
			right.Normalize ();
			up.Normalize ();
			
			//Middle point of the clipping plane used to work out all the corners
			Vector3 middle = CameraPosition + (forward * nearClipDistance);
			
			//LOWER RIGHT
			_nearPlaneCorners[0] = middle + (right * width) ;
			_nearPlaneCorners[0] -= up * height;
			//UPPER RIGHT
			_nearPlaneCorners[1] = middle + (right * width) ;
			_nearPlaneCorners[1] += up * height;
			//LOWER LEFT
			_nearPlaneCorners[2] = middle - (right * width) ;
			_nearPlaneCorners[2] -= up * height;
			//UPPER LEFT
			_nearPlaneCorners[3] = middle - (right * width) ;
			_nearPlaneCorners[3] += up * height;
		}
		
		/*******************************************************************************************************************************
		CALCULATE POSITION
		Positions the camera according to the rotations and the geometry collision tests
		*******************************************************************************************************************************/
		private void CalculatePosition()
		{
			if(state == ThirdPersonCameraStates.SPRINT_TRACKING)
			{
				transform.position = GeometryCheck(defaultPosition.transform.position);
				transform.rotation = defaultPosition.transform.rotation;
			}
			
			else
			{		
				Vector3 cameraOffset;
				
				//rotations around the y axes...
				Quaternion yRot = Quaternion.Euler(0f,yRotation,0f);
				//and x axes determined by the mouse movement
				Quaternion xRot = Quaternion.Euler(xRotation,0f,0f);
											
				//the default offset vector of cam is rotated by what avatar has turned and with the mouse rotations
				cameraOffset = offsetRotation * yRot * xRot * defaultOffset;
				
				
				
				Debug.DrawRay(cameraTarget.transform.position, cameraOffset,Color.white);
		
				//and offset it from the camera's target
				cameraOffset += cameraTarget.transform.position;
							
				//now set the camera's position to this
				transform.position = GeometryCheck(cameraOffset);
				//and make sure the camera looks at it's target
				transform.LookAt (cameraTarget.transform.position);			
			}
		}
		
		/*******************************************************************************************************************************
		GEOMETRY CHECK
		Checks see if any of the corners of the near clipping plane (if camera placed at desired position) are inside any geometry.
		If they are, a further foward position will be returned which fixes this problem
		*******************************************************************************************************************************/
		private Vector3 GeometryCheck(Vector3 desiredPosition)
		{
			//the distance the camera needs to be positioned so that the view is not inside geometry
			float newDistance = maxDistance;
			
			Vector3 targetPos = (state == ThirdPersonCameraStates.DEFAULT) ? defaultPosition.transform.position + defaultPosition.transform.forward * maxDistance: cameraTarget.transform.position;
			
			//gets the near clipping plane corner positions
			CalculateNearPlaneCorners(desiredPosition);
			
			//by ray casting near plane points rather than one point we make sure that no part of the viewport is within 
			//geometry
			for(int j = 0; j < _nearPlaneCorners.Length; j++)
			{
				//offset from the cam position and the point we are testing
				Vector3 offset = _nearPlaneCorners[j] - desiredPosition;
				
				//the ray cast (same direction as cam pos to target)
				Vector3 ray = _nearPlaneCorners[j] - ( targetPos + offset);
				float rayLength = ray.magnitude;
				
				RaycastHit hit;
				
				/////////////////////////////////////////////////////////////
				Debug.DrawRay (targetPos + offset, ray, Color.green);
				////////////////////////////////////////////////////////////
				
				if(Physics.Raycast (targetPos + offset, ray, out hit, rayLength, mask))
				{
					//if the ray hits something before it reaches the near plane corner it means the corner was
					//inside of a collider. by tracking the smallest distance we make sure the corner furthest inside
					//geometry will be catered for
					newDistance = Mathf.Min(newDistance, hit.distance);
					///////////////////////////////////////
					ray.Normalize ();
					Debug.DrawRay (targetPos + offset, ray * hit.distance, Color.magenta);
					///////////////////////////////////////////////////////////////
				}
			}
			
			//we can use this new position so get how far we need to move camera
			float camFwdDistance = maxDistance - newDistance;
			//along its z axis
			Vector3 posToTarget = targetPos - desiredPosition;
			posToTarget.Normalize ();
			
			/////////////////////////////////////////////////////////
			Debug.DrawRay (desiredPosition, posToTarget * camFwdDistance, Color.black);
			////////////////////////////////////////////////
			
			//move forward along facing direction
			Vector3 newPosition = desiredPosition + (posToTarget * camFwdDistance);
			return newPosition;
			
			/*if(newDistance < minDistance)
			{
				//TODO:This may mean we are now inside the player so might be a good idea to make their mesh transparent until we leave but REMEMBER TO MAKE IT OPAQUE AFTER
			}*/
		}
		
		/*******************************************************************************************************************************
		SET AIMING
		Currently called from the CharacterMovement script.
		*******************************************************************************************************************************/
		public void setAiming(bool aiming) {
			// If we are aiming just put the camera at the close ("aiming") position.
			if (aiming) {
				if (!isAiming) 
				{		
					isAiming = true;
					state = ThirdPersonCameraStates.GO_TO_AIM;
					transform.rotation = aimPosition.transform.rotation;
					xRotation = 0f;
					yRotation = 0f;
				}
			}
			// Otherwise, set it at max range.
			// TODO smooth this transition.
			else {
				if (isAiming)
				{
					state = ThirdPersonCameraStates.RETURN_FROM_AIM;
					isAiming = false;
				}
			}
		}
	}
}