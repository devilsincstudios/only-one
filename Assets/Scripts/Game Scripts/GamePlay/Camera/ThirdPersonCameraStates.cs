﻿using UnityEngine;
using System.Collections;

namespace DI_Camera
{
	
	public enum ThirdPersonCameraStates
	{
		ORBITING,
		RETURN_FROM_ORBIT,
		GO_TO_AIM,
		AIMING,
		RETURN_FROM_AIM,
		DEFAULT,
		SPRINT_TRACKING
	};
}
