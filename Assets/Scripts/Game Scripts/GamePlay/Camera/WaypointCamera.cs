using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text; 
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;

// 	Devils Inc Studios
// 	How Long
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//
namespace DI_Camera 
{
	[AddComponentMenu("Cameras/Waypoint Camera")]
	public class WaypointCamera : MonoBehaviour
	{
		/*******************************************************************************************************************************
		MEMBERS
		*******************************************************************************************************************************/
		public List<CameraWaypoint> waypoints = new List<CameraWaypoint>();
		public bool loop = true; //whether the camera should jump to the beginning waypoint after finishing the last one.
		public Color editorColour = Color.cyan;//color of the scene GUI for the handles
		public string waypointsID = ""; //ID of the collection of waypoints	
		public TextAsset file = null; //File containing already saved waypoint data
		private bool instantTranslation;
		private bool instantRotation;
		private short index; //the index into the list of the current way point
		
		/*******************************************************************************************************************************
		PROPERTIES
		*******************************************************************************************************************************/

		
		/*******************************************************************************************************************************
		ON ENABLE / DISABLE
		*******************************************************************************************************************************/
		void OnEnable()
		{				
			if(gameObject.GetComponent<Camera>() == null)
			{
				Debug.LogWarning(string.Format("The gameobject \"{0}\" has no camera component but is using a waypoint camera script", gameObject.ToString())); 
			}
			
			if(waypoints.Count <= 0)
			{
				Debug.LogError(string.Format ("The waypoint camera \"{0}\" has no waypoints", gameObject.ToString ()));
				return;
			}
			
			transform.position = waypoints[0].position;
			transform.rotation = waypoints[0].rotation;
			index = 0;

			//Add event listener so can change to desired WP anywhere in script
			DI_Events.EventCenter<string>.addListener("ChangeCameraWaypoint",ChangeWaypoint);
		}
		
		void OnDisable()
		{
			//Disable listener
			DI_Events.EventCenter<string>.removeListener ("ChangeCameraWaypoint",ChangeWaypoint);
		}
		
		/*******************************************************************************************************************************
		UPDATE
		*******************************************************************************************************************************/
		void Update()
		{
			if(waypoints.Count >= 0)
			{
				if(index >= waypoints.Count)
				{
					//if we have reached the last waypoint and are not looping back to the beginning one we just stick on the last one
					return;
				}
				
				//If camera is ready to move on to next waypoint	
				if(waypoints[index].IsFinished())
				{
					//get next waypoint
					index++;
					//if moving on from the final waypoint
					if(index >= waypoints.Count)
					{
						if(loop)
						{
							//go back to the beginning waypoint
							index = 0;
						}
						else
						{
							return;
						}
					}

					Debug.Log(string.Format ("Moved to Camera Waypoint \"{0}\" Using Timer", waypoints[index].ID));
					
					//if camera must move to next position straight away
					if(waypoints[index].speed >= CameraWaypoint.INSTANT_TRANSITION)
					{
						instantTranslation = true;
						transform.position = waypoints[index].position;
					}
					
					//otherwise instant translation is false so will lerp to new position
					else
					{
						instantTranslation = false;
					}
					
					if(waypoints[index].rotationSpeed >= CameraWaypoint.INSTANT_TRANSITION)
					{
						instantRotation = true;
						transform.rotation = waypoints[index].rotation;
					}
					
					else
					{
						instantRotation = false;
					}
				}
				
				if(!instantRotation)
				{
					//rotate to new direction smoothly
					transform.rotation = Quaternion.Lerp(transform.rotation, waypoints[index].rotation, Time.deltaTime * waypoints[index].rotationSpeed);
				}
				
				if(!instantTranslation)
				{
					//move to new position smoothly
					transform.position = Vector3.Lerp(transform.position, waypoints[index].position, Time.deltaTime * waypoints[index].speed);
				}
			}
			
			else
			{
				Debug.LogError ("The Waypoint Camera Has No Waypoints");
			}
		}
		
		/*******************************************************************************************************************************
		CHANGE WAY POINT
		*******************************************************************************************************************************/
		private void ChangeWaypoint(string ID)
		{
			for(int i = 0; i < waypoints.Count; i++)
			{
				if(waypoints[i].ID == ID)
				{
					//move to identified waypoint
					index = (short)i;
					Debug.Log(string.Format ("Moved to Camera Waypoint \"{0}\" Using Event", waypoints[index].ID));
					
					//if camera must move to next position straight away
					if(waypoints[index].speed >= CameraWaypoint.INSTANT_TRANSITION)
					{
						instantTranslation = true;
						transform.position = waypoints[index].position;
					}
					
					//otherwise instant translation is false so will lerp to new position
					else
					{
						instantTranslation = false;
					}
					
					if(waypoints[index].rotationSpeed >= CameraWaypoint.INSTANT_TRANSITION)
					{
						instantRotation = true;
						transform.rotation = waypoints[index].rotation;
					}
					
					else
					{
						instantRotation = false;
					}
				}
			}
		}
		
		/*******************************************************************************************************************************
		SERIALIZE / DESERIALIZE
		*******************************************************************************************************************************/
		
		public void Load()
		{
			//Cannot load with no file present
			if(file == null)
			{
				Debug.LogError("File is empty");
				return;				
			}
			
			try
			{
				//empty the current waypoints ready to add ones on file
				waypoints.Clear();
				
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(file.text);
				
				//ID of the whole set
				XmlNode root = doc.DocumentElement;
				waypointsID = root.Attributes["ID"].InnerText;
				//Whether the waypoints loop
				loop = bool.Parse(root.Attributes["LOOP"].InnerText);
				
				// array of the WPs 
				XmlNodeList waypointList = doc.GetElementsByTagName("WAYPOINT");  
				
				foreach (XmlNode waypoint in waypointList)
				{ 
					//Create the waypoint
					CameraWaypoint temp = new CameraWaypoint();
					
					//Get ID
					temp.ID = waypoint.Attributes["ID"].InnerText;
					XmlNodeList  dataSets = waypoint.ChildNodes;
					
					//Get all data about the waypoint
					foreach( XmlNode data in dataSets)
					{
						switch(data.Name)
						{
						case("DELAY"):
							temp.delay = float.Parse (data.InnerText);
							break;
						case("WAIT_FOR_EVENT"):
							temp.waitForEvent = bool.Parse(data.InnerText);
							break;
						case("POSITION"):
							//need as float
							temp.position.x = float.Parse (data.ChildNodes[0].InnerText);
							temp.position.y = float.Parse (data.ChildNodes[1].InnerText);
							temp.position.z = float.Parse (data.ChildNodes[2].InnerText);
							break;
						case("ROTATION"):
							temp.rotation.x = float.Parse (data.ChildNodes[0].InnerText);
							temp.rotation.y = float.Parse (data.ChildNodes[1].InnerText);
							temp.rotation.z = float.Parse (data.ChildNodes[2].InnerText);
							temp.rotation.w = float.Parse (data.ChildNodes[3].InnerText);	
							break;
						case("SPEED"):
							temp.speed = float.Parse(data.InnerText);
							break;
						case("ROTATION_SPEED"):
							temp.rotationSpeed = float.Parse (data.InnerText);
							break;
						}					
					}
					
					//Then add it to the list
					waypoints.Add (temp);
				}
			}
			
			catch(Exception e)
			{
				Debug.LogException (e);
			}
		}
		
		public void Save()
		{
			if(waypointsID == "")
			{
				Debug.LogError(String.Format("The camera waypoints set attached to \"{0}\" does not have an ID so could not be saved",gameObject.name));
				return;
			}
			
			try
			{
				//Check see if we have a working directory
				if (!Directory.Exists("Assets\\Camera Waypoint Sets\\"))
				{
					Directory.CreateDirectory("Assets\\Camera Waypoint Sets\\");
				}
				
				//Where file will be saved to
				string filePath = string.Format ("Assets\\Camera Waypoint Sets\\{0}.xml", waypointsID);	
								
				//Create root element
				XElement root = new XElement("WAYPOINTS");
				root.SetAttributeValue("ID", waypointsID);
				root.SetAttributeValue ("LOOP", loop.ToString());
				
				//Then create elements for each waypoint in list
				foreach (CameraWaypoint wp in waypoints)
				{
					XElement wpElement = new XElement("WAYPOINT");
					wpElement.SetAttributeValue("ID", wp.ID);
					root.Add(wpElement);
					
					//SAVE THE DATA : DELAY TIME
					XElement delay = new XElement ("DELAY",wp.delay.ToString());					
					wpElement.Add(delay);
					
					//SAVE THE DATA : WAIT FOR EVENT BOOL
					XElement waitForEvent = new XElement ("WAIT_FOR_EVENT", wp.waitForEvent.ToString());
					wpElement.Add(waitForEvent);
					
					//SAVE THE DATA : SPEED
					XElement speed = new XElement("SPEED", wp.speed.ToString ());
					wpElement.Add(speed);
					
					//SAVE THE DATA : ROTATION SPEED
					XElement rotSpeed = new XElement("ROTATION_SPEED", wp.rotationSpeed.ToString());
					wpElement.Add(rotSpeed);
					
					//SAVE THE DATA : POSITION
					XElement position = new XElement("POSITION");
					wpElement.Add(position);
						XElement posX = new XElement ("X", wp.position.x.ToString());
						position.Add(posX);
						XElement posY = new XElement ("Y", wp.position.y.ToString());
						position.Add(posY);
						XElement posZ = new XElement ("Z", wp.position.z.ToString());						
						position.Add(posZ);
					
					
					//SAVE THE DATA : ROTATION
					XElement rotation = new XElement ("ROTATION");
					wpElement.Add(rotation);
						XElement rotX = new XElement ("X", wp.rotation.x.ToString());
						rotation.Add(rotX);
						XElement rotY = new XElement ("Y", wp.rotation.y.ToString());
						rotation.Add(rotY);
						XElement rotZ = new XElement ("Z", wp.rotation.z.ToString());
						rotation.Add(rotZ);
						XElement rotW = new XElement ("W", wp.rotation.w.ToString());
						rotation.Add(rotW);
									
				}	
				
				root.Save(filePath);
			}
			
			catch(Exception e)
			{
				Debug.LogException(e);
			}				
		}
	}
}
