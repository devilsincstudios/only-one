﻿using UnityEngine;
using System.Collections;

/*
*
* 	Devils Inc Studios
* 	How Long
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	CAMERA WAYPOINT 
*	Describes a point in space that a camera can be
*/

namespace DI_Camera {
	[System.Serializable]
	public class CameraWaypoint
	{
		/*******************************************************************************************************************************
		MEMBERS
		*******************************************************************************************************************************/
		public float 		delay; //time to wait before moving to next waypoint(WP)
		public bool 		waitForEvent; //if this waypoint waits for event to move to next WP not delay	
		public string 		ID;	
		public bool 		activeInInspector; // a bool used for inspector use
		public float 		speed; //Speed at which camera travels from prev WP to this WP 
		public float 		rotationSpeed; //Speed at which camera rotates from prev WP rotation to this WP rotation
		public Vector3 		position;
		public Quaternion 	rotation;
		public static float  INSTANT_TRANSITION = 50f; //if either speeds are 100 it will mean they the camera is to be instantly transformed from one WP to the next
	
		private float		timer;
		
		/*******************************************************************************************************************************
		CONSTRUCTOR
		*******************************************************************************************************************************/
		public CameraWaypoint()
		{
			delay = 1f; 
			waitForEvent = false; 
			ID = "";	
			activeInInspector = false; 
			speed = 1f; 		
			rotationSpeed = 1f; 
			position = Vector3.zero;
			rotation = Quaternion.identity;
			timer = 0f;
		}
		
		/*******************************************************************************************************************************
		IS FINISHED
		Checks whether we are finished and need to move away from this node. Must be called every update by a camera.
		*******************************************************************************************************************************/
		public bool IsFinished()
		{
			if(waitForEvent)
			{
				return false;
			}
			
			timer += Time.deltaTime;
			
			//if we have waited enough time
			if(timer >= delay)
			{
				//reset the timer for when we next use the node and return true(we are ready to move on)
			 	timer = 0f;
				return true;			
			}
			
			else
			{
				return false;
			}	
		}
	}
}
