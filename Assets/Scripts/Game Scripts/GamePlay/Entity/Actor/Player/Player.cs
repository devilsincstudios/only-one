// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DI_Game.Entity
{
	[AddComponentMenu("Entity/Actor/Player")]
	public class Player : Actor
	{
		public PlayerType playerType;
		public int currentRest;
		public int maxRest;
		public string playerName;

		public Player(PlayerType type) {
			playerType = type;
			setInventory(new DI_Item.Inventory());
		}

		public Player(PlayerType type, DI_Item.Inventory newInventory) {
			playerType = type;
			setInventory(newInventory);
		}

		public void setMaxRest(int rest) {
			maxRest = rest;
		}
		public void setCurrentRest(int rest) {
			currentRest = rest;
		}
	}
}
