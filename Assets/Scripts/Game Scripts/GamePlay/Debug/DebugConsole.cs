/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	This is the debug console.
*	Entering/Selecting commands runs them against the game to help us with debugging.
*
*/

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace DI_Debug {
	[AddComponentMenu("Debug/Console/Debug Console")]
	public class DebugConsole : DI_GUI.GUIBase {

		public List<string> logContents;
		private Rect autoCompleteDimensions;
		private Rect logDimensions;
		private Rect inputDimensions;
		private string logContentsString;
		public List<String> autoComplete;
		private Vector2 logScroll;
		public string inputString = "";
		private Rect logViewArea;
		private Rect logScrollArea;
		private float slideInSpeed = 8f;
		private int slideInTick = 1;
		private float slideOutSpeed = 60.0f;
		private List<DebugCommand> commandObjects;
		public List<string> commands;
		private Vector2 autoCompleteScroll;

		//=======================================================================================================================
		public void OnEnable()
		{
			guiName = "Debug Console";

			logContents = new List<string>();
			logDimensions  = new Rect(0, 10, Screen.width / 2, Screen.height / 2);
			inputDimensions = new Rect(0, 10 + Screen.height / 2, Screen.width / 2, 50);
			autoCompleteDimensions = new Rect(0, 60 + Screen.height / 2, Screen.width / 2, 100);
			autoComplete = new List<string>();
			autoCompleteScroll = new Vector2(0, 0);

			commandObjects = new List<DebugCommand>();
			commands = new List<string>();

			logScroll = new Vector2(0, 0);
			logViewArea = new Rect(0, 20, logDimensions.width, logDimensions.height - 20);
			logScrollArea = new Rect(20, 20, 20, logDimensions.height - 20);

			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.addListener("onInput", handleInput);
			DI_Events.EventCenter<string>.addListener("onDebugMessage", handleDebugMessage);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.addListener("onGUIShow", handleGUIShow);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.addListener("onGUIHide", handleGUIHide);

			DI_Events.EventCenter<string>.invoke("onDebugMessage", "Console Ready");
			loadCommands();
		}

		public void handleInput(DI_Input.RegisteredKeys _key, DI_Input.KeyStates _state, int _player)
		{
			if (_key == DI_Input.RegisteredKeys.DEV_CONSOLE && _state == DI_Input.KeyStates.KEY_PRESSED) {
				if (_player == 1) {
					if (isVisable) {
						hideGUI();
					}
					else {
						showGUI();
					}
				}
			}
		}

		public void handleDebugMessage(string message)
		{
			while (logContents.Count >= 100) {
				logContents.RemoveAt(1);
			}
			logContents.Add(DateTime.Now + ": " + message);
		}

		public new void OnGUI() {
			if (isVisable) {
				base.OnGUI();
				GUI.Window ((int)DI_GUI.GUIId.DEBUG_LOG, logDimensions, drawGUI, "Developer Console - Pre-Alpha Build");
				GUI.Window ((int)DI_GUI.GUIId.DEBUG_INPUT, inputDimensions, drawGUI, "");
				if (autoComplete.Count != 0) {
					GUI.Window ((int)DI_GUI.GUIId.DEBUG_AUTO_COMPLETE, autoCompleteDimensions, drawGUI, "Suggestions");
				}

				Event currentEvent = Event.current;
				if (currentEvent.keyCode == KeyCode.Return && currentEvent.rawType == EventType.used) {
					parseInputString();
				}
				else {
					updateSuggestions();
				}
			}
		}
		//=======================================================================================================================

		public IEnumerator slideIn()
		{
			slideInTick = 1;

			while (slideInTick / slideInSpeed <= 1) {
				float currentPositionX = Screen.width / 4 * (slideInTick / slideInSpeed);
				logDimensions.x = currentPositionX;
				logDimensions.y = 10 * (slideInTick / slideInSpeed);

				inputDimensions.x = currentPositionX;
				inputDimensions.y = (10 + Screen.height / 2) * (slideInTick / slideInSpeed);

				autoCompleteDimensions.x = currentPositionX;
				autoCompleteDimensions.y = (60 + Screen.height / 2) * (slideInTick / slideInSpeed);
				++slideInTick;
				yield return new WaitForSeconds(0.01f);
			}
		}

		public IEnumerator slideOut()
		{
			while (true) {
				float currentPositionX = logDimensions.x - slideOutSpeed;
				logDimensions.x = currentPositionX;
				logDimensions.y = 10 - slideOutSpeed;
				
				inputDimensions.x = currentPositionX;
				inputDimensions.y = (10 + Screen.height / 2) - slideOutSpeed;
				
				autoCompleteDimensions.x = currentPositionX;
				autoCompleteDimensions.y = (50 + Screen.height / 2) - slideOutSpeed;
				if (currentPositionX < -1 * (Screen.width / 2 + 5)) {
					break;
				}
				else {
					yield return new WaitForSeconds(0.01f);
				}
			}
			isVisable = false;
			DI_Events.EventCenter<DI_Game.GameState>.invoke("onGameStateChanged", DI_Game.GameState.PLAYING);
		}

		public void updateSuggestions()
		{
			autoComplete.Clear();
			if (inputString != "") {
				foreach (String command in commands.ToArray()) {
					if (command.ToUpper().IndexOf(inputString.ToUpper()) != -1) {
						autoComplete.Add(command);
					}
				}
			}
		}

		public void parseInputString()
		{
			bool foundMatch = false;
			int iteration = 0;
			foreach (String command in commands.ToArray()) {
				if (inputString.ToUpper() == command.ToUpper()) {
					DI_Events.EventCenter<string>.invoke("onDebugMessage", inputString.ToUpper());
					foundMatch = true;
					commandObjects[iteration].runFunction();
					break;
				}
				++iteration;
			}

			if (!foundMatch) {
				DI_Events.EventCenter<string>.invoke("onDebugMessage", "Unknown command: " + inputString);
			}

			inputString = "";
			autoComplete.Clear();
		}

		// Show the gui.
		public void handleGUIShow(DI_GUI.GUIId guiId, int player)
		{
			if (guiId == DI_GUI.GUIId.DEBUG_CONSOLE) {
				showGUI();
			}
		}

		// Hide the gui.
		public void handleGUIHide(DI_GUI.GUIId guiId, int player)
		{
			if (guiId == DI_GUI.GUIId.DEBUG_CONSOLE) {
				hideGUI();
			}
		}

		public void loadCommands()
		{
			foreach (Type type in typeof(DebugCommand).Assembly.GetTypes()) {
				if (type.IsSubclassOf(typeof(DebugCommand))) {
					//UnityEngine.Debug.Log(type.ToString());
					DebugCommand newCommand = (DebugCommand)Activator.CreateInstance(type);
					commandObjects.Add(newCommand);
					commands.Add(newCommand.command);
				}
			}
		}

		public new void showGUI()
		{
			base.showGUI();
			StartCoroutine(slideIn());
			UnityEngine.Debug.Log("Show GUI");
		}
		public new void hideGUI()
		{
			StartCoroutine(slideOut());
			UnityEngine.Debug.Log("Hide GUI");
		}

		public override void drawGUI (int guiId)
		{
			switch (guiId) {
				case (int) DI_GUI.GUIId.DEBUG_AUTO_COMPLETE:
					drawAutoComplete();
				break;
				case (int) DI_GUI.GUIId.DEBUG_INPUT:
					drawInput();
				break;
				case (int) DI_GUI.GUIId.DEBUG_LOG:
					drawLog();
				break;
			}
		}

		public void drawLog()
		{
			logScroll = GUI.BeginScrollView(new Rect(0, 20, Screen.width / 2 - 3, Screen.height / 2 - 20), logScroll, new Rect(0, 0, Screen.width / 2 - 27, logContents.Count * 20));
			int currentEntry = 1;
			foreach (string logEntry in logContents.ToArray()) {
				GUI.Label(new Rect(10, currentEntry * 20, Screen.width/2 - 20, 20), logEntry, leftAligned);
				++currentEntry;
			}
			GUI.EndScrollView(true);
		}

		public void drawInput()
		{
			inputString = GUI.TextField(new Rect(10, 0, inputDimensions.width, inputDimensions.height - 20), inputString, new GUIStyle("Command Entry"));
		}

		public void drawAutoComplete()
		{
			int iteration = 0;
			autoCompleteScroll = GUI.BeginScrollView(new Rect(0, 20, Screen.width / 2 - 10, 80), autoCompleteScroll, new Rect(0, 0, Screen.width / 2 - 5, autoComplete.Count * 20));

			foreach (string command in autoComplete.ToArray()) {
				if (GUI.Button(new Rect(10, iteration * 20, 200, 20), command, new GUIStyle("button"))) {
					inputString = command;
					autoComplete.Clear();
				}
				GUI.Label(new Rect(220, iteration * 20, Screen.width / 2 - 230, 20), commandObjects[commands.IndexOf(command)].description, leftAligned);
				++iteration;
			}

			GUI.EndScrollView(true);
		}

		public void OnDisable()
		{
			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.removeListener("onInput", handleInput);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.removeListener("onGUIShow", handleGUIShow);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.removeListener("onGUIHide", handleGUIHide);
			DI_Events.EventCenter<string>.removeListener("onDebugMessage", handleDebugMessage);
		}
	}
}