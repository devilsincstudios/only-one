/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Main menu for the prototype, currently used as a stage select.
*
*/

using UnityEngine;
using System.Collections.Generic;

namespace DI_Game
{
	[AddComponentMenu("Menus/Main/MainMenu")]
	public class MainMenu : MonoBehaviour {
		private LayerMask textLayer;
		private AudioClip buttonSound;
		private GameObject lastHit;
		private Dictionary<MenuItem, bool> hitList;
		public GameObject loadingScreen;
		public List<GameObject> menuItems;
		private List<MenuItem> cachedHitList;
		public Camera activeCamera;
		public GameObject menuGroup;
		private bool mouseHeld;

		void OnEnable() {
			// We only care about the text objects, ignore the rest of the scene.
			textLayer = 1 << LayerMask.NameToLayer("Text");
			hitList = new Dictionary<MenuItem, bool>();
			foreach (GameObject gameObject in menuItems.ToArray()) {
				hitList.Add(gameObject.GetComponent<MenuItem>(), false);
				//Debug.Log("Adding to hitList");
			}
		}

		void LateUpdate() {
			// If we are loading a level, stop raycasting and playing mouse over sounds.
			if (!Application.isLoadingLevel) {

				cachedHitList = new List<MenuItem>(hitList.Keys);
				foreach (MenuItem menuItem in cachedHitList) {
					hitList[menuItem] = false;
				}

				//Raycast to the current mouse position.
				Ray ray = activeCamera.ScreenPointToRay(Input.mousePosition);
				Debug.DrawRay(ray.origin, ray.direction, Color.red, 0.1f, true);

				RaycastHit hit;
				// If we hit something.
				if (Physics.Raycast(ray, out hit, Mathf.Infinity, textLayer)) {
					//Debug.Log("Ray hit : " + hit.collider.gameObject.name);
					Debug.DrawLine(activeCamera.transform.position, hit.point, Color.blue);
					MenuItem hitItem = hit.collider.GetComponent<MenuItem>();
					// We are clicking on it so do something.

					if (Input.GetKeyDown(KeyCode.Mouse0)) {
						hitItem.onClick();
						hitList[hitItem] = true;
					}
					// Just hover, we aren't clicking on it.
					else {
						hitItem.startHover();
						hitList[hitItem] = true;
					}
				}

				foreach (KeyValuePair<MenuItem, bool> data in hitList) {
					if (data.Value == false) {
						if (data.Key.isHovered) {
							data.Key.stopHover();
							data.Key.onRelease();
						}
					}
				}
			}
		}
	}
}