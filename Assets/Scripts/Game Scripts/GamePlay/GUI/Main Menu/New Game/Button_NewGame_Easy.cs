// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;

namespace DI_Game
{
	[AddComponentMenu("Menus/Main/New Game/Easy")]
	public class Button_NewGame_Easy : MenuItem
	{
		override public void clickAction() {
			Debug.Log ("clickAction");
			menuScript.loadingScreen.SetActive(true);
			Application.LoadLevel("DevLand");
		}
	}
}