// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;
using System;

namespace DI_Game
{
	[AddComponentMenu("Menus/Main/MenuItem")]
	public class MenuItem : MonoBehaviour
	{
		public AudioSource mouseOverSound;
		public Color hoverColor;

		private Color startingColor;

		public bool isHovered;
		private TextMesh mesh;
		private bool isClicked;
		protected MainMenu menuScript;
		public string cameraWaypointName;
		public GameObject menuGroup;

		public void OnEnable()
		{
			mesh = this.GetComponent<TextMesh>();
			this.startingColor = mesh.renderer.material.color;
			isHovered = false;
			isClicked = false;
			menuScript = menuGroup.transform.parent.gameObject.GetComponent<MainMenu>();
		}

		virtual public void clickAction()
		{

		}

		public void onClick() {
			if (isClicked == false) {
				isClicked = true;
				//Debug.Log ("OnClick");
				this.clickAction();
				this.stopHover();
			}
		}

		public void onRelease() {
			isClicked = false;
		}

		public void startHover()
		{
			if (isHovered == false) {
				mesh.renderer.material.color = hoverColor;
				if (mouseOverSound.isPlaying) {
					mouseOverSound.Stop();
				}
				mouseOverSound.Play();
				isHovered = true;
				//Debug.Log("Start Hover: " + mesh.text);
			}
		}

		public void stopHover()
		{
			if (isHovered == true) {
				isHovered = false;
				isClicked = false;
				mesh.renderer.material.color = startingColor;
				//Debug.Log("Stop Hover: " + mesh.text);
			}
		}
	}
}