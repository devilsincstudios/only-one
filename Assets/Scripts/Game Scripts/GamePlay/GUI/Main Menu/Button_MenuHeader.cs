// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using UnityEngine;
using DI_Events;

namespace DI_Game
{
	[AddComponentMenu("Menus/Main/Menu Header")]
	public class Button_MenuHeader : MenuItem
	{
		override public void clickAction() {
			//menuScript.activeCamera.gameObject.SetActive(false);
			//menuScript.activeCamera = menuGroupCamera;
			//menuScript.activeCamera.gameObject.SetActive(true);
			menuScript.menuGroup.SetActive(false);
			menuScript.menuGroup = menuGroup;
			EventCenter<string>.invoke("ChangeCameraWaypoint", cameraWaypointName);
			menuGroup.SetActive(true);
			this.stopHover();
		}
	}
}