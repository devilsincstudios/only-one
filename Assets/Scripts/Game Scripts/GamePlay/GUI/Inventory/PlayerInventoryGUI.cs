// 	Devils Inc Studios
// 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
//
// 	TODO: Include a description of the file here.
//

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DI_GUI
{
	[AddComponentMenu("GUI/Inventory/PlayerInventoryGUI")]
	public class PlayerInventoryGUI : GUIBase
	{
		private Rect guiDimensions;
		private Rect statusDimensions;
		private Rect hotbarDimensions;
		private Rect equippedDimensions;

		private Rect guiRect;
		private DI_Item.Inventory inventory;
		public GameObject player;
		private Texture2D nullIcon;
		private Texture2D nullStatusIcon;

		private DI_GameState.GameStateController GSC;

		private GUIStyle inventoryIconStyle;
		private Dictionary<Dictionary<int, int>, Dictionary<string, Rect>> itemPositions;
		private DI_Input.KeyStates inventoryKeyLastState;
		private Vector3 mousePosition;
		private bool startDrag = false;
		private bool isDragging = false;

		private int playerControlling = 1;

		private int selectedItemSlot;
		private KeyValuePair<int, int> selectedItem;
		private Vector3 startingPoint;
		private int guiDepth;
		private Rect lastHoveredItem;

		private Vector3 mousePositionAtEndOfDrag;

		private bool hasShownModal = false;
		private bool confirmAnswer = false;
		private bool displayModal = false;
		private Rect modalDimensions;

		private List<DI_Game.StatusEffect> statusEffects;
		private DI_Game.Entity.Player playerComponent;

		private bool endOfDrag = false;

		private string confirmMessage;

		private List<Texture2D> statusEffectIcons;
		private Dictionary<KeyValuePair<int, int>, Texture2D> itemIcons;

		public void OnEnable()
		{
			GSC = GameObject.FindGameObjectWithTag("Master Mind").GetComponent<DI_GameState.GameStateController>();
			playerComponent = player.GetComponent<DI_Game.Entity.Player>();

			statusEffects = playerComponent.statusEffects;

			guiDimensions = new Rect(Screen.width / 4, 10, 660, 475);
			statusDimensions = new Rect(Screen.width / 4 - 138, 10, 138, 475);
			modalDimensions = new Rect(Screen.width / 2 - 150, Screen.height / 2 - 40, 300, 80);
			Debug.Log(guiDimensions);
			inventory = DI_Item.InventoryManager.loadInventory(player.GetComponent<DI_Game.Entity.Player>().inventoryId);
			nullIcon = Resources.Load<Texture2D>("Icons/Items/noItemIcon");
			nullStatusIcon = Resources.Load<Texture2D>("Icons/Statuses/statusEffectIcon");

			itemPositions = new Dictionary<Dictionary<int, int>, Dictionary<string, Rect>>();

			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.addListener("onInput", handleInput);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.addListener("onGUIShow", handleGUIShow);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.addListener("onGUIHide", handleGUIHide);

			mousePosition = Input.mousePosition;
			mousePosition.y = Screen.height - mousePosition.y;
			playerControlling = (int) player.GetComponent<DI_Game.Entity.Player>().playerType + 1;
			selectedItem = new KeyValuePair<int, int>(-1, -1);

			itemIcons = new Dictionary<KeyValuePair<int, int>, Texture2D>();
			statusEffectIcons = new List<Texture2D>();
		}

		// Show the inventory if a loot window is opened.
		public void handleGUIShow(DI_GUI.GUIId guiId, int player)
		{
			if (guiId == GUIId.LOOT_CONTAINER && player == playerControlling) {
				showGUI();
			}
		}

		// Hide the inventory if a loot window is closed.
		public void handleGUIHide(DI_GUI.GUIId guiId, int player)
		{
			if (guiId == GUIId.LOOT_CONTAINER && player == playerControlling) {
				hideGUI();
			}
		}

		// Displaying a GUI window overrides the rest of the input controls.
		// So we will never be able to close the inventory from an event unless we fire it outself from an Event.key inside the GUI.
		public void handleInput(DI_Input.RegisteredKeys key, DI_Input.KeyStates state, int player)
		{
			if (player == playerControlling) {
				if (key == DI_Input.RegisteredKeys.INVENTORY) {
					if (state == DI_Input.KeyStates.KEY_PRESSED) {
						showGUI();
						DI_Events.EventCenter<DI_GUI.GUIId, int>.invoke("onGUIShow", GUIId.PLAYER_INVENTORY, playerControlling);
					}
				}
			}
		}

		public new void OnGUI ()
		{
			base.OnGUI();
			if (displayModal) {
				modalDimensions = GUI.ModalWindow((int)GUIId.MODAL_CONFIRM, modalDimensions, drawConfirm, "", guiHeader);
			}

			if (isVisable) {
				guiDimensions = GUI.Window((int)GUIId.PLAYER_INVENTORY, guiDimensions, drawGUI, "Inventory", guiHeader);
				statusDimensions = GUI.Window((int) GUIId.PLAYER_STATUS, statusDimensions, drawStatusGUI, "Status", guiHeader);
			}

			Event currentEvent = Event.current;

			// TODO This needs to be reworked for xbox controllers
			// The drag will be removed for xbox meaning we need an alternate way to delete/swap items.

			switch (currentEvent.button) {
				case 0:
					switch (currentEvent.type) {
						case EventType.MouseDown:
							startDrag = true;
						break;
						
						case EventType.MouseDrag:
							if (!isDragging) {
								startDrag = true;
								isDragging = true;
								Debug.Log("Start Drag");
								currentEvent.Use();
							}
						break;
						case EventType.MouseUp:
							if (isDragging) {
								startDrag = false;
								isDragging = false;
								mousePositionAtEndOfDrag = Input.mousePosition;
								mousePositionAtEndOfDrag.y = Screen.height - mousePositionAtEndOfDrag.y;
								endOfDrag = true;
								Debug.Log("End Drag");
								currentEvent.Use();
							}
						break;
					}
					break;
			}

			if (currentEvent.Equals(Event.KeyboardEvent(DI_Input.BindManager.getKey(DI_Input.RegisteredKeys.INVENTORY, playerControlling)))) {
				hideGUI();
				selectedItem = new KeyValuePair<int, int>(-1, -1);
				selectedItemSlot = 0;
				currentEvent.Use();
				DI_Events.EventCenter<DI_GUI.GUIId, int>.invoke("onGUIHide", GUIId.PLAYER_INVENTORY, playerControlling);
			}
		}

		public void drawStatusIcon(DI_Game.StatusEffect _status, int row, Texture2D icon)
		{
			Rect statusBorder;
			Rect statusTimer;
			Rect statusText;

			// 0 for even 64 for odd.
			int xOffset = (row % 2) * 74;

			if (row % 2 == 0) {
				statusText = new Rect(xOffset, 64 * (row / 2) + 22, 64, 10);
				statusBorder = new Rect(xOffset, 64 * (row / 2) + 20, 64, 64);
				statusTimer = new Rect(xOffset, 64 * (row / 2) + 65, 64, 10);
			}
			else {
				statusText = new Rect(xOffset, 64 * (row - 1) / 2 + 22, 64, 10);
				statusBorder = new Rect(xOffset, 64 * (row - 1) / 2 + 20, 64, 64);
				statusTimer = new Rect(xOffset, 64 * (row - 1) / 2 + 65, 64, 10);
			}

			GUIStyle style = new GUIStyle("CenterAligned");
			if (_status.isBuff) {
				style.normal.textColor = Color.green;
			}
			else {
				style.normal.textColor = Color.red;
			}
			style.fontSize = 12;
			style.wordWrap = false;
			style.fontStyle = FontStyle.Normal;

			GUI.Box(statusBorder, icon);
			GUI.Label(statusText, _status.status.ToString(), style);

			style.fontSize = 18;
			GUI.Label(statusTimer, MathLib.convertToReadableTime(_status.timeLeft), style);
		}

		public void drawStatusGUI(int guiId)
		{
			int row = 0;
			foreach (DI_Game.StatusEffect _statusEffect in statusEffects.ToArray()) {
				Texture2D icon;
				if (statusEffectIcons.Count > row) {
					icon = statusEffectIcons[row];
				}
				else {
					icon = _statusEffect.getStatusEffectIcon();
					if (icon == null) {
						icon = nullStatusIcon;
					}
				}
				drawStatusIcon(_statusEffect, row, icon);
				++row;
			}
		}

		public void drawConfirm(int guiId)
		{
			Rect contents = new Rect(0,0, 300, 50);

			GUI.Box(contents, "", guiSkin.box);

			Rect label = new Rect(0, 0, 300, 20);
			GUI.Label(label, confirmMessage, new GUIStyle("CenterAligned"));

			Rect yesButton = new Rect(100, 20, 50, 30);
			Rect noButton = new Rect(160, 20, 50, 30);

			if (GUI.Button(yesButton, "Yes")) {
				confirmAnswer = true;
				displayModal = false;
				hasShownModal = true;
			}
			if (GUI.Button(noButton, "No")) {
				confirmAnswer = false;
				displayModal = false;
				hasShownModal = true;
			}
		}

		public void drawInventoryItem(int row, int column, DI_Item.Item _item, Texture2D icon, int inventorySlot)
		{
			Rect boundingBox;
			Rect namePosition;
			Rect iconPosition;
			Rect stackPosition;
			Rect durabilityPosition;
			Rect ammoPosition;

			KeyValuePair<int, int> currentSlot = new KeyValuePair<int, int>(row, column);
			Dictionary<int, int> key = new Dictionary<int, int>();
			key.Add(row, column);

			if (itemPositions.ContainsKey(key)){
				 boundingBox = itemPositions[key]["boundingBox"];
				 namePosition = itemPositions[key]["namePosition"];
				 iconPosition = itemPositions[key]["iconPosition"];
				 stackPosition = itemPositions[key]["stackPosition"];
				 durabilityPosition = itemPositions[key]["durabilityPosition"];
				 ammoPosition = itemPositions[key]["ammoPosition"];
			}
			else {
				boundingBox = new Rect(128 * column + 10, 110 * row + 20, 128, 110);
				namePosition = new Rect(128 * column + 10, 110 * row + 20, 128, 20);
				iconPosition = new Rect(128 * column + 42, 110 * row + 40, 64, 64);
				stackPosition = new Rect(128 * column + 96, 110 * row + 108, 32, 20);
				durabilityPosition = new Rect(128 * column + 90, 110 * row + 108, 40, 20);
				ammoPosition = new Rect(128 * column + 20, 110 * row + 108, 32, 20);

				Dictionary<string, Rect> value = new Dictionary<string, Rect>();
				itemPositions.Add(key, value);
				itemPositions[key].Add("boundingBox", boundingBox);
				itemPositions[key].Add("namePosition", namePosition);
				itemPositions[key].Add("iconPosition", iconPosition);
				itemPositions[key].Add("stackPosition", stackPosition);
				itemPositions[key].Add("durabilityPosition", durabilityPosition);
				itemPositions[key].Add("ammoPosition", ammoPosition);
			}

			inventoryIconStyle = new GUIStyle("InventoryIcon");

			Rect screenSpaceBox = boundingBox;
			screenSpaceBox.x = screenSpaceBox.x + Screen.width / 4;
			if (screenSpaceBox.Contains(mousePosition)) {
				if (startDrag) {
					GUI.depth = 10;
					selectedItem = currentSlot;
					selectedItemSlot = inventorySlot;
					startingPoint = mousePosition;
					InventoryDragHelper.setDrag(inventory.getItem(inventorySlot), inventorySlot, inventory, GUIId.PLAYER_INVENTORY);
					startDrag = false;
					if (_item != null) {
						Debug.Log(_item.itemName + " Slot: " + inventorySlot);
					}
				}
			}

			if (endOfDrag) {
				if (screenSpaceBox.Contains(mousePositionAtEndOfDrag)) {
					if (endOfDrag) {
						if (selectedItem.Key == row && selectedItem.Value == column) {
							// Same as the starting item.
							// Ignore it.
							endOfDrag = false;
						}
						else {
							if (endOfDrag) {
								inventory.swapItems(selectedItemSlot, inventorySlot);
								endOfDrag = false;
							}
						}
					}
				}
				else {
//					// Player has dragged item outside of the GUI
//					if (endOfDrag && !hasShownModal) {
//						confirmMessage = "Delete Item?";
//						displayModal = true;
//					}
//					else {
//						if (endOfDrag) {
//							if (confirmAnswer) {
//								inventory.removeItem(selectedItemSlot);
//								confirmAnswer = false;
//								hasShownModal = false;
//							}
//							else {
//								confirmAnswer = false;
//								hasShownModal = false;
//							}
//							endOfDrag = false;
//						}
//					}
				}
			}

			if (isDragging) {
				if (selectedItem.Key == row && selectedItem.Value == column) {
					int deltaX = (int)mousePosition.x - (int)startingPoint.x;
					int deltaY = (int)mousePosition.y - (int)startingPoint.y;
					boundingBox = new Rect(128 * column + 10 + deltaX, 110 * row + 20 + deltaY, 128, 110);
					namePosition = new Rect(128 * column + 10 + deltaX, 110 * row + 20 + deltaY, 128, 20);
					iconPosition = new Rect(128 * column + 42 + deltaX, 110 * row + 40 + deltaY, 64, 64);
					stackPosition = new Rect(128 * column + 96 + deltaX, 110 * row + 108 + deltaY, 32, 20);
					durabilityPosition = new Rect(128 * column + 90 + deltaX, 110 * row + 108 + deltaY, 40, 20);
					ammoPosition = new Rect(128 * column + 20 + deltaX, 110 * row + 108 + deltaY, 32, 20);
				}
			}

			if (icon != null) {
				if (selectedItem.Key == row && selectedItem.Value == column) {
					GUI.Box(boundingBox, "", new GUIStyle("SelectedItem"));
				}
				else {
					GUI.Box(boundingBox, "", guiSkin.button);
				}
				GUI.Label(namePosition, _item.itemName, centerAligned);
				GUI.Label(iconPosition, icon, inventoryIconStyle);
				if (_item.stackable) {
					GUI.Label(stackPosition, _item.stackSize + "", rightAligned);
				}
				if (_item.type == DI_Item.ItemTypes._ARMOR || _item.type == DI_Item.ItemTypes._WEAPON) {
					float durability = Mathf.Round((_item.currentDurability / _item.maxDurability) * 100);
					if (durability > 50) {
						GUI.Label(durabilityPosition, durability + "%", rightTextGood);
					}
					else if (durability > 25) {
						GUI.Label(durabilityPosition, durability + "%", rightTextOk);
					}
					else if (durability > 10) {
						GUI.Label(durabilityPosition, durability + "%", rightTextPoor);
					}
					else {
						GUI.Label(durabilityPosition, durability + "%", rightTextCritical);
					}
				}
				if (_item.type == DI_Item.ItemTypes._WEAPON) {
					if (_item.ammoType != DI_Item.AmmoType._UNASSIGNED) {
						GUI.Label(ammoPosition, _item.currentAmmo + "/" + _item.maxAmmo, leftAligned);
					}
				}
			}
			else {
				GUI.Box(boundingBox, "", guiSkin.button);
			}

			itemPositions[key]["boundingBox"] = boundingBox;
			itemPositions[key]["namePosition"] = namePosition;
			itemPositions[key]["iconPosition"] = iconPosition;
			itemPositions[key]["stackPosition"] = stackPosition;
			itemPositions[key]["durabilityPosition"] = durabilityPosition;
			itemPositions[key]["ammoPosition"] = ammoPosition;
		}
		
		public override void drawGUI (int guiId)
		{
			if (GUI.Button(new Rect(628, 0, 24, 20), "X")) {
				hideGUI();
			}

			int row = 0;
			int displayedSlots = 0;
			int iteration = 0;
			for (row = 0; row < 4; ++row) {
				if (displayedSlots < inventory.maxSlots) {
					for (int column = 0; column < 5; ++column) {
						// Show 5 Items across
						if (displayedSlots < inventory.maxSlots) {
							Texture2D icon = null;
							KeyValuePair<int, int> index = new KeyValuePair<int, int>(row, column);

							if (itemIcons.ContainsKey(index)) {
								icon = itemIcons[index];
							}

							try {
								if (icon == null) {
									icon = DI_Item.ItemDatabase.getItemIcon(inventory.inventoryItems[iteration]);
								}
								if (icon == null) {
									icon = nullIcon;
								}
								else {
									// Cache the item icon so we don't have to keep loading it.
									if (!itemIcons.ContainsKey(index)) {
										itemIcons.Add(index, icon);
									}
								}

								drawInventoryItem(row, column, inventory.inventoryItems[iteration], icon, iteration);
								displayedSlots += inventory.inventoryItems[iteration].slots;
							}
							catch (Exception) {
								drawInventoryItem(row, column, null, null, iteration);
								++displayedSlots;
							}
							++iteration;
						}
					}
				}
			}
		}

		public void Update()
		{
			if (isVisable) {
				mousePosition = Input.mousePosition;
				mousePosition.y = Screen.height - mousePosition.y;
			}
		}

		public void OnDisable()
		{
			DI_Events.EventCenter<DI_Input.RegisteredKeys, DI_Input.KeyStates, int>.removeListener("onInput", handleInput);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.removeListener("onGUIShow", handleGUIShow);
			DI_Events.EventCenter<DI_GUI.GUIId, int>.removeListener("onGUIHide", handleGUIHide);
		}
	}
}
