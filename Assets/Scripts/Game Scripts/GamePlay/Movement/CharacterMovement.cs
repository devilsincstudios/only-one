﻿/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	This is the heart of the character it is the handler for mechanim.
*
*/

using UnityEngine;
using System.Collections;

namespace DI_Game {
	[AddComponentMenu("Controllers/Player/Player Controller")]
	public class CharacterMovement : EntityMovement {
		public int playerControlling;
		public GameObject cameraObject;
		protected DI_Camera.ThirdPersonCamera thirdPersonCamera;
		private bool isReloading = false;

		new public void Start() {
			thirdPersonCamera = cameraObject.GetComponent<DI_Camera.ThirdPersonCamera>();
			base.Start();
		}

		override public void FixedUpdate() {
			if (gameState == GameState.PLAYING) {
				currentBaseState = animator.GetCurrentAnimatorStateInfo(0);
				
				// Prevent looped rolls
				if (currentBaseState.nameHash == rollState) {
					animator.SetBool("Roll", false);
				}
				
				//Raycast down from the character's feet.
				Ray ray = new Ray(rootBone.transform.position, Vector3.down);
				RaycastHit hitInfo = new RaycastHit();
				if (Physics.Raycast(ray, out hitInfo)) {
					// If the current state is the fall state.
					if (currentBaseState.nameHash == fallState) {
						// Check the height, if we are higher than the rollHeight
						if (hitInfo.distance > rollHeight) {
							// Preform a roll.
							animator.SetBool("Roll", true);
						}
						// This doesn't seem to work quite as expected.
						// TODO Fix this.
						// Check if the animator is currently in transition.
						// It complains about the match target being set while in transition.
						// This was an attempt to fix that message.
						if (animator.IsInTransition(jumpState) == false) {
							animator.MatchTarget(hitInfo.point, Quaternion.identity, AvatarTarget.Root, new MatchTargetWeightMask(new Vector3(0, 1, 0), 0), 0.35f, 0.5f);
						}
					}
					
					MaterialType materialType = hitInfo.collider.gameObject.GetComponent<MaterialType>();
					if (materialType != null) {
						materialUnderFoot = materialType.materialType;
					}
					else {
						materialUnderFoot = Materials.GENERIC;
					}
					
					// If we are ignoring height we need to just fake being grounded.
					// Otherwise check the distance from the ground and compare it to the offset.
					if (!ignoreHeight && hitInfo.distance > groundedHeight) {
						animator.SetBool("Grounded", false);
					}
					else {
						animator.SetBool("Grounded", true);
					}
					
					animator.SetFloat("Jump Height", hitInfo.distance);
				}
			}
		}
	
		public void onReloadStart()
		{
			isReloading = true;
			animator.SetLayerWeight(aimingLayer, 1f);
		}

		public void onReloadComplete()
		{
			animator.SetBool("Reloading", false);
			animator.SetLayerWeight(aimingLayer, 0f);
			isReloading = false;
		}

		override public void Update() {
			if (gameState != DI_Game.GameState.PLAYING) {
				return;
			}
			
			// Is the player using a keyboard?
			if (DI_Input.InputManager.getPlayerControllerType(playerControlling) == DI_Input.ControllerType.PLAYER_ONE_KEYBOARD
			    || DI_Input.InputManager.getPlayerControllerType(playerControlling) == DI_Input.ControllerType.PLAYER_TWO_KEYBOARD) {
				
				// If sprint is pressed set isSprinting.
				if ( GSC.getKeyState(DI_Input.RegisteredKeys.SPRINT, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					isSprinting = true;
				}
				else {
					isSprinting = false;
				}
				
				// If AIM is pressed, let the camera know.
				// We also need to reduce the speed to a walk.
				if (GSC.getKeyState(DI_Input.RegisteredKeys.AIM, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					thirdPersonCamera.setAiming(true);
					isAiming = true;
					maxSpeed = walkSpeed;
					animator.SetLayerWeight(aimingLayer, 1f);
				}
				else {
					thirdPersonCamera.setAiming(false);
					isAiming = false;
					maxSpeed = runSpeed;
					if (!isReloading) {
						animator.SetLayerWeight(aimingLayer, 0f);
					}
				}

				if (GSC.getKeyState(DI_Input.RegisteredKeys.RELOAD, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					animator.SetBool("Reloading", true);
				}

				if (GSC.getKeyState(DI_Input.RegisteredKeys.ATTACK, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					animator.SetBool("Attacking", true);
				}
				else {
					animator.SetBool("Attacking", false);
				}

				if (GSC.getKeyState(DI_Input.RegisteredKeys.CROUCH, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					animator.SetBool("Crouching", !animator.GetBool("Crouching"));
				}

				// If we aren't aiming and we are sprinting.
				// Increase the max speed.
				if (!isAiming && isSprinting) {
					maxSpeed = sprintSpeed;
				}
				
				// If the player is pressing forwards.
				if (GSC.getKeyState(DI_Input.RegisteredKeys.FORWARDS, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					// We just exited a sprint
					// Slow down gradually to ease the transition.
					if (currentSpeed > maxSpeed) {
						// Slowly decrease the current speed until we reach non sprint speeds.
						currentSpeed = Mathf.Clamp(currentSpeed - acceleration / Time.deltaTime, 0f, sprintSpeed);
					}
					else {
						// The player was not sprinting so cap the speed at run speed.
						currentSpeed = Mathf.Clamp(currentSpeed + acceleration / Time.deltaTime, 0f, maxSpeed);
					}
				}
				// If the player is pressing backwards.
				else if (GSC.getKeyState(DI_Input.RegisteredKeys.BACKWARDS, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					// "Increase" speed up to the walk speed.
					// Walk speed is negative when moving backwards.
					currentSpeed = Mathf.Clamp(currentSpeed - acceleration / Time.deltaTime, -walkSpeed, walkSpeed);
				}
				else {
					// Decrease speed until we hit 0 so we get avoid having a sudden stop after moving.
					currentSpeed = Mathf.Clamp(currentSpeed - acceleration / Time.deltaTime, 0, sprintSpeed);
				}
				
				// If the player is holding turn left.
				if (GSC.getKeyState(DI_Input.RegisteredKeys.TURN_LEFT, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					// Decrease the turn angle until we hit the cap.
					currentTurnRate = Mathf.Clamp(currentTurnRate - turnRate / Time.deltaTime, -maxTurnAngle, maxTurnAngle);
				}
				
				// If the player is holding turn right.
				else if (GSC.getKeyState(DI_Input.RegisteredKeys.TURN_RIGHT, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					// Increase the turn angle until we hit the cap.
					currentTurnRate = Mathf.Clamp(currentTurnRate + turnRate / Time.deltaTime, -maxTurnAngle, maxTurnAngle);
				}
				// The player isn't holding down turn anymore.
				else {
					if (currentTurnRate < 0) {
						// Player was turning left.
						// Increase the turn angle until we hit 0.
						currentTurnRate = Mathf.Clamp(currentTurnRate + (turnRate * turnDeceleration) / Time.deltaTime, -maxTurnAngle, 0);
					}
					else {
						// Player was turning right.
						// Decrease the turn angle until we hit 0.
						currentTurnRate = Mathf.Clamp(currentTurnRate - (turnRate * turnDeceleration) / Time.deltaTime, 0, maxTurnAngle);
					}
				}
			}
			// If they are using a gamepad we have to use axis controlls.
			else {
				
				// If AIM is pressed, let the camera know.
				// We also need to reduce the speed to a walk.
				if (GSC.getKeyState(DI_Input.RegisteredKeys.AIM, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					thirdPersonCamera.setAiming(true);
					maxSpeed = walkSpeed;
					isAiming = true;
					animator.SetLayerWeight(aimingLayer, 1f);
				}
				else {
					thirdPersonCamera.setAiming(false);
					// Controllers always have sprint speed as their max speed.
					maxSpeed = sprintSpeed;
					isAiming = false;
					if (!isReloading) {
						animator.SetLayerWeight(aimingLayer, 0f);
					}
				}

				if (GSC.getKeyState(DI_Input.RegisteredKeys.ATTACK, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					animator.SetBool("Attacking", true);
				}
				else {
					animator.SetBool("Attacking", false);
				}

				if (GSC.getKeyState(DI_Input.RegisteredKeys.RELOAD, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					animator.SetBool("Reloading", true);
					animator.SetLayerWeight(aimingLayer, 1f);
				}

				// If the player is pressing forward.
				// We don't need to check for sprint on a controller as the player has more control over the input speed.
				// But this does need some work.
				// TODO FIX THIS FOR CONTROLLERS.
				if (GSC.getKeyState(DI_Input.RegisteredKeys.VERTICAL, playerControlling) == DI_Input.KeyStates.KEY_POSITIVE) {
					currentSpeed = Mathf.Clamp(currentSpeed + acceleration / Time.deltaTime, 0f, sprintSpeed);
				}
				// If the player is pressing backward.
				else if (GSC.getKeyState(DI_Input.RegisteredKeys.VERTICAL, playerControlling) == DI_Input.KeyStates.KEY_NEGATIVE) {
					// "Increase" speed up to the walk speed.
					// Walk speed is negative when moving backwards.
					currentSpeed = Mathf.Clamp(currentSpeed - acceleration / Time.deltaTime, -walkSpeed, walkSpeed);
				}
				else {
					// Decrease speed until we hit 0 so we get avoid having a sudden stop after moving.
					currentSpeed = Mathf.Clamp(currentSpeed - acceleration / Time.deltaTime, 0, sprintSpeed);
				}
				
				// If the player is pressing left or right.
				if (GSC.getKeyState(DI_Input.RegisteredKeys.HORIZONTAL, playerControlling) == DI_Input.KeyStates.KEY_NEGATIVE) {
					// Decrease the turn angle until we hit the cap.
					currentTurnRate = Mathf.Clamp(currentTurnRate - turnRate / Time.deltaTime, -maxTurnAngle, maxTurnAngle);
				}
				else if (GSC.getKeyState(DI_Input.RegisteredKeys.HORIZONTAL, playerControlling) == DI_Input.KeyStates.KEY_POSITIVE) {
					// Increase the turn angle until we hit the cap.
					currentTurnRate = Mathf.Clamp(currentTurnRate + turnRate / Time.deltaTime, -maxTurnAngle, maxTurnAngle);
				}
				// The player isn't holding down turn anymore.
				else {
					if (currentTurnRate < 0) {
						// Player was turning left.
						// Increase the turn angle until we hit 0.
						currentTurnRate = Mathf.Clamp(currentTurnRate + (turnRate * turnDeceleration) / Time.deltaTime, -maxTurnAngle, 0);
					}
					else {
						// Player was turning right.
						// Decrease the turn angle until we hit 0.
						currentTurnRate = Mathf.Clamp(currentTurnRate - (turnRate * turnDeceleration) / Time.deltaTime, 0, maxTurnAngle);
					}
				}
			}
			
			// Check the current state, if its the locomotive state (run, walk, sprint, walk backwards)
			if (currentBaseState.nameHash == locoState) {
				// If the player has jump held down.
				if (GSC.getKeyState(DI_Input.RegisteredKeys.JUMP, playerControlling) == DI_Input.KeyStates.KEY_PRESSED) {
					// Tell the animator we want to jump.
					animator.SetBool("Jump", true);
				}
				else {
					// Tell the animator we don't want to jump.
					animator.SetBool("Jump", false);
				}
			}
			// Check the current state, if its the jump state
			// Ignore the jump command.
			if (currentBaseState.nameHash == jumpState) {
				// Tell the animator we don't want to jump.
				animator.SetBool("Jump", false);
			}
			
			// Update the animator parameters.
			animator.SetFloat("Speed", currentSpeed);
			animator.SetFloat("Turn Angle", currentTurnRate);
			animator.SetBool("Aiming", isAiming);
		}
	}
}
