/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	This is the heart of the character it is the handler for mechanim.
*
*/

using UnityEngine;
using System.Collections;

namespace DI_Game {
	[AddComponentMenu("Controllers/Player/Player Controller")]
	public class EntityMovement : MonoBehaviour {
		
		public float acceleration;
		public float turnRate;
		public float maxTurnAngle;
		public float runSpeed;
		public float sprintSpeed;
		public float walkSpeed;
		public float turnDeceleration;
		public float rollHeight;
		public float groundedHeight;
		public GameObject rootBone;

		public Transform avatar;
		
		protected DI_GameState.GameStateController GSC;
		protected Animator animator;
		protected DI_Game.GameState gameState;
		
		protected bool ignoreHeight;
		
		protected AnimatorStateInfo currentBaseState;
		protected float startingMass;
		
		protected static int idleState = Animator.StringToHash("Base Layer.Idle");
		protected static int locoState = Animator.StringToHash("Base Layer.Locomotion");
		protected static int jumpState = Animator.StringToHash("Base Layer.Jump");
		protected static int fallState = Animator.StringToHash("Base Layer.Fall");
		protected static int rollState = Animator.StringToHash("Base Layer.Roll");
		protected static int aimingLayer = 1;
		protected float currentSpeed = 0;
		protected float currentTurnRate = 0;
		protected float maxSpeed;
		protected bool isAiming;
		protected bool isSprinting;
		public bool onStairs;
		
		protected float savedRunSpeed;
		protected float savedSprintSpeed;
		protected Materials materialUnderFoot;
		
		//protected ParticleSystem Dust;
		
		// Use this for initialization
		public void Start () {
			GSC = GameObject.FindGameObjectWithTag("Master Mind").GetComponent<DI_GameState.GameStateController>();
			animator = this.GetComponentInChildren<Animator>();
			gameState = GSC.getGameState();
			// Register gamestate change listener.
			DI_Events.EventCenter<DI_Game.GameState>.addListener("onGameStateChange", handleGameStateChange);
			startingMass = this.rigidbody.mass;
			maxSpeed = runSpeed;
			onStairs = false;
			materialUnderFoot = Materials.GENERIC;
			//Dust = Resources.Load<ParticleSystem>("Effects/Particles/Dust/Dust");
			savedRunSpeed = runSpeed;
			savedSprintSpeed = sprintSpeed;
		}
		
		// Called by the stairs trigger.
		public void enterStairs() {
			onStairs = true;
			ignoreHeight = true;
			runSpeed = 1;
			sprintSpeed = 1;
		}

		public void exitStairs() {
			onStairs = false;
			ignoreHeight = false;
			runSpeed = savedRunSpeed;
			sprintSpeed = savedSprintSpeed;
		}
		
		public void handleGameStateChange(DI_Game.GameState state) {
			gameState = state;
		}

		//Physics
		virtual public void FixedUpdate() {

		}
		
		// Input
		virtual public void Update () {
		}
		
		// Audio
		public void LateUpdate() {
		}
		
		public void onFootDown() {
			Footsteps.playFootstep(materialUnderFoot, this.audio);
			//ParticleSystem DustClone = (ParticleSystem) Instantiate(Dust);
			//DustClone.transform.position = this.avatar.GetComponent<AttachmentPoints>().getBone(AttachmentTypes.RIGHT_FOOT).transform.position;
			//DustClone.transform.localPosition.Set(0f, DustClone.transform.localPosition.y + .025f, 0f);
			//Destroy(DustClone.gameObject, DustClone.duration);
		}
		
		// This is ment for walking up stairs and such so that the player doesn't constantly fall.
		public void setIgnoreHeight(bool ignore) {
			ignoreHeight = ignore;
		}
	}
}
