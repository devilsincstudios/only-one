/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	Used on AI characters, this tracks fall height and isGrounded states.
*
*/

// TODO update this with changes from CharacterMovement.

using UnityEngine;
using System.Collections;

namespace DI_Game {
	[AddComponentMenu("Controllers/AI/AI Controller")]
	public class AIMovement : EntityMovement {
		//Physics
		override public void FixedUpdate() {
			currentBaseState = animator.GetCurrentAnimatorStateInfo(0);
			
			// Prevent looped rolls
			if (currentBaseState.nameHash == rollState) {
				animator.SetBool("Roll", false);
			}
			
			//Raycast down from the character's feet.
			Ray ray = new Ray(rootBone.transform.position, Vector3.down);
			RaycastHit hitInfo = new RaycastHit();
			if (Physics.Raycast(ray, out hitInfo)) {
				if (currentBaseState.nameHash == fallState || currentBaseState.nameHash == jumpState) {
					if (currentBaseState.nameHash == fallState) {
						if (hitInfo.distance > 6f) {
							animator.SetBool("Roll", true);
						}
						if (animator.IsInTransition(jumpState) == false) {
							animator.MatchTarget(hitInfo.point, Quaternion.identity, AvatarTarget.Root, new MatchTargetWeightMask(new Vector3(0, 1, 0), 0), 0.35f, 0.5f);
						}
					}
					//if (currentBaseState.nameHash == jumpState && hitInfo.distance < 2f) {
					//animator.MatchTarget(hitInfo.point, Quaternion.identity, AvatarTarget.Root, new MatchTargetWeightMask(new Vector3(0, 1, 0), 0), 0.35f, 0.5f);
					//}
				}

				MaterialType materialType = hitInfo.collider.gameObject.GetComponent<MaterialType>();
				if (materialType != null) {
					materialUnderFoot = materialType.materialType;
				}
				else {
					materialUnderFoot = Materials.GENERIC;
				}

				if (hitInfo.distance > 0.3f) {
					animator.SetBool("Grounded", false);
				}
				else {
					animator.SetBool("Grounded", true);
				}
				
				animator.SetFloat("Jump Height", hitInfo.distance);
			}
		}
	}
}
