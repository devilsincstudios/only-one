﻿/*
*
* 	Devils Inc Studios
* 	Copyright DEVILS INC. STUDIOS LIMITED 2013, 2014
*	
*	TODO: Include a description of the file here.
*
*/

/*
 * 
 * TODO This needs an overhaul.
 * 
*/

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace DI_Game {
	[AddComponentMenu("Environment/Day Night Cycle")]
	public class CycleTime : MonoBehaviour {
		public double timeScale = 4.0f;
		public int hour;
		public GameObject theSun;
		public GameObject theMoon;
		public GameObject celestialRotator;
		
		private GameObject timeDisplay;
		private DateTime date;
		
		private bool hasRotated;
		private float idleTime;
		private GameObject masterMind;
//		private Material skyboxMaterial;
		
		// Use this for initialization
		void Start () {
			//Fill codes we use.
			//The real date doesn't mater for this object.
			//Prepopulate it with Jan 1st 2000.
			//skyboxMaterial = RenderSettings.skybox;
		}

		public void convertToUTC(DateTime dateObject) {
			TimeZone localZone = TimeZone.CurrentTimeZone;
			DateTime baseUTC = new DateTime(2000, 1, 1);
			DateTime localTime = localZone.ToLocalTime(baseUTC);
			TimeSpan localOffset = localZone.GetUtcOffset(localTime);
		}
		
		void FixedUpdate () {
		}
		
		void rotateSun() {
			//Unity has no concept of North,South,East, West.
			//So we get to create our own joy!
			// Each hour is 15 degrees - 360/(24 * 60) = 0.25
			double sunRotation = (double) ((date.Hour + 1) * 60 + date.Minute) * 0.25;
			celestialRotator.gameObject.transform.rotation = Quaternion.Euler((float) sunRotation, 0, 0);		
			if (date.Hour >= 6 && date.Hour <= 21) {
				switch (date.Hour) {
				case 6:
				case 7:
					theSun.light.intensity = 0.25f;
					//skyboxMaterial.SetColor("_Tint", Color.grey);
				break;
				case 8:
				case 9:
					theSun.light.intensity = 0.375f;
					//skyboxMaterial.SetColor("_Tint", Color.blue);
				break;
				case 10:
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
					theSun.light.intensity = 0.5f;
					//skyboxMaterial.SetColor("_Tint", Color.blue);
				break;
				case 16:
				case 17:
				case 18:
					theSun.light.intensity = 0.375f;
					//skyboxMaterial.SetColor("_Tint", Color.blue);
				break;
				case 19:
				case 20:
				case 21:
					theSun.light.intensity = 0.25f;
					//skyboxMaterial.SetColor("_Tint", Color.grey);
				break;
				}
				theMoon.light.intensity = 0.0f;
				//skyboxMaterial.SetColor("_Tint", Color.black);
			}
			else {
				theMoon.light.intensity = 0.02f;
				theSun.light.intensity = 0.0f;
			}
			// Total Light = 1
			// Each object sun/moon needs to share.
			//double sunBrightness = CelestialTime.getBrightness(sunRotation);
			//double moonBrightness = 0.5 - sunBrightness;
			
			//theSun.light.intensity = (float) sunBrightness;
			//theMoon.light.intensity = (float) moonBrightness;
			//Debug.Log ("Sun: " + sunBrightness + " Moon: " + moonBrightness);
		}
		
		void updateTimeDisplay() {
			timeDisplay.guiText.text = date.Hour + ":" + date.Minute + ":" + date.Second;
		}
	}
}